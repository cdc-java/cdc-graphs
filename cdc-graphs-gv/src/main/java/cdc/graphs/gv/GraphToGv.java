package cdc.graphs.gv;

import java.io.File;
import java.io.IOException;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.graphs.EdgeTip;
import cdc.graphs.GraphAdapter;
import cdc.graphs.core.GraphCycles;
import cdc.gv.GvWriter;
import cdc.gv.atts.GvEdgeAttributes;
import cdc.gv.atts.GvNodeAttributes;
import cdc.gv.atts.GvNodeShape;
import cdc.gv.atts.GvNodeStyle;
import cdc.gv.colors.GvColor;
import cdc.gv.colors.GvX11Colors;
import cdc.gv.tools.GvEngine;
import cdc.gv.tools.GvFormat;
import cdc.gv.tools.GvToAny;
import cdc.util.lang.Checks;

/**
 * Utility used to create a Graphviz representation of a graph (for debug
 * purpose).
 * <p>
 * Node fill color depends on the node connectivity.<br>
 * Edge color and node font color depend on its belonging to a cycle.
 *
 * @author Damien Carbonne
 * @param <N> The node type.
 * @param <E> The edge type.
 *
 */
public final class GraphToGv<N, E> {
    private static final Logger LOGGER = LogManager.getLogger(GraphToGv.class);

    protected static final GvColor COLOR_IN = new GvColor(255, 175, 175);
    protected static final GvColor COLOR_OUT = new GvColor(175, 175, 255);
    protected static final GvColor COLOR_IN_OUT = new GvColor(255, 175, 255);
    protected static final GvColor COLOR_ISOLATED = new GvColor(255, 255, 175);

    private final Set<Feature> features = EnumSet.noneOf(Feature.class);

    public static class DefaultNodeIdProvider {
        private final Map<Object, String> map = new HashMap<>();

        public String getId(Object object) {
            return map.computeIfAbsent(object, k -> "N" + map.size());
        }
    }

    public final DefaultNodeIdProvider defaultNodeIdProvider = new DefaultNodeIdProvider();

    /**
     * Default function used to extract the id of a node.
     */
    public final Function<? super N, String> defaultNodeToId = defaultNodeIdProvider::getId;

    protected Function<? super N, String> nodeToId = defaultNodeToId;

    public enum Feature {
        SHOW_CYCLES;

        private static final Feature[] ALL = {
                Feature.SHOW_CYCLES
        };

        public static Feature[] all() {
            return ALL.clone();
        }
    }

    public GraphToGv() {
    }

    public GraphToGv(Feature... features) {
        for (final Feature feature : features) {
            setEnabled(feature, true);
        }
    }

    public void setEnabled(Feature feature,
                           boolean enabled) {
        Checks.isNotNull(feature, "feature");
        if (enabled) {
            features.add(feature);
        } else {
            features.remove(feature);
        }
    }

    public boolean isEnabled(Feature feature) {
        return features.contains(feature);
    }

    public void setNodeToId(final Function<? super N, String> function) {
        Checks.isNotNull(function, "function");
        this.nodeToId = function;
    }

    private class Internal {
        private final GraphAdapter<N, E> adapter;
        private final GraphCycles<N, E> cycles;

        public Internal(GraphAdapter<N, E> adapter) {
            this.adapter = adapter;
            if (isEnabled(Feature.SHOW_CYCLES)) {
                this.cycles = new GraphCycles<>(adapter);
            } else {
                this.cycles = null;
            }
            // Check node ids
            final Map<String, N> map = new HashMap<>();
            for (final N node : adapter.getNodes()) {
                final String id = nodeToId.apply(node);
                if (map.containsKey(id)) {
                    throw new IllegalArgumentException("Duplicate id " + id + ": " + map.get(id) + " " + node);
                } else {
                    map.put(id, node);
                }
            }
        }

        private GvNodeAttributes getNodeAttributes(N node) {
            final GvNodeAttributes result =
                    new GvNodeAttributes().setStyle(GvNodeStyle.FILLED)
                                          .setShape(GvNodeShape.BOX)
                                          .setLabel(node.toString())
                                          .setMargin(0.05, 0.025)
                                          .setHeight(0.1)
                                          .setWidth(0.1);
            if (cycles != null && cycles.nodeIsCycleMember(node)) {
                result.setFontColor(GvX11Colors.RED);
            } else {
                result.setFontColor(GvX11Colors.BLACK);
            }

            switch (adapter.getConnectivity(node)) {
            case IN:
                result.setFillColor(COLOR_IN);
                break;
            case IN_OUT:
                result.setFillColor(COLOR_IN_OUT);
                break;
            case ISOLATED:
                result.setFillColor(COLOR_ISOLATED);
                break;
            case OUT:
                result.setFillColor(COLOR_OUT);
                break;
            default:
                assert false;
                break;
            }
            return result;
        }

        private GvEdgeAttributes getEdgeAttributes(E edge) {
            final GvEdgeAttributes result = new GvEdgeAttributes();
            if (cycles.edgeIsCycleMember(edge)) {
                result.setColor(GvX11Colors.RED);
            } else {
                result.setColor(GvX11Colors.BLACK);
            }
            return result;
        }

        void write(GvWriter writer) throws IOException {
            writer.beginGraph("g", true, null);
            for (final N node : adapter.getNodes()) {
                writer.addNode(nodeToId.apply(node), getNodeAttributes(node));
            }
            for (final E edge : adapter.getEdges()) {
                writer.addEdge(nodeToId.apply(adapter.getTip(edge, EdgeTip.SOURCE)),
                               nodeToId.apply(adapter.getTip(edge, EdgeTip.TARGET)),
                               getEdgeAttributes(edge));
            }
            writer.endGraph();
        }
    }

    /**
     * Generates a Graphviz file.
     *
     * @param adapter The graph adapter.
     * @param filename The file name.
     * @throws IOException When an I/O error occurs.
     */
    public void write(GraphAdapter<N, E> adapter,
                      String filename) throws IOException {
        try (final GvWriter writer = new GvWriter(filename)) {
            final Internal internal = new Internal(adapter);
            internal.write(writer);
        }
    }

    /**
     * Generates a Graphviz file.
     *
     * @param adapter The graph adapter.
     * @param file the file.
     * @throws IOException When an I/O error occurs.
     */
    public void write(GraphAdapter<N, E> adapter,
                      File file) throws IOException {
        try (final GvWriter writer = new GvWriter(file)) {
            final Internal internal = new Internal(adapter);
            internal.write(writer);
        }
    }

    /**
     * Generates a Graphviz file and converts it to PNG.
     *
     * @param adapter The graph adapter.
     * @param outputDir The directory where files are generated.
     * @param basename The files base names.
     * @param engine The Graphviz engine to use for layout.
     * @throws IOException When an I/O error occurs.
     */
    public void write(GraphAdapter<N, E> adapter,
                      File outputDir,
                      String basename,
                      GvEngine engine) throws IOException {
        Checks.isNotNull(outputDir, "outputDir");
        Checks.isNotNull(basename, "basename");
        outputDir.mkdir();
        final File gvFile = new File(outputDir, basename + ".gv");
        write(adapter, gvFile);

        final GvToAny.MainArgs margs = new GvToAny.MainArgs();
        margs.input = gvFile;
        margs.outputDir = outputDir;
        margs.engine = engine;
        margs.formats.add(GvFormat.PNG);
        GvToAny.execute(margs);
    }

    /**
     * Generates a Graphviz file and converts it to PNG, using the DOT engine.
     *
     * @param adapter The graph adapter.
     * @param outputDir The directory where files are generated.
     * @param basename The files base names.
     * @throws IOException When an I/O error occurs.
     */
    public void write(GraphAdapter<N, E> adapter,
                      File outputDir,
                      String basename) throws IOException {
        write(adapter, outputDir, basename, GvEngine.DOT);
    }

    /**
     * Generates a Graphviz file and converts it to PNG.
     * <p>
     * If an I/O error occurs, it is logged.
     *
     * @param adapter The graph adapter.
     * @param outputDir The directory where files are generated.
     * @param basename The files base names.
     * @param engine The Graphviz engine to use for layout.
     */
    public void writeNoException(GraphAdapter<N, E> adapter,
                                 File outputDir,
                                 String basename,
                                 GvEngine engine) {
        try {
            write(adapter, outputDir, basename, engine);
        } catch (final IOException e) {
            LOGGER.catching(e);
        }
    }

    /**
     * Generates a Graphviz file and converts it to PNG, using the DOT engine.
     * <p>
     * If an I/O error occurs, it is logged.
     *
     * @param adapter The graph adapter.
     * @param outputDir The directory where files are generated.
     * @param basename The files base names.
     */
    public void writeNoException(GraphAdapter<N, E> adapter,
                                 File outputDir,
                                 String basename) {
        writeNoException(adapter, outputDir, basename, GvEngine.DOT);
    }
}