package cdc.graphs.core;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.graphs.EdgeDirection;
import cdc.graphs.GraphAdapter;
import cdc.graphs.TraversalMethod;
import cdc.graphs.TraversalOrder;
import cdc.util.function.Evaluator;
import cdc.util.function.Visitor;
import cdc.util.lang.Checks;
import cdc.util.lang.FailureReaction;

/**
 * Set of simple algorithms for <em>slim</em> graphs.
 * <p>
 * In a <em>slim</em> graph, the node type can be any object and there is no edge structure.
 * However, it is possible to explore neighbor nodes of any node.
 * <p>
 * The following algorithms are provided.
 * <ul>
 * <li>Transitive closure of a node.
 * <li>Traversal (depth first and breadth first) starting on a node.
 * <li>Topological sort of the transitive closure of a node.
 * <li>Are 2 nodes connected?
 * </ul>
 *
 * Static and non-static versions of functions are provided.
 *
 * @author Damien Carbonne
 * @param <N> The node type.
 */
public final class SlimGraph<N> {
    private static final Logger LOGGER = LogManager.getLogger(SlimGraph.class);
    private static final String METHOD = "method";
    private static final String NODE = "node";
    private static final String ORDER = "order";
    private static final String VISITOR = "visitor";
    private static final String NEIGHBORS = "neighbors";

    private final Function<N, Iterable<? extends N>> neighbors;

    /**
     * Creates a slim graph.
     *
     * @param neighbors The function that returns the neighbors of a node.
     */
    public SlimGraph(Function<N, Iterable<? extends N>> neighbors) {
        Checks.isNotNull(neighbors, NEIGHBORS);

        this.neighbors = neighbors;
    }

    /**
     * Created a slim graph from a graph adapter.
     *
     * @param adapter The graph adapter.
     * @param direction The edge direction.
     */
    public SlimGraph(GraphAdapter<N, ?> adapter,
                     EdgeDirection direction) {
        Checks.isNotNull(adapter, "adapter");
        this.neighbors = n -> adapter.getConnectedNodes(n, direction);
    }

    /**
     * Computes the transitive closure of a node.
     *
     * @param node The starting node.
     * @return The set of nodes that are directly or indirectly connected to {@code node}.
     */
    public Set<N> computeTransitiveClosure(N node) {
        return computeTransitiveClosure(node, neighbors);
    }

    /**
     * Most generic traversal function.
     *
     * @param node The initial node.
     * @param method The traversal method.
     * @param order The traversal order.
     * @param visitor Visitor that will be invoked for each visited node.
     * @param evaluator An optional evaluator used to control traversal termination.
     * @throws IllegalArgumentException When {@code node}, {@code method}, {@code order},
     *             or {@code visitor} is {@code null}.
     */
    public void traverse(N node,
                         TraversalMethod method,
                         TraversalOrder order,
                         Visitor<N> visitor,
                         Evaluator<N> evaluator) {
        traverse(node, method, order, visitor, evaluator, neighbors);
    }

    /**
     * Depth first traversal.
     *
     * @param node The initial node.
     * @param order The traversal order.
     * @param visitor Visitor that will be invoked for each visited node.
     * @param evaluator An optional evaluator used to control traversal termination.
     * @throws IllegalArgumentException When {@code node}, {@code order}, {@code visitor}
     *             or {@code neighbors} is {@code null}.
     */
    public void traverseDepthFirst(N node,
                                   TraversalOrder order,
                                   Visitor<N> visitor,
                                   Evaluator<N> evaluator) {
        traverseDepthFirst(node, order, visitor, evaluator, neighbors);
    }

    /**
     * Depth first pre order traversal.
     *
     * @param node The initial node.
     * @param visitor Visitor that will be invoked for each visited node.
     * @param evaluator An optional evaluator used to control traversal termination.
     * @throws IllegalArgumentException When {@code node}, {@code visitor}
     *             or {@code neighbors} is {@code null}.
     */
    public void traverseDepthFirstPre(N node,
                                      Visitor<N> visitor,
                                      Evaluator<N> evaluator) {
        traverseDepthFirstPre(node, visitor, evaluator, neighbors);
    }

    /**
     * Depth first post order traversal.
     *
     * @param node The initial node.
     * @param visitor Visitor that will be invoked for each visited node.
     * @param evaluator An optional evaluator used to control traversal termination.
     * @throws IllegalArgumentException When {@code node}, {@code visitor}
     *             or {@code neighbors} is {@code null}.
     */
    public void traverseDepthFirstPost(N node,
                                       Visitor<N> visitor,
                                       Evaluator<N> evaluator) {
        traverseDepthFirstPost(node, visitor, evaluator, neighbors);
    }

    /**
     * Breadth first traversal.
     *
     * @param node The initial node.
     * @param order The traversal order.
     * @param visitor Visitor that will be invoked for each visited node.
     * @param evaluator An optional evaluator used to control traversal termination.
     * @throws IllegalArgumentException When {@code node}, {@code order}, {@code visitor}
     *             or {@code neighbors} is {@code null}.
     */
    public void traverseBreadthFirst(N node,
                                     TraversalOrder order,
                                     Visitor<N> visitor,
                                     Evaluator<N> evaluator) {
        traverseBreadthFirst(node, order, visitor, evaluator, neighbors);
    }

    /**
     * Breadth first pre order traversal.
     *
     * @param node The initial node.
     * @param visitor Visitor that will be invoked for each visited node.
     * @param evaluator An optional evaluator used to control traversal termination.
     * @throws IllegalArgumentException When {@code node}, {@code visitor}
     *             or {@code neighbors} is {@code null}.
     */
    public void traverseBreadthFirstPre(N node,
                                        Visitor<N> visitor,
                                        Evaluator<N> evaluator) {
        traverseBreadthFirstPre(node, visitor, evaluator, neighbors);
    }

    /**
     * Breadth first post order traversal.
     *
     * @param node The initial node.
     * @param visitor Visitor that will be invoked for each visited node.
     * @param evaluator An optional evaluator used to control traversal termination.
     * @throws IllegalArgumentException When {@code node}, {@code visitor}
     *             or {@code neighbors} is {@code null}.
     */
    public void traverseBreadthFirstPost(N node,
                                         Visitor<N> visitor,
                                         Evaluator<N> evaluator) {
        traverseBreadthFirstPost(node, visitor, evaluator, neighbors);
    }

    /**
     * Returns a topological sort of nodes connected to a root node.
     * <p>
     * If node {@code A} is connected to node {@code B}, then {@code A} will come before {@code B} in result.
     *
     * @param node The root node.
     * @param reaction The reaction to adopt if the graph contains loops.
     * @return A topological sort of nodes connected to {@code node} or an empty list.
     * @throws IllegalArgumentException When {@code node} or {@code neighbors} is {@code null},
     *             or when there is a loop and {@code reaction} is {@link FailureReaction#FAIL}.
     */
    public List<N> topologicalSort(N node,
                                   FailureReaction reaction) {
        return topologicalSort(node, reaction, neighbors);
    }

    /**
     * Returns a topological sort of nodes connected to a root node.
     * <p>
     * If node {@code A} is connected to node {@code B}, then {@code A} will come before {@code B} in result.
     *
     * @param node The root node.
     * @return A topological sort of nodes connected to {@code node} or an empty list.
     * @throws IllegalArgumentException When {@code node} or {@code neighbors} is {@code null},
     *             or when there is a loop.
     */
    public List<N> topologicalSort(N node) {
        return topologicalSort(node, neighbors);
    }

    /**
     * Returns {@code true} when 2 nodes are connected.
     *
     * @param source The source node.
     * @param target The target node.
     * @return {@code true} when {@code source} is connected to {@code target}.
     */
    public boolean areConnected(N source,
                                N target) {
        return areConnected(source, target, neighbors);
    }

    /**
     * Returns whether a particular node is member of a cycle or not.
     *
     * @param node The tested node.
     * @return whether {@code node} is member of a cycle or not.
     */
    public boolean nodeIsCycleMember(N node) {
        return nodeIsCycleMember(node, neighbors);
    }

    /**
     * Computes the transitive closure of a node.
     *
     * @param <N> The node type.
     * @param node The starting node.
     * @param neighbors The function that returns the set of nodes that are directly connected to a node.
     * @return The set of nodes that are directly or indirectly connected to {@code node}.
     */
    public static <N> Set<N> computeTransitiveClosure(N node,
                                                      Function<N, Iterable<? extends N>> neighbors) {
        Checks.isNotNull(node, NODE);
        Checks.isNotNull(neighbors, NEIGHBORS);

        final Set<N> done = new HashSet<>();
        final Set<N> todo = new HashSet<>();
        todo.add(node);
        while (!todo.isEmpty()) {
            // Extract next node to process
            final N next = todo.iterator().next();
            todo.remove(next);
            // next was not already processed
            if (!done.contains(next)) {
                done.add(next);
                for (final N n : neighbors.apply(next)) {
                    if (!done.contains(n)) {
                        todo.add(n);
                    }
                }
            }
        }
        return done;
    }

    /**
     * Most generic traversal function.
     *
     * @param <N> The node type.
     * @param node The initial node.
     * @param method The traversal method.
     * @param order The traversal order.
     * @param visitor Visitor that will be invoked for each visited node.
     * @param evaluator An optional evaluator used to control traversal termination.
     * @param neighbors The function that returns the set of nodes that are directly connected to a node.
     * @throws IllegalArgumentException When {@code node}, {@code method}, {@code order}, {@code visitor}
     *             or {@code neighbors} is {@code null}.
     */
    public static <N> void traverse(N node,
                                    TraversalMethod method,
                                    TraversalOrder order,
                                    Visitor<N> visitor,
                                    Evaluator<N> evaluator,
                                    Function<N, Iterable<? extends N>> neighbors) {
        Checks.isNotNull(method, METHOD);
        if (method == TraversalMethod.BREADTH_FIRST) {
            traverseBreadthFirst(node, order, visitor, evaluator, neighbors);
        } else {
            traverseDepthFirst(node, order, visitor, evaluator, neighbors);
        }
    }

    /**
     * Depth first traversal.
     *
     * @param <N> The node type.
     * @param node The initial node.
     * @param order The traversal order.
     * @param visitor Visitor that will be invoked for each visited node.
     * @param evaluator An optional evaluator used to control traversal termination.
     * @param neighbors The function that returns the set of nodes that are directly connected to a node.
     * @throws IllegalArgumentException When {@code node}, {@code order}, {@code visitor}
     *             or {@code neighbors} is {@code null}.
     */
    public static <N> void traverseDepthFirst(N node,
                                              TraversalOrder order,
                                              Visitor<N> visitor,
                                              Evaluator<N> evaluator,
                                              Function<N, Iterable<? extends N>> neighbors) {
        Checks.isNotNull(order, ORDER);
        if (order == TraversalOrder.POST_ORDER) {
            traverseDepthFirstPost(node, visitor, evaluator, neighbors);
        } else {
            traverseDepthFirstPre(node, visitor, evaluator, neighbors);
        }
    }

    /**
     * Depth first pre order traversal.
     *
     * @param <N> The node type.
     * @param node The initial node.
     * @param visitor Visitor that will be invoked for each visited node.
     * @param evaluator An optional evaluator used to control traversal termination.
     * @param neighbors The function that returns the set of nodes that are directly connected to a node.
     * @throws IllegalArgumentException When {@code node}, {@code visitor}
     *             or {@code neighbors} is {@code null}.
     */
    public static <N> void traverseDepthFirstPre(N node,
                                                 Visitor<N> visitor,
                                                 Evaluator<N> evaluator,
                                                 Function<N, Iterable<? extends N>> neighbors) {
        Checks.isNotNull(node, NODE);
        Checks.isNotNull(visitor, VISITOR);
        Checks.isNotNull(neighbors, NEIGHBORS);

        final DepthFirstTraverser<N> traverser = new DepthFirstTraverser<>(visitor,
                                                                           evaluator,
                                                                           neighbors);
        traverser.traversePre(node);
    }

    /**
     * Depth first post order traversal.
     *
     * @param <N> The node type.
     * @param node The initial node.
     * @param visitor Visitor that will be invoked for each visited node.
     * @param evaluator An optional evaluator used to control traversal termination.
     * @param neighbors The function that returns the set of nodes that are directly connected to a node.
     * @throws IllegalArgumentException When {@code node}, {@code visitor}
     *             or {@code neighbors} is {@code null}.
     */
    public static <N> void traverseDepthFirstPost(N node,
                                                  Visitor<N> visitor,
                                                  Evaluator<N> evaluator,
                                                  Function<N, Iterable<? extends N>> neighbors) {
        Checks.isNotNull(node, NODE);
        Checks.isNotNull(visitor, VISITOR);
        Checks.isNotNull(neighbors, NEIGHBORS);

        final DepthFirstTraverser<N> traverser = new DepthFirstTraverser<>(visitor,
                                                                           evaluator,
                                                                           neighbors);
        traverser.traversePost(node);
    }

    /**
     * Breadth first traversal.
     *
     * @param <N> The node type.
     * @param node The initial node.
     * @param order The traversal order.
     * @param visitor Visitor that will be invoked for each visited node.
     * @param evaluator An optional evaluator used to control traversal termination.
     * @param neighbors The function that returns the set of nodes that are directly connected to a node.
     * @throws IllegalArgumentException When {@code node}, {@code order}, {@code visitor}
     *             or {@code neighbors} is {@code null}.
     */
    public static <N> void traverseBreadthFirst(N node,
                                                TraversalOrder order,
                                                Visitor<N> visitor,
                                                Evaluator<N> evaluator,
                                                Function<N, Iterable<? extends N>> neighbors) {
        Checks.isNotNull(order, ORDER);
        if (order == TraversalOrder.POST_ORDER) {
            traverseBreadthFirstPost(node, visitor, evaluator, neighbors);
        } else {
            traverseBreadthFirstPre(node, visitor, evaluator, neighbors);
        }
    }

    /**
     * Breadth first pre order traversal.
     *
     * @param <N> The node type.
     * @param node The initial node.
     * @param visitor Visitor that will be invoked for each visited node.
     * @param evaluator An optional evaluator used to control traversal termination.
     * @param neighbors The function that returns the set of nodes that are directly connected to a node.
     * @throws IllegalArgumentException When {@code node}, {@code visitor}
     *             or {@code neighbors} is {@code null}.
     */
    public static <N> void traverseBreadthFirstPre(N node,
                                                   Visitor<N> visitor,
                                                   Evaluator<N> evaluator,
                                                   Function<N, Iterable<? extends N>> neighbors) {
        Checks.isNotNull(node, NODE);
        Checks.isNotNull(visitor, VISITOR);
        Checks.isNotNull(neighbors, NEIGHBORS);

        final BreadthFirstTraverser<N> traverser =
                new BreadthFirstTraverser<>(evaluator,
                                            neighbors);
        traverser.traversePre(node, visitor);
    }

    /**
     * Breadth first post order traversal.
     *
     * @param <N> The node type.
     * @param node The initial node.
     * @param visitor Visitor that will be invoked for each visited node.
     * @param evaluator An optional evaluator used to control traversal termination.
     * @param neighbors The function that returns the set of nodes that are directly connected to a node.
     * @throws IllegalArgumentException When {@code node}, {@code visitor}
     *             or {@code neighbors} is {@code null}.
     */
    public static <N> void traverseBreadthFirstPost(N node,
                                                    Visitor<N> visitor,
                                                    Evaluator<N> evaluator,
                                                    Function<N, Iterable<? extends N>> neighbors) {
        Checks.isNotNull(node, NODE);
        Checks.isNotNull(visitor, VISITOR);
        Checks.isNotNull(neighbors, NEIGHBORS);

        final BreadthFirstTraverser<N> traverser =
                new BreadthFirstTraverser<>(evaluator,
                                            neighbors);
        traverser.traversePost(node, visitor);
    }

    /**
     * Returns a topological sort of nodes connected to a root node.
     * <p>
     * If node {@code A} is connected to node {@code B}, then {@code A} will come before {@code B} in result.
     *
     * @param <N> The node type.
     * @param node The root node.
     * @param reaction The reaction to adopt if the graph contains loops.
     * @param neighbors The function that returns the set of nodes that are directly connected to a node.
     * @return A topological sort of nodes connected to {@code node} or an empty list.
     * @throws IllegalArgumentException When {@code node} or {@code neighbors} is {@code null},
     *             or when there is a loop and {@code reaction} is {@link FailureReaction#FAIL}.
     */
    public static <N> List<N> topologicalSort(N node,
                                              FailureReaction reaction,
                                              Function<N, Iterable<? extends N>> neighbors) {
        Checks.isNotNull(node, NODE);
        Checks.isNotNull(neighbors, NEIGHBORS);

        final TopologicalSorter<N> sorter = new TopologicalSorter<>(neighbors, node);
        if (sorter.hasLoop) {
            return FailureReaction.onError("Graph contains loops",
                                           LOGGER,
                                           reaction,
                                           Collections.emptyList(),
                                           IllegalArgumentException::new);
        } else {
            return sorter.sorted;
        }
    }

    /**
     * Returns a topological sort of nodes connected to a root node.
     * <p>
     * If node {@code A} is connected to node {@code B}, then {@code A} will come before {@code B} in result.
     *
     * @param <N> The node type.
     * @param node The root node.
     * @param neighbors The function that returns the set of nodes that are directly connected to a node.
     * @return A topological sort of nodes connected to {@code node} or an empty list.
     * @throws IllegalArgumentException When {@code node} or {@code neighbors} is {@code null},
     *             or when there is a loop.
     */
    public static <N> List<N> topologicalSort(N node,
                                              Function<N, Iterable<? extends N>> neighbors) {
        return topologicalSort(node, FailureReaction.FAIL, neighbors);
    }

    /**
     * Returns {@code true} when 2 nodes are connected.
     *
     * @param source The source node.
     * @param target The target node.
     * @param neighbors The function that returns the set of nodes that are directly connected to a node.
     * @return {@code true} when {@code source} is connected to {@code target}.
     */
    public static <N> boolean areConnected(N source,
                                           N target,
                                           Function<N, Iterable<? extends N>> neighbors) {
        final ConnectionDetector<N> detector = new ConnectionDetector<>(neighbors, source, target);
        return detector.areConnected();
    }

    /**
     * Returns whether a particular node is member of a cycle or not.
     *
     * @param node The tested node.
     * @param neighbors The function that returns the set of nodes that are directly connected to a node.
     * @return whether {@code node} is member of a cycle or not.
     */
    public static <N> boolean nodeIsCycleMember(N node,
                                                Function<N, Iterable<? extends N>> neighbors) {
        return areConnected(node, node, neighbors);
    }

    /**
     * Internal class used to compute depth first traversal.
     *
     * @author Damien Carbonne
     *
     * @param <N> The node type.
     */
    private static class DepthFirstTraverser<N> {
        private final Visitor<N> visitor;
        private final Evaluator<N> evaluator;
        private final Function<N, Iterable<? extends N>> neighbors;
        private final Set<N> visited = new HashSet<>();

        public DepthFirstTraverser(Visitor<N> visitor,
                                   Evaluator<N> evaluator,
                                   Function<N, Iterable<? extends N>> neighbors) {
            this.visitor = visitor;
            this.evaluator = evaluator;
            this.neighbors = neighbors;
        }

        public void traversePre(N node) {
            visitor.visit(node);
            if (Evaluator.continueTraversal(evaluator, node)) {
                visited.add(node);
                for (final N succ : neighbors.apply(node)) {
                    if (!visited.contains(succ)) {
                        traversePre(succ);
                    }
                }
            }
        }

        void traversePost(N node) {
            if (Evaluator.continueTraversal(evaluator, node)) {
                visited.add(node);
                for (final N succ : neighbors.apply(node)) {
                    if (!visited.contains(succ)) {
                        traversePost(succ);
                    }
                }
            }
            visitor.visit(node);
        }
    }

    /**
     * Internal class used to compute breadth first traversal.
     *
     * @author Damien Carbonne
     *
     * @param <N> The node type.
     */
    private static class BreadthFirstTraverser<N> {
        private final Evaluator<N> evaluator;
        private final Function<N, Iterable<? extends N>> neighbors;

        public BreadthFirstTraverser(Evaluator<N> evaluator,
                                     Function<N, Iterable<? extends N>> neighbors) {
            this.evaluator = evaluator;
            this.neighbors = neighbors;
        }

        public void traversePre(N from,
                                Visitor<N> visitor) {
            final Deque<N> q = new ArrayDeque<>();
            final Set<N> visited = new HashSet<>();
            q.addLast(from);
            visited.add(from);
            while (!q.isEmpty()) {
                final N node = q.pollFirst();
                visitor.visit(node);
                if (Evaluator.continueTraversal(evaluator, node)) {
                    for (final N y : neighbors.apply(node)) {
                        if (!visited.contains(y)) {
                            visited.add(y);
                            q.addLast(y);
                        }
                    }
                }
            }
        }

        public void traversePost(N from,
                                 Visitor<N> visitor) {
            final List<N> visited = new ArrayList<>();
            traversePre(from, visited::add);
            for (int index = visited.size() - 1; index >= 0; index--) {
                final N node = visited.get(index);
                visitor.visit(node);
            }
        }
    }

    /**
     * Internal class used to compute topological sort.
     *
     * @author Damien Carbonne
     *
     * @param <N> The node type.
     */
    private static class TopologicalSorter<N> {
        private final Function<N, Iterable<? extends N>> neighbors;
        private final Set<N> visited = new HashSet<>();
        private final Set<N> visitedInThisCallStack = new HashSet<>();
        final List<N> sorted = new ArrayList<>();
        boolean hasLoop = false;

        public TopologicalSorter(Function<N, Iterable<? extends N>> neighbors,
                                 N from) {
            this.neighbors = neighbors;
            visit(from);
            Collections.reverse(sorted);
        }

        private void visit(N node) {
            if (visited.contains(node)) {
                if (visitedInThisCallStack.contains(node)) {
                    hasLoop = true;
                }
            } else {
                visited.add(node);
                visitedInThisCallStack.add(node);
                for (final N next : neighbors.apply(node)) {
                    visit(next);
                }
                visitedInThisCallStack.remove(node);
                sorted.add(node);
            }
        }
    }

    /**
     * Internal class used to detect connected nodes.
     *
     * @author Damien Carbonne
     *
     * @param <N> The node type.
     */
    private static class ConnectionDetector<N> {
        boolean found = false;
        final N target;
        final Function<N, Iterable<? extends N>> neighbors;
        final Set<N> visited = new HashSet<>();

        public ConnectionDetector(Function<N, Iterable<? extends N>> neighbors,
                                  N source,
                                  N target) {
            this.target = target;
            this.neighbors = neighbors;
            traverse(source);
        }

        void traverse(N node) {
            if (!visited.contains(node)) {
                visited.add(node);
                for (final N next : neighbors.apply(node)) {
                    if (next.equals(target)) {
                        found = true;
                    } else {
                        traverse(next);
                    }
                }
            }
        }

        public final boolean areConnected() {
            return found;
        }
    }
}