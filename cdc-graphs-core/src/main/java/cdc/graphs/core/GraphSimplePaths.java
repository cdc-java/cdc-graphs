package cdc.graphs.core;

import java.util.HashSet;
import java.util.Set;

import cdc.graphs.EdgeDirection;
import cdc.graphs.GraphAdapter;

/**
 * Utility used to find simple (node) paths in a graph.
 * <p>
 * A simple paths passes once by a node, except for the first and last node that can be the same.
 * <p>
 * <b>WARNING:</b> Node paths may be ambiguous when several edges are allowed between 2 nodes.
 *
 * @author Damien Carbonne
 *
 * @param <N> Node class.
 * @param <E> Edge class.
 */
public class GraphSimplePaths<N, E> extends GraphBase<N, E> {

    public GraphSimplePaths(GraphAdapter<N, E> adapter) {
        super(adapter);
    }

    private void findSimpleNodePathsBetween(N source,
                                            N target,
                                            GraphPath.Builder<N> path,
                                            Set<GraphPath<N>> paths) {
        for (final N next : adapter.getConnectedNodes(source, EdgeDirection.OUTGOING)) {
            if (next == target) {
                final GraphPath.Builder<N> tmp = GraphPath.builder();
                tmp.push(path.getItems());
                tmp.push(next);
                paths.add(tmp.build());
            } else if (!path.contains(next)) {
                path.push(next);
                findSimpleNodePathsBetween(next, target, path, paths);
                path.pop();
            }
        }
    }

    /**
     * Finds all simple node paths between a source and a target.
     *
     * @param source The source node.
     * @param target The target node.
     * @return A set of all simple paths between source and target.
     */
    public Set<GraphPath<N>> findSimpleNodePathsBetween(N source,
                                                        N target) {
        final Set<GraphPath<N>> paths = new HashSet<>();
        final GraphPath.Builder<N> path = GraphPath.builder();
        path.push(source);
        findSimpleNodePathsBetween(source, target, path, paths);
        return paths;
    }

    /**
     * Finds all simple node paths starting from a source node.
     *
     * @param source The source node.
     * @return A set of all simple paths starting from source.
     */
    public Set<GraphPath<N>> findSimpleNodePathsFrom(N source) {
        final Set<GraphPath<N>> result = new HashSet<>();
        for (final N target : getAdapter().getNodes()) {
            final Set<GraphPath<N>> paths = findSimpleNodePathsBetween(source, target);
            result.addAll(paths);
        }
        return result;
    }

    /**
     * Find all simple node paths arriving to a target node.
     *
     * @param target The target node.
     * @return All simple paths arriving to target.
     */
    public Set<GraphPath<N>> findAllSimpleNodePathsTo(N target) {
        final Set<GraphPath<N>> result = new HashSet<>();
        for (final N source : getAdapter().getNodes()) {
            final Set<GraphPath<N>> paths = findSimpleNodePathsBetween(source, target);
            result.addAll(paths);
        }
        return result;
    }

    /**
     * Find all simple node paths in the underlying graph.
     *
     * @return A set of all simple paths in the graph.
     */
    public Set<GraphPath<N>> findAllSimpleNodePaths() {
        final Set<GraphPath<N>> result = new HashSet<>();
        for (final N source : getAdapter().getNodes()) {
            for (final N target : getAdapter().getNodes()) {
                final Set<GraphPath<N>> paths = findSimpleNodePathsBetween(source, target);
                result.addAll(paths);
            }
        }
        return result;
    }
}