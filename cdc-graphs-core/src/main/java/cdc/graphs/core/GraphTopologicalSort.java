package cdc.graphs.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.graphs.EdgeDirection;
import cdc.graphs.GraphAdapter;
import cdc.util.lang.FailureReaction;

/**
 * Utility used to create a topological sort of nodes of a graph.
 * <p>
 * Topological sort is possible if there is no loop.
 *
 * @author Damien Carbonne
 *
 * @param <N> Node class
 * @param <E> Edge class
 */
public class GraphTopologicalSort<N, E> extends GraphBase<N, E> {
    private static final Logger LOGGER = LogManager.getLogger(GraphTopologicalSort.class);

    public GraphTopologicalSort(GraphAdapter<N, E> adapter) {
        super(adapter);
    }

    /**
     * @return A topological sort of nodes of this graph.
     * @throws IllegalArgumentException when the graph contains loops.
     */
    public List<N> topologicalSort() {
        return topologicalSort(FailureReaction.FAIL);
    }

    /**
     * Returns a topological sort of nodes of this graph.
     *
     * @param reaction The reaction to adopt if the graph contains loops.
     * @return A topological sort of nodes of this graph or an empty list.
     * @throws IllegalArgumentException when the graph contains loops and {@code reaction} is {@code FAIL}.
     */
    public List<N> topologicalSort(FailureReaction reaction) {
        final TopologicalSorter<N, E> sorter = new TopologicalSorter<>(adapter);
        if (sorter.hasLoop) {
            return FailureReaction.onError("Graph contains loops",
                                           LOGGER,
                                           reaction,
                                           Collections.emptyList(),
                                           IllegalArgumentException::new);
        } else {
            return sorter.sorted;
        }
    }

    private static class TopologicalSorter<N, E> extends GraphBase<N, E> {
        final List<N> sorted = new ArrayList<>();
        private final Set<N> visited = new HashSet<>();
        boolean hasLoop = false;

        public TopologicalSorter(GraphAdapter<N, E> adapter) {
            super(adapter);
            final Set<N> visitedInThisCallStack = new HashSet<>();
            for (final N node : getAdapter().getNodes()) {
                visitedInThisCallStack.clear();
                visitNoLoopPermitted(node, visitedInThisCallStack);
            }
        }

        private void visitNoLoopPermitted(N node,
                                          Set<N> visitedInThisCallStack) {
            if (visited.contains(node)) {
                if (visitedInThisCallStack.contains(node)) {
                    hasLoop = true;
                }
            } else {
                visited.add(node);
                visitedInThisCallStack.add(node);
                for (final N n1 : adapter.getConnectedNodes(node, EdgeDirection.INGOING)) {
                    visitNoLoopPermitted(n1, visitedInThisCallStack);
                }
                visitedInThisCallStack.remove(node);
                sorted.add(node);
            }
        }
    }
}