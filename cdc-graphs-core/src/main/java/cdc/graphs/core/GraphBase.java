package cdc.graphs.core;

import cdc.graphs.GraphAdapter;
import cdc.util.lang.Checks;

/**
 * Base class used to create graph algorithms.
 *
 * @author Damien Carbonne
 *
 * @param <N> Node class
 * @param <E> Edge class
 */
public class GraphBase<N, E> {
    protected final GraphAdapter<N, E> adapter;

    public GraphBase(GraphAdapter<N, E> adapter) {
        Checks.isNotNull(adapter, "adapter");
        this.adapter = adapter;
    }

    public final GraphAdapter<N, E> getAdapter() {
        return adapter;
    }
}