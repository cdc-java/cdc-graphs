package cdc.graphs.core;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import cdc.graphs.EdgeDirection;
import cdc.graphs.EdgeTip;
import cdc.graphs.GraphAdapter;
import cdc.graphs.impl.ExplicitSubGraph;
import cdc.graphs.impl.ExtensionSubGraph;
import cdc.util.lang.Checks;

/**
 * Utility used to compute transitive closure of a node or a set of nodes.
 *
 * @author Damien Carbonne
 *
 * @param <N> Node type
 * @param <E> Edge type
 */
public class GraphTransitiveClosure<N, E> extends GraphBase<N, E> {
    private static final String NODE = "node";
    private static final String NODES = "nodes";

    public GraphTransitiveClosure(GraphAdapter<N, E> adapter) {
        super(adapter);
    }

    public Set<N> computeTransitiveClosureNodes(N node) {
        Checks.isNotNull(node, NODE);

        final Set<N> result = new HashSet<>();
        addClosure(node, result);
        return result;
    }

    public Set<N> computeTransitiveClosureNodes(Collection<N> nodes) {
        Checks.isNotNull(nodes, NODES);

        final Set<N> result = new HashSet<>();
        for (final N node : nodes) {
            addClosure(node, result);
        }
        return result;
    }

    public Set<N> computeTransitiveClosureNodes(N node,
                                                EdgeDirection direction) {
        Checks.isNotNull(node, NODE);

        final Set<N> resultIn =
                direction != EdgeDirection.OUTGOING ? new HashSet<>() : null;
        final Set<N> resultOut =
                direction != EdgeDirection.INGOING ? new HashSet<>() : null;

        if (resultOut != null) {
            addOutgoingClosure(node, resultOut);
        }
        if (resultIn != null) {
            addIngoingClosure(node, resultIn);
        }
        return merge(resultIn, resultOut);
    }

    public Set<N> computeTransitiveClosureNodes(Collection<N> nodes,
                                                EdgeDirection direction) {
        Checks.isNotNull(nodes, NODES);

        final Set<N> resultIn =
                direction != EdgeDirection.OUTGOING ? new HashSet<>() : null;
        final Set<N> resultOut =
                direction != EdgeDirection.INGOING ? new HashSet<>() : null;

        for (final N node : nodes) {
            if (resultOut != null) {
                addOutgoingClosure(node, resultOut);
            }
            if (resultIn != null) {
                addIngoingClosure(node, resultIn);
            }
        }
        return merge(resultIn, resultOut);
    }

    public ExplicitSubGraph<N, E> computeTransitiveClosure(N node) {
        Checks.isNotNull(node, NODE);

        final ExplicitSubGraph<N, E> result = new ExtensionSubGraph<>(adapter);
        addClosure(node, result);
        return result;
    }

    public ExplicitSubGraph<N, E> computeTransitiveClosure(Collection<N> nodes) {
        Checks.isNotNull(nodes, NODES);

        final ExplicitSubGraph<N, E> result = new ExtensionSubGraph<>(adapter);
        for (final N node : nodes) {
            addClosure(node, result);
        }
        return result;
    }

    /**
     * Computes the transitive closure of a node.
     *
     * @param node The node.
     * @param direction The transitive closure direction. {@code null} means both directions.
     * @return The transitive closure of {@code node}.
     */
    public ExplicitSubGraph<N, E> computeTransitiveClosure(N node,
                                                           EdgeDirection direction) {
        Checks.isNotNull(node, NODE);

        final ExplicitSubGraph<N, E> resultIn =
                direction != EdgeDirection.OUTGOING ? new ExtensionSubGraph<>(adapter) : null;
        final ExplicitSubGraph<N, E> resultOut =
                direction != EdgeDirection.INGOING ? new ExtensionSubGraph<>(adapter) : null;

        if (resultOut != null) {
            addOutgoingClosure(node, resultOut);
        }
        if (resultIn != null) {
            addIngoingClosure(node, resultIn);
        }
        return merge(resultIn, resultOut);
    }

    /**
     * Computes the transitive closure of a collection of nodes.
     *
     * @param nodes The collection of nodes.
     * @param direction The transitive closure direction. {@code null} means both directions.
     * @return The transitive closure of {@code nodes}.
     */
    public ExplicitSubGraph<N, E> computeTransitiveClosure(Collection<N> nodes,
                                                           EdgeDirection direction) {
        Checks.isNotNull(nodes, NODES);

        final ExplicitSubGraph<N, E> resultIn =
                direction != EdgeDirection.OUTGOING ? new ExtensionSubGraph<>(adapter) : null;
        final ExplicitSubGraph<N, E> resultOut =
                direction != EdgeDirection.INGOING ? new ExtensionSubGraph<>(adapter) : null;

        for (final N node : nodes) {
            if (resultOut != null) {
                addOutgoingClosure(node, resultOut);
            }
            if (resultIn != null) {
                addIngoingClosure(node, resultIn);
            }
        }
        return merge(resultIn, resultOut);
    }

    private void addClosure(N node,
                            ExplicitSubGraph<N, E> result) {
        if (!result.containsNode(node)) {
            result.addNode(node);
            for (final E edge : adapter.getEdges(node, EdgeDirection.OUTGOING)) {
                if (!result.containsEdge(edge)) {
                    final N target = adapter.getTip(edge, EdgeTip.TARGET);
                    addOutgoingClosure(target, result);
                    result.addEdge(edge);
                }
            }
            for (final E edge : adapter.getEdges(node, EdgeDirection.INGOING)) {
                if (!result.containsEdge(edge)) {
                    final N source = adapter.getTip(edge, EdgeTip.SOURCE);
                    addOutgoingClosure(source, result);
                    result.addEdge(edge);
                }
            }
        }
    }

    private void addOutgoingClosure(N node,
                                    ExplicitSubGraph<N, E> result) {
        if (!result.containsNode(node)) {
            result.addNode(node);
            for (final E edge : adapter.getEdges(node, EdgeDirection.OUTGOING)) {
                if (!result.containsEdge(edge)) {
                    final N target = adapter.getTip(edge, EdgeTip.TARGET);
                    addOutgoingClosure(target, result);
                    result.addEdge(edge);
                }
            }
        }
    }

    private void addIngoingClosure(N node,
                                   ExplicitSubGraph<N, E> result) {
        if (!result.containsNode(node)) {
            result.addNode(node);
            for (final E edge : adapter.getEdges(node, EdgeDirection.INGOING)) {
                if (!result.containsEdge(edge)) {
                    final N source = adapter.getTip(edge, EdgeTip.SOURCE);
                    addIngoingClosure(source, result);
                    result.addEdge(edge);
                }
            }
        }
    }

    private ExplicitSubGraph<N, E> merge(ExplicitSubGraph<N, E> in,
                                         ExplicitSubGraph<N, E> out) {
        if (in == null) {
            return out;
        } else if (out == null) {
            return in;
        } else {
            for (final N n : in.getNodes()) {
                out.addNode(n);
            }
            for (final E e : in.getEdges()) {
                out.addEdge(e);
            }
            return out;
        }
    }

    private void addClosure(N node,
                            Set<N> result) {
        if (!result.contains(node)) {
            result.add(node);
            for (final E edge : adapter.getEdges(node, EdgeDirection.OUTGOING)) {
                final N target = adapter.getTip(edge, EdgeTip.TARGET);
                addOutgoingClosure(target, result);
            }
            for (final E edge : adapter.getEdges(node, EdgeDirection.INGOING)) {
                final N source = adapter.getTip(edge, EdgeTip.SOURCE);
                addOutgoingClosure(source, result);
            }
        }
    }

    private void addOutgoingClosure(N node,
                                    Set<N> result) {
        if (!result.contains(node)) {
            result.add(node);
            for (final E edge : adapter.getEdges(node, EdgeDirection.OUTGOING)) {
                final N target = adapter.getTip(edge, EdgeTip.TARGET);
                addOutgoingClosure(target, result);
            }
        }
    }

    private void addIngoingClosure(N node,
                                   Set<N> result) {
        if (!result.contains(node)) {
            result.add(node);
            for (final E edge : adapter.getEdges(node, EdgeDirection.INGOING)) {
                final N source = adapter.getTip(edge, EdgeTip.SOURCE);
                addIngoingClosure(source, result);
            }
        }
    }

    private Set<N> merge(Set<N> in,
                         Set<N> out) {
        if (in == null) {
            return out;
        } else if (out == null) {
            return in;
        } else {
            out.addAll(in);
            return out;
        }
    }
}