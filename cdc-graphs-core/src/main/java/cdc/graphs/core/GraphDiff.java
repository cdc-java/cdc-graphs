package cdc.graphs.core;

import java.util.ArrayList;
import java.util.List;

public class GraphDiff<N, E> {
    private final List<N> addedNodes = new ArrayList<>();
    private final List<N> removedNodes = new ArrayList<>();
    private final List<E> addedEdges = new ArrayList<>();
    private final List<E> removedEdges = new ArrayList<>();

    public GraphDiff() {
        super();
    }

    public void addAddedNode(N node) {
        addedNodes.add(node);
    }

    public void addRemovedNode(N node) {
        removedNodes.add(node);
    }

    public void addAddedEdge(E edge) {
        addedEdges.add(edge);
    }

    public void addRemovedEdge(E edge) {
        removedEdges.add(edge);
    }

    public List<N> getAddedNodes() {
        return addedNodes;
    }

    public List<N> getRemovedNodes() {
        return removedNodes;
    }

    public List<E> getAddedEdges() {
        return addedEdges;
    }

    public List<E> getRemovedEdges() {
        return removedEdges;
    }

    public boolean isEmpty() {
        return addedNodes.isEmpty()
                && removedNodes.isEmpty()
                && addedEdges.isEmpty()
                && removedEdges.isEmpty();
    }
}