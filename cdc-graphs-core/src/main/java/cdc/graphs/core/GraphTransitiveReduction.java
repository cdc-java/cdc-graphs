package cdc.graphs.core;

import java.util.HashSet;
import java.util.Set;

import cdc.graphs.EdgeDirection;
import cdc.graphs.EdgeTip;
import cdc.graphs.GraphAdapter;
import cdc.graphs.impl.ExplicitSubGraph;
import cdc.graphs.impl.RestrictionSubGraph;

/**
 *
 * @author Damien Carbonne
 *
 * @param <N> Node class
 * @param <E> Edge class
 */
public class GraphTransitiveReduction<N, E> extends GraphBase<N, E> {
    public GraphTransitiveReduction(GraphAdapter<N, E> adapter) {
        super(adapter);
    }

    public ExplicitSubGraph<N, E> computeTransitiveReduction() {
        final ExplicitSubGraph<N, E> result = new RestrictionSubGraph<>(adapter);

        final Set<N> done = new HashSet<>();

        for (final N node : result.getNodes()) {
            for (final E edge : result.getEdges(node, EdgeDirection.OUTGOING)) {
                final N child = result.getTip(edge, EdgeTip.TARGET);
                done.clear();
                df(result, node, child, done);
            }
        }

        return result;
    }

    private void df(ExplicitSubGraph<N, E> result,
                    N node,
                    N child,
                    Set<N> done) {
        if (!done.contains(child)) {
            for (final E edge : result.getEdges(child, EdgeDirection.OUTGOING)) {
                final N subchild = result.getTip(edge, EdgeTip.TARGET);
                result.removeEdges(node, subchild);
                df(result, node, subchild, done);
                done.add(child);
            }
        }
    }
}