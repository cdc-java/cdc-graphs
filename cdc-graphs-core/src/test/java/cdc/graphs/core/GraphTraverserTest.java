package cdc.graphs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.graphs.EdgeDirection;
import cdc.graphs.TraversalMethod;
import cdc.graphs.TraversalOrder;
import cdc.graphs.impl.tests.TestGraphHeavyEdge;
import cdc.graphs.impl.tests.TestGraphHeavyNode;
import cdc.graphs.impl.tests.TestHeavyGraph;

class GraphTraverserTest {
    private final TestHeavyGraph adapter = new TestHeavyGraph();
    private final GraphTraverser<TestGraphHeavyNode, TestGraphHeavyEdge> traverser = new GraphTraverser<>(adapter);
    private final TestGraphHeavyNode n0 = adapter.getOrCreateNode(0);
    private final TestGraphHeavyNode n1 = adapter.getOrCreateNode(1);
    private final TestGraphHeavyNode n2 = adapter.getOrCreateNode(2);
    private final TestGraphHeavyNode n3 = adapter.getOrCreateNode(3);
    private final TestGraphHeavyNode n4 = adapter.getOrCreateNode(4);
    private final TestGraphHeavyNode n5 = adapter.getOrCreateNode(5);
    private final TestGraphHeavyNode n6 = adapter.getOrCreateNode(6);
    private final TestGraphHeavyNode n7 = adapter.getOrCreateNode(7);
    private final TestGraphHeavyNode n8 = adapter.getOrCreateNode(8);
    private final TestGraphHeavyNode n9 = adapter.getOrCreateNode(9);
    // final GraphToGv<TestGraphHeavyNode, TestGraphHeavyEdge> g2gv = new GraphToGv<>();

    public GraphTraverserTest() {
        adapter.getOrCreateEdge(0, 1);
        adapter.getOrCreateEdge(0, 2);
        adapter.getOrCreateEdge(0, 3);
        adapter.getOrCreateEdge(3, 0);
        adapter.getOrCreateEdge(3, 2);
        adapter.getOrCreateEdge(2, 1);
        adapter.getOrCreateEdge(2, 4);
        adapter.getOrCreateEdge(0, 1);
        adapter.getOrCreateEdge(4, 5);
        adapter.getOrCreateEdge(5, 5);
        adapter.getOrCreateEdge(6, 6);
        adapter.getOrCreateEdge(7, 8);
        adapter.getOrCreateEdge(8, 7);
        adapter.getOrCreateEdge(8, 9);
        // if (LOGGER.isDebugEnabled()) {
        // g2gv.writeNoException(adapter, new File("output"), getClass().getSimpleName() + "-init");
        // }
    }

    private void test(List<TestGraphHeavyNode> expected,
                      TestGraphHeavyNode start,
                      TraversalMethod method,
                      TraversalOrder order,
                      EdgeDirection direction) {
        final List<TestGraphHeavyNode> visited = new ArrayList<>();
        adapter.clearLabels();
        traverser.traverse(start,
                           method,
                           order,
                           direction,
                           n -> {
                               visited.add(n);
                               n.setLabel(Integer.toString(visited.size()));
                           });
        // if (LOGGER.isDebugEnabled()) {
        // g2gv.writeNoException(adapter, new File("output"), getClass().getSimpleName() + "-" + start.getName() + "-" + method +
        // "-" + order + "-" + direction);
        // }
        assertEquals(expected, visited);
    }

    private static List<TestGraphHeavyNode> toList(TestGraphHeavyNode... nodes) {
        final List<TestGraphHeavyNode> result = new ArrayList<>();
        for (final TestGraphHeavyNode node : nodes) {
            result.add(node);
        }
        return result;
    }

    @Test
    void testDepthFirstPreOut() {
        test(toList(n0, n1, n2, n4, n5, n3),
             n0,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n1),
             n1,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n2, n1, n4, n5),
             n2,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n3, n0, n1, n2, n4, n5),
             n3,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n4, n5),
             n4,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n5),
             n5,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n6),
             n6,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n7, n8, n9),
             n7,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n8, n7, n9),
             n8,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n9),
             n9,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);
    }

    @Test
    void testDepthFirstPreIn() {
        test(toList(n0, n3),
             n0,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n1, n0, n3, n2),
             n1,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n2, n0, n3),
             n2,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n3, n0),
             n3,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n4, n2, n0, n3),
             n4,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n5, n4, n2, n0, n3),
             n5,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n6),
             n6,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n7, n8),
             n7,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n8, n7),
             n8,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n9, n8, n7),
             n9,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);
    }

    @Test
    void testDepthFirstPostOut() {
        test(toList(n1, n5, n4, n2, n3, n0),
             n0,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n1),
             n1,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n1, n5, n4, n2),
             n2,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n1, n5, n4, n2, n0, n3),
             n3,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n5, n4),
             n4,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n5),
             n5,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n6),
             n6,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n9, n8, n7),
             n7,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n7, n9, n8),
             n8,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n9),
             n9,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);
    }

    @Test
    void testDepthFirstPostIn() {
        test(toList(n3, n0),
             n0,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n3, n0, n2, n1),
             n1,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n3, n0, n2),
             n2,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n0, n3),
             n3,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n3, n0, n2, n4),
             n4,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n3, n0, n2, n4, n5),
             n5,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n6),
             n6,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n8, n7),
             n7,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n7, n8),
             n8,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n7, n8, n9),
             n9,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);
    }

    @Test
    void testBreadthFirstPreOut() {
        test(toList(n0, n1, n2, n3, n4, n5),
             n0,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n1),
             n1,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n2, n1, n4, n5),
             n2,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n3, n0, n2, n1, n4, n5),
             n3,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n4, n5),
             n4,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n5),
             n5,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n6),
             n6,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n7, n8, n9),
             n7,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n8, n7, n9),
             n8,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n9),
             n9,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);
    }

    @Test
    void testBreadthFirstPreIn() {
        test(toList(n0, n3),
             n0,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n1, n0, n2, n3),
             n1,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n2, n0, n3),
             n2,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n3, n0),
             n3,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n4, n2, n0, n3),
             n4,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n5, n4, n2, n0, n3),
             n5,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n6),
             n6,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n7, n8),
             n7,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n8, n7),
             n8,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(n9, n8, n7),
             n9,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);
    }

    @Test
    void testBreadthFirstPostOut() {
        test(toList(n5, n4, n3, n2, n1, n0),
             n0,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n1),
             n1,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n5, n4, n1, n2),
             n2,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n5, n4, n1, n2, n0, n3),
             n3,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n5, n4),
             n4,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n5),
             n5,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n6),
             n6,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n9, n8, n7),
             n7,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n9, n7, n8),
             n8,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(n9),
             n9,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);
    }

    @Test
    void testBreadthFirstPostIn() {
        test(toList(n3, n0),
             n0,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n3, n2, n0, n1),
             n1,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n3, n0, n2),
             n2,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n0, n3),
             n3,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n3, n0, n2, n4),
             n4,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n3, n0, n2, n4, n5),
             n5,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n6),
             n6,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n8, n7),
             n7,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n7, n8),
             n8,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(n7, n8, n9),
             n9,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);
    }
}