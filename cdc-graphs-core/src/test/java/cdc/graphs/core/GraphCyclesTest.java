package cdc.graphs.core;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import cdc.graphs.impl.tests.TestGraphHeavyEdge;
import cdc.graphs.impl.tests.TestGraphHeavyNode;
import cdc.graphs.impl.tests.TestHeavyGraph;

class GraphCyclesTest {
    @Test
    void testAreConnected() {
        final TestHeavyGraph g = new TestHeavyGraph();
        final GraphCycles<TestGraphHeavyNode, TestGraphHeavyEdge> gc = new GraphCycles<>(g);
        // gc.setPrefix(getClass().getSimpleName() + "-");

        assertFalse(gc.containsCycles());
        for (int i = 0; i < 10; i++) {
            g.getOrCreateNode(i);
        }
        assertFalse(gc.containsCycles());
        assertFalse(gc.areConnected(g.getNode(0), g.getNode(0)));
        assertFalse(gc.areConnected(g.getNode(0), g.getNode(1)));
        assertFalse(gc.nodeIsCycleMember(g.getNode(0)));

        g.getOrCreateEdge(0, 0);
        assertTrue(gc.containsCycles());
        assertTrue(gc.areConnected(g.getNode(0), g.getNode(0)));
        assertFalse(gc.areConnected(g.getNode(0), g.getNode(1)));
        assertTrue(gc.nodeIsCycleMember(g.getNode(0)));
        assertFalse(gc.nodeIsCycleMember(g.getNode(1)));

        g.removeEdge(0, 0);
        assertFalse(gc.areConnected(g.getNode(0), g.getNode(0)));

        g.getOrCreateEdge(0, 1);
        g.getOrCreateEdge(1, 2);
        g.getOrCreateEdge(2, 3);
        assertFalse(gc.containsCycles());
        assertTrue(gc.areConnected(g.getNode(0), g.getNode(1)));
        assertTrue(gc.areConnected(g.getNode(0), g.getNode(2)));
        assertTrue(gc.areConnected(g.getNode(0), g.getNode(3)));
        assertTrue(gc.areConnected(g.getNode(1), g.getNode(2)));
        assertTrue(gc.areConnected(g.getNode(1), g.getNode(3)));
        assertTrue(gc.areConnected(g.getNode(2), g.getNode(3)));

        g.getOrCreateEdge(3, 0);
        assertTrue(gc.containsCycles());
        assertTrue(gc.edgeIsCycleMember(g.getEdge(0, 1)));
        assertTrue(gc.edgeIsCycleMember(g.getEdge(1, 2)));

        final Set<TestGraphHeavyNode> nodes = new HashSet<>();
        final Set<TestGraphHeavyEdge> edges = new HashSet<>();
        gc.computeCyclesMembers(nodes, edges);
        assertTrue(nodes.contains(g.getNode(0)));
        assertTrue(nodes.contains(g.getNode(1)));
        assertTrue(nodes.contains(g.getNode(2)));
        assertTrue(nodes.contains(g.getNode(3)));
        assertFalse(nodes.contains(g.getNode(4)));
    }
}