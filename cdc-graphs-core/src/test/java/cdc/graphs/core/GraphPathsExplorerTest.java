package cdc.graphs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.graphs.EdgeDirection;
import cdc.graphs.impl.tests.TestGraphHeavyEdge;
import cdc.graphs.impl.tests.TestGraphHeavyNode;
import cdc.graphs.impl.tests.TestHeavyGraph;

class GraphPathsExplorerTest {
    protected static final Logger LOGGER = LogManager.getLogger(GraphPathsExplorerTest.class);

    private static class PathAccumulator {
        protected final Set<GraphPath<TestGraphHeavyEdge>> paths = new HashSet<>();

        public PathAccumulator() {
        }

        public void add(GraphPath<TestGraphHeavyEdge> path) {
            LOGGER.info("add({})", path);
            paths.add(path);
        }
    }

    private static GraphPath<TestGraphHeavyEdge> p(TestGraphHeavyEdge... edges) {
        return GraphPath.builder(TestGraphHeavyEdge.class)
                        .push(edges)
                        .build();
    }

    @SuppressWarnings("unchecked")
    private static Set<GraphPath<TestGraphHeavyEdge>> toSet(GraphPath<TestGraphHeavyEdge>... paths) {
        final Set<GraphPath<TestGraphHeavyEdge>> result = new HashSet<>();
        for (final GraphPath<TestGraphHeavyEdge> path : paths) {
            result.add(path);
        }
        return result;
    }

    @SafeVarargs
    private static void check(TestHeavyGraph adapter,
                              TestGraphHeavyNode node,
                              EdgeDirection direction,
                              GraphPath<TestGraphHeavyEdge>... expected) {
        LOGGER.info("============================");
        final PathAccumulator accumulator = new PathAccumulator();
        final GraphPathsExplorer<TestGraphHeavyNode, TestGraphHeavyEdge> explorer = new GraphPathsExplorer<>(adapter);
        explorer.explore(node, direction, p -> accumulator.add(p));
        assertEquals(toSet(expected), accumulator.paths);
    }

    @Test
    void testExploreCycle() {
        final TestHeavyGraph adapter = new TestHeavyGraph();
        final GraphPathsExplorer<TestGraphHeavyNode, TestGraphHeavyEdge> explorer = new GraphPathsExplorer<>(adapter);
        final TestGraphHeavyNode n0 = adapter.getOrCreateNode(0);
        final PathAccumulator accumulator = new PathAccumulator();
        adapter.getOrCreateEdge(0, 0);
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         explorer.explore(n0, EdgeDirection.OUTGOING, p -> accumulator.add(p));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         explorer.explore(n0, EdgeDirection.INGOING, p -> accumulator.add(p));
                     });
    }

    @Test
    void testExplore1() {
        final TestHeavyGraph adapter = new TestHeavyGraph();
        final TestGraphHeavyNode n0 = adapter.getOrCreateNode(0);
        final TestGraphHeavyNode n1 = adapter.getOrCreateNode(1);
        final TestGraphHeavyEdge e01a = adapter.getOrCreateEdge(0, 1, "(a)");
        check(adapter, n0, EdgeDirection.OUTGOING, p(e01a));
        check(adapter, n0, EdgeDirection.INGOING);
        check(adapter, n1, EdgeDirection.OUTGOING);
        check(adapter, n1, EdgeDirection.INGOING, p(e01a));

        final TestGraphHeavyEdge e01b = adapter.getOrCreateEdge(0, 1, "(b)");
        check(adapter, n0, EdgeDirection.OUTGOING, p(e01a), p(e01b));
        check(adapter, n0, EdgeDirection.INGOING);
        check(adapter, n1, EdgeDirection.OUTGOING);
        check(adapter, n1, EdgeDirection.INGOING, p(e01a), p(e01b));

        final TestGraphHeavyNode n2 = adapter.getOrCreateNode(2);
        final TestGraphHeavyEdge e12 = adapter.getOrCreateEdge(1, 2);
        check(adapter, n0, EdgeDirection.OUTGOING, p(e01a, e12), p(e01b, e12));
        check(adapter, n0, EdgeDirection.INGOING);
        check(adapter, n1, EdgeDirection.OUTGOING, p(e12));
        check(adapter, n1, EdgeDirection.INGOING, p(e01a), p(e01b));
        check(adapter, n2, EdgeDirection.OUTGOING);
        check(adapter, n2, EdgeDirection.INGOING, p(e12, e01a), p(e12, e01b));

        final TestGraphHeavyNode n3 = adapter.getOrCreateNode(3);
        final TestGraphHeavyEdge e23a = adapter.getOrCreateEdge(2, 3, "a");
        final TestGraphHeavyEdge e23b = adapter.getOrCreateEdge(2, 3, "b");
        check(adapter, n0, EdgeDirection.OUTGOING, p(e01a, e12, e23a), p(e01a, e12, e23b), p(e01b, e12, e23a), p(e01b, e12, e23b));
        check(adapter, n0, EdgeDirection.INGOING);
        check(adapter, n1, EdgeDirection.OUTGOING, p(e12, e23a), p(e12, e23b));
        check(adapter, n1, EdgeDirection.INGOING, p(e01a), p(e01b));
        check(adapter, n2, EdgeDirection.OUTGOING, p(e23a), p(e23b));
        check(adapter, n2, EdgeDirection.INGOING, p(e12, e01a), p(e12, e01b));
        check(adapter, n3, EdgeDirection.OUTGOING);
        check(adapter, n3, EdgeDirection.INGOING, p(e23a, e12, e01a), p(e23a, e12, e01b), p(e23b, e12, e01a), p(e23b, e12, e01b));
    }
}