package cdc.graphs.core;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import cdc.graphs.GraphAdapter;
import cdc.graphs.impl.tests.TestGraphHeavyEdge;
import cdc.graphs.impl.tests.TestGraphHeavyNode;
import cdc.graphs.impl.tests.TestHeavyGraph;

class GraphTransitiveReductionTest {
    @Test
    void test() {
        final TestHeavyGraph g = new TestHeavyGraph();
        final GraphCycles<TestGraphHeavyNode, TestGraphHeavyEdge> gc = new GraphCycles<>(g);
        final GraphTransitiveReduction<TestGraphHeavyNode, TestGraphHeavyEdge> gtr = new GraphTransitiveReduction<>(g);

        final int max = 10;
        for (int index = 0; index < 3; index++) {
            final int base = index * 100;
            for (int i1 = base; i1 < base + max; i1++) {
                for (int i2 = i1 + 1; i2 < base + max; i2++) {
                    g.getOrCreateEdge(i1, i2);
                }
            }
        }

        final GraphAdapter<TestGraphHeavyNode, TestGraphHeavyEdge> reduction = gtr.computeTransitiveReduction();
        final GraphCycles<TestGraphHeavyNode, TestGraphHeavyEdge> gctr = new GraphCycles<>(reduction);

        for (final TestGraphHeavyNode source : g.getNodes()) {
            for (final TestGraphHeavyNode target : g.getNodes()) {
                assertSame(gc.areConnected(source, target), gctr.areConnected(source, target));
            }
        }
    }
}