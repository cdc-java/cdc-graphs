package cdc.graphs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.graphs.EdgeDirection;
import cdc.graphs.TraversalMethod;
import cdc.graphs.TraversalOrder;
import cdc.graphs.impl.tests.TestTree;
import cdc.graphs.impl.tests.TestTreeNode;
import cdc.util.function.Evaluation;

class TreeTraverserTest {
    private final TestTree adapter = new TestTree();
    private final TreeTraverser<TestTreeNode> traverser = new TreeTraverser<>(adapter);
    private final TestTreeNode c0 = new TestTreeNode("C0");
    private final TestTreeNode c1 = new TestTreeNode("C1", c0);
    private final TestTreeNode c11 = new TestTreeNode("C1.1", c1);
    private final TestTreeNode c12 = new TestTreeNode("C1.2", c1);
    private final TestTreeNode c13 = new TestTreeNode("C1.3", c1);
    private final TestTreeNode c131 = new TestTreeNode("C1.3.1", c13);
    private final TestTreeNode c2 = new TestTreeNode("C2", c0);
    private final TestTreeNode c21 = new TestTreeNode("C2.1", c2);
    private final TestTreeNode c22 = new TestTreeNode("C2.2", c2);
    private final TestTreeNode c23 = new TestTreeNode("C2.3", c2);

    private void test(List<TestTreeNode> expected,
                      TestTreeNode start,
                      TraversalMethod method,
                      TraversalOrder order,
                      EdgeDirection direction) {
        final List<TestTreeNode> visited = new ArrayList<>();
        traverser.traverse(start,
                           method,
                           order,
                           direction,
                           n -> visited.add(n));
        assertEquals(expected, visited);
    }

    private void test(List<TestTreeNode> expected,
                      TestTreeNode start,
                      TestTreeNode stop,
                      TraversalMethod method,
                      TraversalOrder order,
                      EdgeDirection direction) {
        final List<TestTreeNode> visited = new ArrayList<>();
        traverser.traverse(start,
                           method,
                           order,
                           direction,
                           n -> visited.add(n),
                           n -> n == stop ? Evaluation.PRUNE : Evaluation.CONTINUE);
        assertEquals(expected, visited);
    }

    private static List<TestTreeNode> toList(TestTreeNode... nodes) {
        final List<TestTreeNode> result = new ArrayList<>();
        for (final TestTreeNode node : nodes) {
            result.add(node);
        }
        return result;
    }

    @Test
    void testDepthFirstPre() {
        test(toList(c0, c1, c11, c12, c13, c131, c2, c21, c22, c23),
             c0,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c1, c11, c12, c13, c131),
             c1,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c0),
             c0,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(c1, c0),
             c1,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(c131, c13, c1, c0),
             c131,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(c13, c1, c0),
             c13,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);
    }

    @Test
    void testDepthFirstPreEvaluator() {
        test(toList(c0, c1, c11, c12, c13, c2, c21, c22, c23),
             c0,
             c13,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c1),
             c1,
             c1,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c1, c11, c12, c13),
             c1,
             c13,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c131, c13, c1),
             c131,
             c1,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(c131, c13),
             c131,
             c13,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(c131),
             c131,
             c131,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);
    }

    @Test
    void testDepthFirstPost() {
        test(toList(c11, c12, c131, c13, c1, c21, c22, c23, c2, c0),
             c0,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c11, c12, c131, c13, c1),
             c1,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c0),
             c0,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(c0, c1),
             c1,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(c0, c1, c13, c131),
             c131,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(c0, c1, c13),
             c13,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);
    }

    @Test
    void testDepthFirstPostEvaluator() {
        test(toList(c0),
             c0,
             c0,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c1, c21, c22, c23, c2, c0),
             c0,
             c1,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c11, c12, c13, c1, c21, c22, c23, c2, c0),
             c0,
             c13,
             TraversalMethod.DEPTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);
    }

    @Test
    void testBreadthFirstPre() {
        test(toList(c0, c1, c2, c11, c12, c13, c21, c22, c23, c131),
             c0,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c1, c11, c12, c13, c131),
             c1,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c0),
             c0,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);

        test(toList(c1, c0),
             c1,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.INGOING);
    }

    @Test
    void testBreadthFirstPreEvaluator() {
        test(toList(c0),
             c0,
             c0,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c0, c1, c2, c21, c22, c23),
             c0,
             c1,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c0, c1, c2, c11, c12, c13, c131),
             c0,
             c2,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c0, c1, c2, c11, c12, c13, c21, c22, c23),
             c0,
             c13,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c1),
             c1,
             c1,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c1, c11, c12, c13, c131),
             c1,
             c11,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c1, c11, c12, c13),
             c1,
             c13,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.PRE_ORDER,
             EdgeDirection.OUTGOING);
    }

    @Test
    void testBreadthFirstPost() {
        test(toList(c131, c23, c22, c21, c13, c12, c11, c2, c1, c0),
             c0,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c131, c13, c12, c11, c1),
             c1,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c0),
             c0,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(c0, c1),
             c1,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);
    }

    @Test
    void testBreadthFirstPostEvaluator() {
        test(toList(c0),
             c0,
             c0,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c23, c22, c21, c2, c1, c0),
             c0,
             c1,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c131, c13, c12, c11, c2, c1, c0),
             c0,
             c2,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c1),
             c1,
             c1,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c13, c12, c11, c1),
             c1,
             c13,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.OUTGOING);

        test(toList(c0),
             c0,
             c0,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);

        test(toList(c0, c1),
             c1,
             c0,
             TraversalMethod.BREADTH_FIRST,
             TraversalOrder.POST_ORDER,
             EdgeDirection.INGOING);
    }
}