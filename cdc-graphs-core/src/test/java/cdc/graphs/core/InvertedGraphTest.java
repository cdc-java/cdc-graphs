package cdc.graphs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.graphs.impl.InvertedGraph;
import cdc.graphs.impl.tests.TestGraphHeavyEdge;
import cdc.graphs.impl.tests.TestGraphHeavyNode;
import cdc.graphs.impl.tests.TestHeavyGraph;

class InvertedGraphTest {
    @Test
    void test() {
        final TestHeavyGraph g = new TestHeavyGraph();
        // final GraphBasicQueries<TestGraphHeavyNode, TestGraphHeavyEdge> gbq = new GraphBasicQueries<>(g);
        final GraphCycles<TestGraphHeavyNode, TestGraphHeavyEdge> gc = new GraphCycles<>(g);
        // gbq.setPrefix(getClass().getSimpleName() + "-");
        final InvertedGraph<TestGraphHeavyNode, TestGraphHeavyEdge> ig = new InvertedGraph<>(g);
        final GraphCycles<TestGraphHeavyNode, TestGraphHeavyEdge> igc = new GraphCycles<>(ig);

        final int max = 10;
        for (int index = 0; index < 3; index++) {
            final int base = index * 100;
            for (int i1 = base; i1 < base + max; i1++) {
                for (int i2 = i1 + 1; i2 < base + max; i2++) {
                    g.getOrCreateEdge(i1, i2);
                }
            }
        }
        // gbq.printToGv(g, "init");
        // gbq.printToGv(ig, "inverted");

        for (final TestGraphHeavyNode source : g.getNodes()) {
            for (final TestGraphHeavyNode target : g.getNodes()) {
                assertEquals(gc.areConnected(source, target), igc.areConnected(target, source));
            }
        }
        for (final TestGraphHeavyNode node : g.getNodes()) {
            assertTrue(ig.containsNode(node));
        }
        for (final TestGraphHeavyNode node : ig.getNodes()) {
            assertTrue(g.containsNode(node));
        }
        for (final TestGraphHeavyEdge edge : g.getEdges()) {
            assertTrue(ig.containsEdge(edge));
        }
        for (final TestGraphHeavyEdge edge : ig.getEdges()) {
            assertTrue(g.containsEdge(edge));
        }
    }
}