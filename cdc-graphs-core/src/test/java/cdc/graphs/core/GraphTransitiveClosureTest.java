package cdc.graphs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.graphs.EdgeDirection;
import cdc.graphs.impl.tests.TestGraphHeavyEdge;
import cdc.graphs.impl.tests.TestGraphHeavyNode;
import cdc.graphs.impl.tests.TestHeavyGraph;

class GraphTransitiveClosureTest {
    private static TestHeavyGraph buildGraph() {
        final TestHeavyGraph g = new TestHeavyGraph();
        g.createNode("A1");
        g.createNode("A2");
        g.createNode("A3");
        g.createNode("A3A1");
        g.createNode("A3A2");
        g.createNode("A3B1");
        g.createNode("A3B2");
        g.createNode("A4");
        g.createNode("A5");

        g.createEdge("A2", "A1");
        g.createEdge("A3", "A2");
        g.createEdge("A3A1", "A3");
        g.createEdge("A3A2", "A3A1");
        g.createEdge("A3B1", "A3");
        g.createEdge("A3B2", "A3B1");
        g.createEdge("A4", "A3A2");
        g.createEdge("A4", "A3B2");
        g.createEdge("A5", "A4");

        g.createNode("B1");
        g.createNode("B2");
        g.createNode("B3");

        g.createEdge("B2", "B1");
        g.createEdge("B3", "B2");

        g.createNode("C1");
        return g;
    }

    @Test
    void testOutgoingNodes() {
        final TestHeavyGraph g = buildGraph();
        final GraphTransitiveClosure<TestGraphHeavyNode, TestGraphHeavyEdge> gtc = new GraphTransitiveClosure<>(g);

        final EdgeDirection direction = EdgeDirection.OUTGOING;
        assertEquals(g.getNodes("A1"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A1"), direction));
        assertEquals(g.getNodes("A1", "A2"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A2"), direction));
        assertEquals(g.getNodes("A1", "A2", "A3"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A3"), direction));
        assertEquals(g.getNodes("A1", "A2", "A3", "A3A1"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A3A1"), direction));
        assertEquals(g.getNodes("A1", "A2", "A3", "A3B1"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A3B1"), direction));
        assertEquals(g.getNodes("A1", "A2", "A3", "A3A1", "A3A2"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A3A2"), direction));
        assertEquals(g.getNodes("A1", "A2", "A3", "A3B1", "A3B2"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A3B2"), direction));
        assertEquals(g.getNodes("A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A4"), direction));
        assertEquals(g.getNodes("A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A5"), direction));

        assertEquals(g.getNodes("B1"),
                     gtc.computeTransitiveClosureNodes(g.getNode("B1"), direction));
        assertEquals(g.getNodes("B1", "B2"),
                     gtc.computeTransitiveClosureNodes(g.getNode("B2"), direction));
        assertEquals(g.getNodes("B1", "B2", "B3"),
                     gtc.computeTransitiveClosureNodes(g.getNode("B3"), direction));

        assertEquals(g.getNodes("C1"),
                     gtc.computeTransitiveClosureNodes(g.getNode("C1"), direction));
    }

    @Test
    void testIngoingNodes() {
        final TestHeavyGraph g = buildGraph();
        final GraphTransitiveClosure<TestGraphHeavyNode, TestGraphHeavyEdge> gtc = new GraphTransitiveClosure<>(g);

        final EdgeDirection direction = EdgeDirection.INGOING;
        assertEquals(g.getNodes("A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A1"), direction));
        assertEquals(g.getNodes("A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A2"), direction));
        assertEquals(g.getNodes("A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A3"), direction));
        assertEquals(g.getNodes("A3A1", "A3A2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A3A1"), direction));
        assertEquals(g.getNodes("A3B1", "A3B2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A3B1"), direction));
        assertEquals(g.getNodes("A3A2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A3A2"), direction));
        assertEquals(g.getNodes("A3B2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A3B2"), direction));
        assertEquals(g.getNodes("A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A4"), direction));
        assertEquals(g.getNodes("A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A5"), direction));

        assertEquals(g.getNodes("B1", "B2", "B3"),
                     gtc.computeTransitiveClosureNodes(g.getNode("B1"), direction));
        assertEquals(g.getNodes("B2", "B3"),
                     gtc.computeTransitiveClosureNodes(g.getNode("B2"), direction));
        assertEquals(g.getNodes("B3"),
                     gtc.computeTransitiveClosureNodes(g.getNode("B3"), direction));

        assertEquals(g.getNodes("C1"),
                     gtc.computeTransitiveClosureNodes(g.getNode("C1"), direction));
    }

    @Test
    void testIngoingAndOutgoingNodes() {
        final TestHeavyGraph g = buildGraph();
        final GraphTransitiveClosure<TestGraphHeavyNode, TestGraphHeavyEdge> gtc = new GraphTransitiveClosure<>(g);

        final EdgeDirection direction = null;

        assertEquals(g.getNodes("A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A1"), direction));
        assertEquals(g.getNodes("A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A2"), direction));
        assertEquals(g.getNodes("A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A3"), direction));
        assertEquals(g.getNodes("A1", "A2", "A3", "A3A1", "A3A2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A3A1"), direction));
        assertEquals(g.getNodes("A1", "A2", "A3", "A3B1", "A3B2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A3B1"), direction));
        assertEquals(g.getNodes("A1", "A2", "A3", "A3A1", "A3A2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A3A2"), direction));
        assertEquals(g.getNodes("A1", "A2", "A3", "A3B1", "A3B2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A3B2"), direction));
        assertEquals(g.getNodes("A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A4"), direction));
        assertEquals(g.getNodes("A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"),
                     gtc.computeTransitiveClosureNodes(g.getNode("A5"), direction));

        assertEquals(g.getNodes("B1", "B2", "B3"),
                     gtc.computeTransitiveClosureNodes(g.getNode("B1"), direction));
        assertEquals(g.getNodes("B1", "B2", "B3"),
                     gtc.computeTransitiveClosureNodes(g.getNode("B2"), direction));
        assertEquals(g.getNodes("B1", "B2", "B3"),
                     gtc.computeTransitiveClosureNodes(g.getNode("B3"), direction));

        assertEquals(g.getNodes("C1"),
                     gtc.computeTransitiveClosureNodes(g.getNode("C1"), direction));
    }
}