package cdc.graphs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.graphs.GraphAdapter;
import cdc.graphs.impl.tests.TestGraphHeavyEdge;
import cdc.graphs.impl.tests.TestGraphHeavyNode;
import cdc.graphs.impl.tests.TestHeavyGraph;

class GraphConnectedComponentsTest {
    @Test
    void test() {
        final TestHeavyGraph g = new TestHeavyGraph();
        // final GraphBasicQueries<TestGraphHeavyNode, TestGraphHeavyEdge> gbq = new GraphBasicQueries<>(g);

        final int ccmax = 3;
        final int max = 10;
        for (int index = 0; index < ccmax; index++) {
            final int base = index * 100;
            for (int i1 = base; i1 < base + max; i1++) {
                for (int i2 = i1 + 1; i2 < base + max; i2++) {
                    g.getOrCreateEdge(i1, i2);
                }
            }
        }

        final GraphConnectedComponents<TestGraphHeavyNode, TestGraphHeavyEdge> gcc = new GraphConnectedComponents<>(g);
        final List<GraphAdapter<TestGraphHeavyNode, TestGraphHeavyEdge>> cc = gcc.computeConnectedComponents();
        // int index = 0;
        // for (final GraphAdapter<TestGraphHeavyNode, TestGraphHeavyEdge> adapter : cc) {
        // final GraphBasicQueries<TestGraphHeavyNode, TestGraphHeavyEdge> gbq1 = new GraphBasicQueries<>(adapter);
        // gbq1.setPrefix(getClass().getSimpleName() + "-");
        // gbq1.printToGv(adapter, "cc" + index);
        // index++;
        // }

        assertEquals(ccmax, cc.size());
    }
}