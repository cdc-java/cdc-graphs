package cdc.graphs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;

import cdc.graphs.EdgeDirection;
import cdc.graphs.TraversalMethod;
import cdc.graphs.TraversalOrder;
import cdc.graphs.impl.tests.TestGraphHeavyNode;
import cdc.graphs.impl.tests.TestHeavyGraph;
import cdc.graphs.impl.tests.TestTreeNode;
import cdc.util.function.Evaluation;
import cdc.util.function.Evaluator;

class SlimGraphTest {
    private final SlimGraph<TestTreeNode> sg = new SlimGraph<>(TestTreeNode::getChildren);
    private final TestTreeNode c0 = new TestTreeNode("C0");
    private final TestTreeNode c1 = new TestTreeNode("C1", c0);
    private final TestTreeNode c11 = new TestTreeNode("C1.1", c1);
    private final TestTreeNode c12 = new TestTreeNode("C1.2", c1);
    private final TestTreeNode c13 = new TestTreeNode("C1.3", c1);
    private final TestTreeNode c131 = new TestTreeNode("C1.3.1", c13);
    private final TestTreeNode c2 = new TestTreeNode("C2", c0);
    private final TestTreeNode c21 = new TestTreeNode("C2.1", c2);
    private final TestTreeNode c22 = new TestTreeNode("C2.2", c2);
    private final TestTreeNode c23 = new TestTreeNode("C2.3", c2);

    private void testTraversal(List<TestTreeNode> expected,
                               TestTreeNode start,
                               TraversalMethod method,
                               TraversalOrder order) {
        final List<TestTreeNode> visited = new ArrayList<>();
        sg.traverse(start,
                    method,
                    order,
                    n -> visited.add(n),
                    Evaluator.continueTraversal());
        assertEquals(expected, visited);
    }

    private void testTraversal(List<TestTreeNode> expected,
                               TestTreeNode start,
                               TestTreeNode stop,
                               TraversalMethod method,
                               TraversalOrder order) {
        final List<TestTreeNode> visited = new ArrayList<>();
        sg.traverse(start,
                    method,
                    order,
                    n -> visited.add(n),
                    n -> n == stop ? Evaluation.PRUNE : Evaluation.CONTINUE);
        assertEquals(expected, visited);
    }

    private void testTransitiveClosure(Set<TestTreeNode> expected,
                                       TestTreeNode start) {
        final Set<TestTreeNode> actual = sg.computeTransitiveClosure(start);
        assertEquals(expected, actual);
    }

    private void testTopologicalSort(TestTreeNode start,
                                     int expectedLength) {
        final List<TestTreeNode> actual = sg.topologicalSort(start);
        assertSame(expectedLength, actual.size());
        for (int index1 = 0; index1 < actual.size(); index1++) {
            final TestTreeNode n1 = actual.get(index1);
            for (int index2 = index1 + 1; index2 < actual.size(); index2++) {
                final TestTreeNode n2 = actual.get(index2);
                final boolean connected21 = sg.areConnected(n2, n1);
                assertFalse(connected21);
            }
        }
    }

    private static List<TestTreeNode> toList(TestTreeNode... nodes) {
        final List<TestTreeNode> result = new ArrayList<>();
        Collections.addAll(result, nodes);
        return result;
    }

    private static Set<TestTreeNode> toSet(TestTreeNode... nodes) {
        final Set<TestTreeNode> result = new HashSet<>();
        Collections.addAll(result, nodes);
        return result;
    }

    @Test
    void testDepthFirstPre() {
        testTraversal(toList(c0, c1, c11, c12, c13, c131, c2, c21, c22, c23),
                      c0,
                      TraversalMethod.DEPTH_FIRST,
                      TraversalOrder.PRE_ORDER);

        testTraversal(toList(c1, c11, c12, c13, c131),
                      c1,
                      TraversalMethod.DEPTH_FIRST,
                      TraversalOrder.PRE_ORDER);
    }

    @Test
    void testDepthFirstPreEvaluator() {
        testTraversal(toList(c0, c1, c11, c12, c13, c2, c21, c22, c23),
                      c0,
                      c13,
                      TraversalMethod.DEPTH_FIRST,
                      TraversalOrder.PRE_ORDER);

        testTraversal(toList(c1),
                      c1,
                      c1,
                      TraversalMethod.DEPTH_FIRST,
                      TraversalOrder.PRE_ORDER);

        testTraversal(toList(c1, c11, c12, c13),
                      c1,
                      c13,
                      TraversalMethod.DEPTH_FIRST,
                      TraversalOrder.PRE_ORDER);
    }

    @Test
    void testDepthFirstPost() {
        testTraversal(toList(c11, c12, c131, c13, c1, c21, c22, c23, c2, c0),
                      c0,
                      TraversalMethod.DEPTH_FIRST,
                      TraversalOrder.POST_ORDER);

        testTraversal(toList(c11, c12, c131, c13, c1),
                      c1,
                      TraversalMethod.DEPTH_FIRST,
                      TraversalOrder.POST_ORDER);
    }

    @Test
    void testDepthFirstPostEvaluator() {
        testTraversal(toList(c0),
                      c0,
                      c0,
                      TraversalMethod.DEPTH_FIRST,
                      TraversalOrder.POST_ORDER);

        testTraversal(toList(c1, c21, c22, c23, c2, c0),
                      c0,
                      c1,
                      TraversalMethod.DEPTH_FIRST,
                      TraversalOrder.POST_ORDER);

        testTraversal(toList(c11, c12, c13, c1, c21, c22, c23, c2, c0),
                      c0,
                      c13,
                      TraversalMethod.DEPTH_FIRST,
                      TraversalOrder.POST_ORDER);
    }

    @Test
    void testBreadthFirstPre() {
        testTraversal(toList(c0, c1, c2, c11, c12, c13, c21, c22, c23, c131),
                      c0,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.PRE_ORDER);

        testTraversal(toList(c1, c11, c12, c13, c131),
                      c1,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.PRE_ORDER);
    }

    @Test
    void testBreadthFirstPreEvaluator() {
        testTraversal(toList(c0),
                      c0,
                      c0,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.PRE_ORDER);

        testTraversal(toList(c0, c1, c2, c21, c22, c23),
                      c0,
                      c1,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.PRE_ORDER);

        testTraversal(toList(c0, c1, c2, c11, c12, c13, c131),
                      c0,
                      c2,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.PRE_ORDER);

        testTraversal(toList(c0, c1, c2, c11, c12, c13, c21, c22, c23),
                      c0,
                      c13,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.PRE_ORDER);

        testTraversal(toList(c1),
                      c1,
                      c1,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.PRE_ORDER);

        testTraversal(toList(c1, c11, c12, c13, c131),
                      c1,
                      c11,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.PRE_ORDER);

        testTraversal(toList(c1, c11, c12, c13),
                      c1,
                      c13,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.PRE_ORDER);
    }

    @Test
    void testBreadthFirstPost() {
        testTraversal(toList(c131, c23, c22, c21, c13, c12, c11, c2, c1, c0),
                      c0,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.POST_ORDER);

        testTraversal(toList(c131, c13, c12, c11, c1),
                      c1,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.POST_ORDER);

    }

    @Test
    void testBreadthFirstPostEvaluator() {
        testTraversal(toList(c0),
                      c0,
                      c0,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.POST_ORDER);

        testTraversal(toList(c23, c22, c21, c2, c1, c0),
                      c0,
                      c1,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.POST_ORDER);

        testTraversal(toList(c131, c13, c12, c11, c2, c1, c0),
                      c0,
                      c2,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.POST_ORDER);

        testTraversal(toList(c1),
                      c1,
                      c1,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.POST_ORDER);

        testTraversal(toList(c13, c12, c11, c1),
                      c1,
                      c13,
                      TraversalMethod.BREADTH_FIRST,
                      TraversalOrder.POST_ORDER);
    }

    @Test
    void testTransitiveClosure() {
        testTransitiveClosure(toSet(c0, c1, c11, c12, c13, c131, c2, c21, c22, c23),
                              c0);
        testTransitiveClosure(toSet(c1, c11, c12, c13, c131),
                              c1);
    }

    @Test
    void testTopologocalSort() {
        testTopologicalSort(c0, 10);
        testTopologicalSort(c1, 5);
        testTopologicalSort(c11, 1);
        testTopologicalSort(c12, 1);
        testTopologicalSort(c13, 2);
        testTopologicalSort(c131, 1);
        testTopologicalSort(c2, 4);
        testTopologicalSort(c21, 1);
        testTopologicalSort(c22, 1);
        testTopologicalSort(c23, 1);
    }

    @Test
    void testAreConnected() {
        assertFalse(sg.areConnected(c0, c0));
        assertTrue(sg.areConnected(c0, c1));
        assertFalse(sg.areConnected(c1, c0));
        assertTrue(sg.areConnected(c0, c131));
    }

    @Test
    void testCycleMember1() {
        final TestHeavyGraph g = new TestHeavyGraph();
        final SlimGraph<TestGraphHeavyNode> sg = new SlimGraph<>(g, EdgeDirection.OUTGOING);

        final TestGraphHeavyNode n1 = g.createNode("N1");
        assertFalse(sg.areConnected(n1, n1), "N1 N1");
        assertFalse(sg.nodeIsCycleMember(n1));

        g.createEdge("E11", "N1", "N1");
        assertTrue(sg.areConnected(n1, n1), "N1 N1");
        assertTrue(sg.nodeIsCycleMember(n1));
    }

    @Test
    void testCycleMember2() {
        final TestHeavyGraph g = new TestHeavyGraph();
        final SlimGraph<TestGraphHeavyNode> sg = new SlimGraph<>(g, EdgeDirection.OUTGOING);
        final TestGraphHeavyNode n1 = g.createNode("N1");
        final TestGraphHeavyNode n2 = g.createNode("N2");
        assertFalse(sg.areConnected(n1, n2), "N1 N2");
        assertFalse(sg.areConnected(n2, n1), "N2 N1");
        assertFalse(sg.nodeIsCycleMember(n1));
        assertFalse(sg.nodeIsCycleMember(n2));

        g.createEdge("E12", "N1", "N2");
        assertTrue(sg.areConnected(n1, n2), "N1 N2");
        assertFalse(sg.areConnected(n2, n1), "N2 N1");
        assertFalse(sg.nodeIsCycleMember(n1));
        assertFalse(sg.nodeIsCycleMember(n2));

        g.createEdge("E21", "N2", "N1");
        assertTrue(sg.areConnected(n1, n2), "N1 N2");
        assertTrue(sg.areConnected(n2, n1), "N2 N1");
        assertTrue(sg.nodeIsCycleMember(n1));
        assertTrue(sg.nodeIsCycleMember(n2));
    }
}