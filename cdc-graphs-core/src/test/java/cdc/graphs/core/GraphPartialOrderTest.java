package cdc.graphs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.junit.jupiter.api.Test;

import cdc.graphs.PartialOrderPosition;
import cdc.graphs.impl.tests.TestGraphHeavyEdge;
import cdc.graphs.impl.tests.TestGraphHeavyNode;
import cdc.graphs.impl.tests.TestHeavyGraph;

class GraphPartialOrderTest {
    private static TestGraphHeavyNode node(GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> g,
                                           String name) {
        return ((TestHeavyGraph) g.getAdapter()).getNode(name);
    }

    private static Set<TestGraphHeavyNode> nodes(GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> g,
                                                 String... names) {
        return ((TestHeavyGraph) g.getAdapter()).getNodes(names);
    }

    private static GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> buildValidGraph1() {
        final TestHeavyGraph g = new TestHeavyGraph();
        g.createNode("A1");
        g.createNode("A2");
        g.createNode("A3");
        g.createNode("A3A1");
        g.createNode("A3A2");
        g.createNode("A3B1");
        g.createNode("A3B2");
        g.createNode("A4");
        g.createNode("A5");

        g.createEdge("A2", "A1");
        g.createEdge("A3", "A2");
        g.createEdge("A3A1", "A3");
        g.createEdge("A3A2", "A3A1");
        g.createEdge("A3B1", "A3");
        g.createEdge("A3B2", "A3B1");
        g.createEdge("A4", "A3A2");
        g.createEdge("A4", "A3B2");
        g.createEdge("A5", "A4");

        g.createNode("B1");
        g.createNode("B2");
        g.createNode("B3");

        g.createEdge("B2", "B1");
        g.createEdge("B3", "B2");

        g.createNode("C1");

        return new GraphPartialOrder<>(g);
    }

    private static GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> buildInvalidGraph1() {
        final TestHeavyGraph g = new TestHeavyGraph();
        g.createNode("A1");
        g.createNode("A2");

        g.createEdge("A2", "A1");
        g.createEdge("A1", "A2");

        return new GraphPartialOrder<>(g);
    }

    private static GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> buildInvalidGraph2() {
        final TestHeavyGraph g = new TestHeavyGraph();
        g.createNode("A1");

        g.createEdge("A1", "A1");

        return new GraphPartialOrder<>(g);
    }

    @Test
    void testIsPartialOrTotalOrder() {
        final GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> vg1 = buildValidGraph1();
        final GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> ig1 = buildInvalidGraph1();
        final GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> ig2 = buildInvalidGraph2();

        assertTrue(vg1.isPartialOrTotalOrder());
        assertFalse(ig1.isPartialOrTotalOrder());
        assertFalse(ig2.isPartialOrTotalOrder());
    }

    @Test
    void testGetDirectGreaterThanNodes() {
        final GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> g = buildValidGraph1();
        final Function<TestGraphHeavyNode, Set<TestGraphHeavyNode>> f = g::getDirectGreaterThanNodes;

        assertEquals(nodes(g, "A2"), f.apply(node(g, "A1")));
        assertEquals(nodes(g, "A3"), f.apply(node(g, "A2")));
        assertEquals(nodes(g, "A3A1", "A3B1"), f.apply(node(g, "A3")));
        assertEquals(nodes(g, "A3A2"), f.apply(node(g, "A3A1")));
        assertEquals(nodes(g, "A3B2"), f.apply(node(g, "A3B1")));
        assertEquals(nodes(g, "A4"), f.apply(node(g, "A3A2")));
        assertEquals(nodes(g, "A4"), f.apply(node(g, "A3B2")));
        assertEquals(nodes(g, "A5"), f.apply(node(g, "A4")));
        assertTrue(f.apply(node(g, "A5")).isEmpty());

        assertEquals(nodes(g, "B2"), f.apply(node(g, "B1")));
        assertEquals(nodes(g, "B3"), f.apply(node(g, "B2")));
        assertTrue(f.apply(node(g, "B3")).isEmpty());

        assertTrue(f.apply(node(g, "C1")).isEmpty());
    }

    @Test
    void testGetDirectLessThanNodes() {
        final GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> g = buildValidGraph1();
        final Function<TestGraphHeavyNode, Set<TestGraphHeavyNode>> f = g::getDirectLessThanNodes;

        assertTrue(f.apply(node(g, "A1")).isEmpty());
        assertEquals(nodes(g, "A1"), f.apply(node(g, "A2")));
        assertEquals(nodes(g, "A2"), f.apply(node(g, "A3")));
        assertEquals(nodes(g, "A3"), f.apply(node(g, "A3A1")));
        assertEquals(nodes(g, "A3"), f.apply(node(g, "A3B1")));
        assertEquals(nodes(g, "A3A1"), f.apply(node(g, "A3A2")));
        assertEquals(nodes(g, "A3B1"), f.apply(node(g, "A3B2")));
        assertEquals(nodes(g, "A3A2", "A3B2"), f.apply(node(g, "A4")));
        assertEquals(nodes(g, "A4"), f.apply(node(g, "A5")));

        assertTrue(f.apply(node(g, "B1")).isEmpty());
        assertEquals(nodes(g, "B1"), f.apply(node(g, "B2")));
        assertEquals(nodes(g, "B2"), f.apply(node(g, "B3")));

        assertTrue(f.apply(node(g, "C1")).isEmpty());
    }

    @Test
    void testGetAllGreaterOrEqualNodes() {
        final GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> g = buildValidGraph1();
        final Function<TestGraphHeavyNode, Set<TestGraphHeavyNode>> f = g::getAllGreaterOrEqualNodes;

        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"), f.apply(node(g, "A1")));
        assertEquals(nodes(g, "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"), f.apply(node(g, "A2")));
        assertEquals(nodes(g, "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"), f.apply(node(g, "A3")));
        assertEquals(nodes(g, "A3A1", "A3A2", "A4", "A5"), f.apply(node(g, "A3A1")));
        assertEquals(nodes(g, "A3B1", "A3B2", "A4", "A5"), f.apply(node(g, "A3B1")));
        assertEquals(nodes(g, "A3A2", "A4", "A5"), f.apply(node(g, "A3A2")));
        assertEquals(nodes(g, "A3B2", "A4", "A5"), f.apply(node(g, "A3B2")));
        assertEquals(nodes(g, "A4", "A5"), f.apply(node(g, "A4")));
        assertEquals(nodes(g, "A5"), f.apply(node(g, "A5")));

        assertEquals(nodes(g, "B1", "B2", "B3"), f.apply(node(g, "B1")));
        assertEquals(nodes(g, "B2", "B3"), f.apply(node(g, "B2")));
        assertEquals(nodes(g, "B3"), f.apply(node(g, "B3")));

        assertEquals(nodes(g, "C1"), f.apply(node(g, "C1")));
    }

    @Test
    void testGetAllGreaterThanNodes() {
        final GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> g = buildValidGraph1();
        final Function<TestGraphHeavyNode, Set<TestGraphHeavyNode>> f = g::getAllGreaterThanNodes;

        assertEquals(nodes(g, "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"), f.apply(node(g, "A1")));
        assertEquals(nodes(g, "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"), f.apply(node(g, "A2")));
        assertEquals(nodes(g, "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"), f.apply(node(g, "A3")));
        assertEquals(nodes(g, "A3A2", "A4", "A5"), f.apply(node(g, "A3A1")));
        assertEquals(nodes(g, "A3B2", "A4", "A5"), f.apply(node(g, "A3B1")));
        assertEquals(nodes(g, "A4", "A5"), f.apply(node(g, "A3A2")));
        assertEquals(nodes(g, "A4", "A5"), f.apply(node(g, "A3B2")));
        assertEquals(nodes(g, "A5"), f.apply(node(g, "A4")));
        assertTrue(f.apply(node(g, "A5")).isEmpty());

        assertEquals(nodes(g, "B2", "B3"), f.apply(node(g, "B1")));
        assertEquals(nodes(g, "B3"), f.apply(node(g, "B2")));
        assertTrue(f.apply(node(g, "B3")).isEmpty());

        assertTrue(f.apply(node(g, "C1")).isEmpty());
    }

    @Test
    void testGetAllLessOrEqualNodes() {
        final GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> g = buildValidGraph1();
        final Function<TestGraphHeavyNode, Set<TestGraphHeavyNode>> f = g::getAllLessOrEqualNodes;

        assertEquals(nodes(g, "A1"), f.apply(node(g, "A1")));
        assertEquals(nodes(g, "A1", "A2"), f.apply(node(g, "A2")));
        assertEquals(nodes(g, "A1", "A2", "A3"), f.apply(node(g, "A3")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1"), f.apply(node(g, "A3A1")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3B1"), f.apply(node(g, "A3B1")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2"), f.apply(node(g, "A3A2")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3B1", "A3B2"), f.apply(node(g, "A3B2")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4"), f.apply(node(g, "A4")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"), f.apply(node(g, "A5")));

        assertEquals(nodes(g, "B1"), f.apply(node(g, "B1")));
        assertEquals(nodes(g, "B1", "B2"), f.apply(node(g, "B2")));
        assertEquals(nodes(g, "B1", "B2", "B3"), f.apply(node(g, "B3")));

        assertEquals(nodes(g, "C1"), f.apply(node(g, "C1")));
    }

    @Test
    void testGetAllLessThanNodes() {
        final GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> g = buildValidGraph1();
        final Function<TestGraphHeavyNode, Set<TestGraphHeavyNode>> f = g::getAllLessThanNodes;

        assertTrue(f.apply(node(g, "A1")).isEmpty());
        assertEquals(nodes(g, "A1"), f.apply(node(g, "A2")));
        assertEquals(nodes(g, "A1", "A2"), f.apply(node(g, "A3")));
        assertEquals(nodes(g, "A1", "A2", "A3"), f.apply(node(g, "A3A1")));
        assertEquals(nodes(g, "A1", "A2", "A3"), f.apply(node(g, "A3B1")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1"), f.apply(node(g, "A3A2")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3B1"), f.apply(node(g, "A3B2")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2"), f.apply(node(g, "A4")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4"), f.apply(node(g, "A5")));

        assertTrue(f.apply(node(g, "B1")).isEmpty());
        assertEquals(nodes(g, "B1"), f.apply(node(g, "B2")));
        assertEquals(nodes(g, "B1", "B2"), f.apply(node(g, "B3")));

        assertTrue(f.apply(node(g, "C1")).isEmpty());
    }

    @Test
    void testGetAllRelatedNodes() {
        final GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> g = buildValidGraph1();
        final Function<TestGraphHeavyNode, Set<TestGraphHeavyNode>> f = g::getAllRelatedNodes;

        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"), f.apply(node(g, "A1")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"), f.apply(node(g, "A2")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"), f.apply(node(g, "A3")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A4", "A5"), f.apply(node(g, "A3A1")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3B1", "A3B2", "A4", "A5"), f.apply(node(g, "A3B1")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A4", "A5"), f.apply(node(g, "A3A2")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3B1", "A3B2", "A4", "A5"), f.apply(node(g, "A3B2")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"), f.apply(node(g, "A4")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"), f.apply(node(g, "A5")));

        assertEquals(nodes(g, "B1", "B2", "B3"), f.apply(node(g, "B1")));
        assertEquals(nodes(g, "B1", "B2", "B3"), f.apply(node(g, "B2")));
        assertEquals(nodes(g, "B1", "B2", "B3"), f.apply(node(g, "B3")));

        assertEquals(nodes(g, "C1"), f.apply(node(g, "C1")));
    }

    @Test
    void testGetAllUnrelatedNodes() {
        final GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> g = buildValidGraph1();
        final Function<TestGraphHeavyNode, Set<TestGraphHeavyNode>> f = g::getAllUnrelatedNodes;

        assertEquals(nodes(g, "B1", "B2", "B3", "C1"), f.apply(node(g, "A1")));
        assertEquals(nodes(g, "B1", "B2", "B3", "C1"), f.apply(node(g, "A2")));
        assertEquals(nodes(g, "B1", "B2", "B3", "C1"), f.apply(node(g, "A3")));
        assertEquals(nodes(g, "A3B1", "A3B2", "B1", "B2", "B3", "C1"), f.apply(node(g, "A3A1")));
        assertEquals(nodes(g, "A3A1", "A3A2", "B1", "B2", "B3", "C1"), f.apply(node(g, "A3B1")));
        assertEquals(nodes(g, "A3B1", "A3B2", "B1", "B2", "B3", "C1"), f.apply(node(g, "A3A2")));
        assertEquals(nodes(g, "A3A1", "A3A2", "B1", "B2", "B3", "C1"), f.apply(node(g, "A3B2")));
        assertEquals(nodes(g, "B1", "B2", "B3", "C1"), f.apply(node(g, "A4")));
        assertEquals(nodes(g, "B1", "B2", "B3", "C1"), f.apply(node(g, "A5")));

        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5", "C1"), f.apply(node(g, "B1")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5", "C1"), f.apply(node(g, "B2")));
        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5", "C1"), f.apply(node(g, "B3")));

        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5", "B1", "B2", "B3"),
                     f.apply(node(g, "C1")));
    }

    @Test
    void testGetAllNodes() {
        final GraphPartialOrder<TestGraphHeavyNode, TestGraphHeavyEdge> g = buildValidGraph1();
        final BiFunction<TestGraphHeavyNode, PartialOrderPosition[], Set<TestGraphHeavyNode>> f = g::getAllNodes;

        assertTrue(f.apply(node(g, "A1"), new PartialOrderPosition[0]).isEmpty());

        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5", "B1", "B2", "B3", "C1"),
                     f.apply(node(g, "A1"),
                             new PartialOrderPosition[] {
                                     PartialOrderPosition.EQUAL,
                                     PartialOrderPosition.LESS_THAN,
                                     PartialOrderPosition.GREATER_THAN,
                                     PartialOrderPosition.UNRELATED }));

        assertEquals(nodes(g, "A1"),
                     f.apply(node(g, "A1"),
                             new PartialOrderPosition[] {
                                     PartialOrderPosition.EQUAL }));

        assertEquals(nodes(g, "A1"),
                     f.apply(node(g, "A1"),
                             new PartialOrderPosition[] {
                                     PartialOrderPosition.EQUAL,
                                     PartialOrderPosition.LESS_THAN }));

        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"),
                     f.apply(node(g, "A1"),
                             new PartialOrderPosition[] {
                                     PartialOrderPosition.EQUAL,
                                     PartialOrderPosition.GREATER_THAN }));

        assertEquals(nodes(g, "A1", "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5"),
                     f.apply(node(g, "A1"),
                             new PartialOrderPosition[] {
                                     PartialOrderPosition.EQUAL,
                                     PartialOrderPosition.LESS_THAN,
                                     PartialOrderPosition.GREATER_THAN }));

        assertEquals(nodes(g, "B1", "B2", "B3", "C1"),
                     f.apply(node(g, "A1"),
                             new PartialOrderPosition[] {
                                     PartialOrderPosition.UNRELATED }));

        assertEquals(nodes(g, "A1", "B1", "B2", "B3", "C1"),
                     f.apply(node(g, "A1"),
                             new PartialOrderPosition[] {
                                     PartialOrderPosition.EQUAL,
                                     PartialOrderPosition.UNRELATED }));

        assertEquals(nodes(g, "B1", "B2", "B3", "C1"),
                     f.apply(node(g, "A1"),
                             new PartialOrderPosition[] {
                                     PartialOrderPosition.LESS_THAN,
                                     PartialOrderPosition.UNRELATED }));

        assertEquals(nodes(g, "A2", "A3", "A3A1", "A3A2", "A3B1", "A3B2", "A4", "A5", "B1", "B2", "B3", "C1"),
                     f.apply(node(g, "A1"),
                             new PartialOrderPosition[] {
                                     PartialOrderPosition.GREATER_THAN,
                                     PartialOrderPosition.UNRELATED }));
    }
}