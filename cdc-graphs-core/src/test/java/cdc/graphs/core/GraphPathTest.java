package cdc.graphs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

class GraphPathTest {
    private static final Logger LOGGER = LogManager.getLogger(GraphPathTest.class);

    @Test
    void test() {
        final GraphPath<String> p0 = GraphPath.builder(String.class).build();
        assertTrue(p0.isEmpty());

        final GraphPath<String> p1 = GraphPath.builder(String.class)
                                              .push("Hello")
                                              .push("World")
                                              .build();
        assertEquals(2, p1.size());
    }

    @Test
    void testReduce() {
        final Set<GraphPath<String>> paths = new HashSet<>();
        paths.add(GraphPath.builder(String.class).push("1", "2", "3").build());
        paths.add(GraphPath.builder(String.class).push("1", "2").build());
        paths.add(GraphPath.builder(String.class).push("2", "3").build());
        paths.add(GraphPath.builder(String.class).push("2").build());
        paths.add(GraphPath.builder(String.class).push("2, 3, 4").build());
        paths.add(GraphPath.builder(String.class).build());
        LOGGER.debug("Before reduction: {}", paths);
        GraphPath.reduce(paths);
        LOGGER.debug("After reduction: {}", paths);
        assertTrue(true);
    }
}