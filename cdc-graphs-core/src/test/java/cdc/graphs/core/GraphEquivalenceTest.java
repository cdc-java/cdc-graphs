package cdc.graphs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.graphs.core.GraphEquivalence.Informer;
import cdc.graphs.impl.GraphPrinter;
import cdc.graphs.impl.tests.TestEdge;
import cdc.graphs.impl.tests.TestGraph;
import cdc.graphs.impl.tests.TestHeavyGraph;
import cdc.graphs.impl.tests.TestLightGraph;
import cdc.graphs.impl.tests.TestNode;

class GraphEquivalenceTest {
    private static final Logger LOGGER = LogManager.getLogger(GraphEquivalenceTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.INFO).buildPrintStream();

    private static final GraphEquivalence.Informer<TestNode> NODE_INFORMER = new Informer<TestNode>() {
        @Override
        public int hashCode(TestNode object) {
            return object.getDefinitionHashCode();
        }

        @Override
        public boolean areEqual(TestNode o1,
                                TestNode o2) {
            return o1.hasSameDefinition(o2);
        }
    };

    private static final GraphEquivalence.Informer<TestEdge<? extends TestNode>> EDGE_INFORMER =
            new Informer<TestEdge<? extends TestNode>>() {
                @Override
                public int hashCode(TestEdge<? extends TestNode> object) {
                    return object.getDefinitionHashCode();
                }

                @Override
                public boolean areEqual(TestEdge<? extends TestNode> o1,
                                        TestEdge<? extends TestNode> o2) {
                    return o1.hasSameDefinition(o2);
                }
            };

    private static <N extends TestNode, E extends TestEdge<N>> void check(TestGraph<N, E> left,
                                                                          TestGraph<N, E> right,
                                                                          int addedNodes,
                                                                          int removedNodes,
                                                                          int addedEdges,
                                                                          int removedEdges) {
        OUT.println("===========================================");
        OUT.println("Left");
        GraphPrinter.print(left, true, OUT);
        OUT.println("Right");
        GraphPrinter.print(right, true, OUT);

        final GraphEquivalence<N, E> ge = new GraphEquivalence<>(NODE_INFORMER,
                                                                 EDGE_INFORMER);
        final GraphDiff<N, E> diff = ge.compare(left, right);
        OUT.println("Added nodes  : " + diff.getAddedNodes());
        OUT.println("Removed nodes: " + diff.getRemovedNodes());
        OUT.println("Added edges  : " + diff.getAddedEdges());
        OUT.println("Removed edges: " + diff.getRemovedEdges());

        assertEquals(diff.getAddedNodes().size(), addedNodes, "Added nodes: ");
        assertEquals(diff.getRemovedNodes().size(), removedNodes, "Removed nodes: ");
        assertEquals(diff.getAddedEdges().size(), addedEdges, "Added edges: ");
        assertEquals(diff.getRemovedEdges().size(), removedEdges, "Removed edges: ");
    }

    @Test
    void testHeavy() {
        final TestHeavyGraph left = new TestHeavyGraph();
        left.createNodes("A", "C");
        left.createEdge("A", "C");
        final TestHeavyGraph right = new TestHeavyGraph();
        right.createNodes("B", "C");
        right.createEdge("B", "C");

        check(left, right, 1, 1, 1, 1);
    }

    @Test
    void testLight() {
        final TestLightGraph left = new TestLightGraph(true);
        left.createNodes("A", "C");
        left.createEdge("A", "C");
        final TestLightGraph right = new TestLightGraph(true);
        right.createNodes("B", "C");
        right.createEdge("B", "C");

        check(left, right, 1, 1, 1, 1);
    }
}