package cdc.graphs.demos;

import cdc.graphs.impl.GraphPrinter;
import cdc.graphs.impl.RestrictionSubGraph;
import cdc.graphs.impl.tests.TestGraphHeavyEdge;
import cdc.graphs.impl.tests.TestGraphHeavyNode;
import cdc.graphs.impl.tests.TestHeavyGraph;

public final class SubGraphDemo {
    private SubGraphDemo() {
    }

    public static void main(String[] args) {
        final TestHeavyGraph g = new TestHeavyGraph();
        final TestGraphHeavyNode n1 = g.createNode("N1");
        final TestGraphHeavyNode n2 = g.createNode("N2");
        final TestGraphHeavyNode n3 = g.createNode("N3");
        final TestGraphHeavyNode n4 = g.createNode("N4");
        final TestGraphHeavyNode n5 = g.createNode("N5");
        final TestGraphHeavyEdge e12 = g.createEdge("E12", n1, n2);
        g.createEdge("E21", n2, n1);
        g.createEdge("E13", n1, n3);
        g.createEdge("E34", n3, n4);
        final TestGraphHeavyEdge e45 = g.createEdge("E45", n4, n5);
        g.createEdge("E54", n5, n4);
        System.out.println("---------------------------");
        System.out.println("Graph");
        GraphPrinter.print(g, true, System.out);

        final RestrictionSubGraph<TestGraphHeavyNode, TestGraphHeavyEdge> sg = new RestrictionSubGraph<>(g);
        System.out.println("---------------------------");
        System.out.println("SubGraph (Should be the same as Graph)");
        GraphPrinter.print(sg, true, System.out);

        sg.clear();
        System.out.println("---------------------------");
        System.out.println("SubGraph (Should be empty)");
        GraphPrinter.print(sg, true, System.out);

        System.out.println("---------------------------");
        System.out.println("Graph");
        GraphPrinter.print(g, true, System.out);

        assert !sg.containsNode(n1);
        assert !sg.containsEdge(e45);
        sg.addNode(n1);
        System.out.println("---------------------------");
        System.out.println("SubGraph (Should contain n1)");
        GraphPrinter.print(sg, true, System.out);
        sg.addEdge(e12);
        sg.addEdge(e45);
        System.out.println("---------------------------");
        System.out.println("SubGraph (Should contain n1,n2, n4 n5, e12 and e45)");
        GraphPrinter.print(sg, true, System.out);
        assert sg.containsNode(n1);
        assert sg.containsEdge(e45);
    }
}