package cdc.graphs.demos;

import cdc.graphs.EdgeDirection;
import cdc.graphs.TraversalMethod;
import cdc.graphs.TraversalOrder;
import cdc.graphs.core.GraphTraverser;
import cdc.graphs.impl.tests.TestGraphHeavyEdge;
import cdc.graphs.impl.tests.TestGraphHeavyNode;
import cdc.graphs.impl.tests.TestHeavyGraph;
import cdc.util.function.Visitor;

public final class TraversalDemo {
    private TraversalDemo() {
    }

    public static void main(String[] args) {
        final Visitor<TestGraphHeavyNode> visitor = item -> System.out.print(" " + item.getName());

        final TestHeavyGraph g = new TestHeavyGraph();
        final TestGraphHeavyNode n1 = g.createNode("N1");
        final TestGraphHeavyNode n2 = g.createNode("N2");
        final TestGraphHeavyNode n3 = g.createNode("N3");
        final TestGraphHeavyNode n4 = g.createNode("N4");
        final TestGraphHeavyNode n5 = g.createNode("N5");
        g.createEdge("E12", n1, n2);
        g.createEdge("E23", n2, n3);
        g.createEdge("E24", n2, n4);
        g.createEdge("E45", n4, n5);

        final GraphTraverser<TestGraphHeavyNode, TestGraphHeavyEdge> traverser = new GraphTraverser<>(g);
        for (final TraversalMethod method : TraversalMethod.values()) {
            // for (final Node node : g.getNodes()) {
            final TestGraphHeavyNode node = n1;
            {
                for (final TraversalOrder order : TraversalOrder.values()) {
                    for (final EdgeDirection direction : EdgeDirection.values()) {
                        System.out.println("--------------------------------");
                        System.out.println(node.getName() + " " + method + " " + order + " " + direction);
                        traverser.traverse(node, method, order, direction, visitor);
                        System.out.println();
                    }
                }
            }
        }
    }
}