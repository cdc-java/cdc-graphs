package cdc.graphs.impl;

import cdc.graphs.GraphAdapter;
import cdc.util.lang.Checks;

/**
 * Base class used to filter a graph and make it appear as another graph.
 *
 * @author Damien Carbonne
 *
 * @param <N> Node class
 * @param <E> Edge class
 */
public abstract class GraphFilter<N, E> implements GraphAdapter<N, E> {
    protected final GraphAdapter<N, E> delegate;

    protected GraphFilter(GraphAdapter<N, E> delegate) {
        Checks.isNotNull(delegate, "delegate");
        this.delegate = delegate;
    }

    /**
     * Checks that a node belongs to delegate.
     *
     * @param node The tested node.
     * @throws IllegalArgumentException When delegate does not contain node.
     */
    protected void checkNodeValidity(N node) {
        if (!delegate.containsNode(node)) {
            throw new IllegalArgumentException("Node (" + node + ") does not belong to delegate");
        }
    }

    /**
     * Checks that an edge belongs to delegate.
     *
     * @param edge The tested edge.
     * @throws IllegalArgumentException When delegate does not contain edge.
     */
    protected void checkEdgeValidity(E edge) {
        if (!delegate.containsEdge(edge)) {
            throw new IllegalArgumentException("Edge does not belong to delegate");
        }
    }

    public final GraphAdapter<N, E> getDelegate() {
        return delegate;
    }
}