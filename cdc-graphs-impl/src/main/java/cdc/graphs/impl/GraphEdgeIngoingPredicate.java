package cdc.graphs.impl;

import java.util.function.Predicate;

import cdc.graphs.GraphEdge;

/**
 * Edge predicate for ingoing edges of a node.
 *
 * @author Damien Carbonne
 *
 * @param <N> Node type.
 */
public final class GraphEdgeIngoingPredicate<N> implements Predicate<GraphEdge<N>> {
    /** Reference node. */
    final N node;

    public GraphEdgeIngoingPredicate(N node) {
        this.node = node;
    }

    @Override
    public boolean test(GraphEdge<N> edge) {
        return edge.getTarget() == node;
    }
}