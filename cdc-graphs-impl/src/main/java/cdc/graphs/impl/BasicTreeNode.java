package cdc.graphs.impl;

import java.util.ArrayList;
import java.util.List;

public class BasicTreeNode<N extends BasicTreeNode<N>> {
    private N parent;
    private final List<N> children = new ArrayList<>();

    public BasicTreeNode() {
        super();
    }

    public BasicTreeNode(N parent) {
        this.setParent(parent);
    }

    void removeChild(BasicTreeNode<?> child) {
        children.remove(child);
    }

    void addChild(N child) {
        children.add(child);
    }

    public N getParent() {
        return parent;
    }

    protected void setParent(N parent) {
        if (this.parent != parent) {
            if (this.parent != null) {
                this.parent.removeChild(this);
                this.parent = null;
            }
            if (parent != null) {
                this.parent = parent;
                @SuppressWarnings("unchecked")
                final N nthis = (N) this;
                parent.addChild(nthis);
            }
        }
    }

    public List<N> getChildren() {
        return children;
    }
}