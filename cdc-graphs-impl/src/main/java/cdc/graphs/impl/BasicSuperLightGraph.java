package cdc.graphs.impl;

public class BasicSuperLightGraph<N> extends BasicLightGraph<N, BasicGraphEdge<N>> {
    public BasicSuperLightGraph(boolean sorted,
                                CollectionKind collectionKind) {
        super(sorted, collectionKind);
    }

    public BasicSuperLightGraph() {
        super();
    }

    public BasicGraphEdge<N> addEdge(N source,
                                     N target) {
        final BasicGraphEdge<N> edge = new BasicGraphEdge<>(source, target);
        addEdge(edge);
        return edge;
    }

    public void addEdgeIfMissing(N source,
                                 N target) {
        if (!hasEdge(source, target)) {
            addEdge(source, target);
        }
    }
}