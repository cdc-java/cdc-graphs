package cdc.graphs.impl;

import java.util.function.Predicate;

import cdc.graphs.GraphEdge;

/**
 * Edge predicate for outgoing edges of a node.
 *
 * @author Damien Carbonne
 *
 * @param <N> Node type.
 */
public final class GraphEdgeOutgoingPredicate<N> implements Predicate<GraphEdge<N>> {
    /** Reference node. */
    final N node;

    public GraphEdgeOutgoingPredicate(N node) {
        this.node = node;
    }

    @Override
    public boolean test(GraphEdge<N> edge) {
        return edge.getSource() == node;
    }
}