package cdc.graphs.impl;

import cdc.graphs.GraphAdapter;

/**
 * Base class use to construct subgraphs by explicitly defining their content.
 *
 * @author Damien Carbonne
 * @see ExtensionSubGraph
 * @see RestrictionSubGraph
 *
 * @param <N> Node class
 * @param <E> Edge class
 */
public abstract class ExplicitSubGraph<N, E> extends GraphFilter<N, E> {
    protected ExplicitSubGraph(GraphAdapter<N, E> delegate) {
        super(delegate);
    }

    /**
     * Make the subgraph empty.
     */
    public abstract void clear();

    /**
     * Return whether the subgraph is empty or not.
     *
     * @return whether the subgraph is empty or not.
     */
    public abstract boolean isEmpty();

    /**
     * Add a node to the subgraph.
     *
     * @param node The node to add. Must belong to delegate.
     */
    public abstract void addNode(N node);

    /**
     * Remove one node and the associated edges.
     * If node does not belong to subgraph, does nothing.
     *
     * @param node The node to remove.
     */
    public abstract void removeNode(N node);

    /**
     * Add an edge and the associated nodes to the subgraph.
     *
     * @param edge The edge to add. Must belong to delegate.
     */
    public abstract void addEdge(E edge);

    /**
     * Remove one edge.
     * If edge does not belong to subgraph, does nothing.
     *
     * @param edge The edge to remove.
     */
    public abstract void removeEdge(E edge);

    /**
     * Remove all edges between a source and a target node.
     * If source or edge does not belong to subgraph, does nothing.
     *
     * @param source The source node.
     * @param target The target node.
     */
    public abstract void removeEdges(N source,
                                     N target);
}