package cdc.graphs.impl;

import cdc.graphs.EdgeDirection;
import cdc.graphs.EdgeTip;
import cdc.graphs.GraphAdapter;
import cdc.util.lang.Checks;

/**
 * Create an inverted view of a Graph. Nodes are the same, edges are the same,
 * except that their direction is inverted.
 *
 * @author Damien Carbonne
 *
 * @param <N> Node class
 * @param <E> Edge class
 */
public class InvertedGraph<N, E> extends GraphFilter<N, E> {
    public InvertedGraph(GraphAdapter<N, E> delegate) {
        super(delegate);
    }

    @Override
    public final Iterable<? extends N> getNodes() {
        return delegate.getNodes();
    }

    @Override
    public final boolean containsNode(N node) {
        return delegate.containsNode(node);
    }

    @Override
    public final Iterable<? extends E> getEdges() {
        return delegate.getEdges();
    }

    @Override
    public final boolean containsEdge(E edge) {
        return delegate.containsEdge(edge);
    }

    @Override
    public final Iterable<? extends E> getEdges(N node,
                                                EdgeDirection direction) {
        if (direction == null) {
            return delegate.getEdges(node);
        } else {
            return delegate.getEdges(node, direction.opposite());
        }
    }

    @Override
    public final N getTip(E edge,
                          EdgeTip tip) {
        Checks.isNotNull(tip, "tip");
        return delegate.getTip(edge, tip.opposite());
    }
}