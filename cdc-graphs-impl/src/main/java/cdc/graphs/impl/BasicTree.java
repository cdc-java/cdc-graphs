package cdc.graphs.impl;

import cdc.graphs.TreeAdapter;

public class BasicTree<N extends BasicTreeNode<N>> implements TreeAdapter<N> {
    public BasicTree() {
        super();
    }

    @Override
    public N getParent(N node) {
        return node.getParent();
    }

    @Override
    public Iterable<N> getChildren(N node) {
        return node.getChildren();
    }

    @Override
    public boolean hasChildren(N node) {
        return !node.getChildren().isEmpty();
    }
}