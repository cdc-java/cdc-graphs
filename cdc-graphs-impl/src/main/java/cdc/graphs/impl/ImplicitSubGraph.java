package cdc.graphs.impl;

import cdc.graphs.EdgeDirection;
import cdc.graphs.EdgeTip;
import cdc.graphs.GraphAdapter;
import cdc.util.function.IterableUtils;

/**
 * Base implementation of implicit subgraph.
 * <p>
 * Concrete implementation must implement:
 * <ul>
 * <li>{@code containsNode}.
 * <li>{@code containsEdge} if necessary.
 * Default implementation accepts all edges that connect 2 accepted nodes.
 * </ul>
 *
 * @author Damien Carbonne
 *
 * @param <N> Node type.
 * @param <E> Edge type.
 */
public abstract class ImplicitSubGraph<N, E> extends GraphFilter<N, E> {
    protected ImplicitSubGraph(GraphAdapter<N, E> delegate) {
        super(delegate);
    }

    @Override
    public Iterable<? extends N> getNodes() {
        return IterableUtils.filter(delegate.getNodes(), this::containsNode);
    }

    @Override
    public Iterable<? extends E> getEdges() {
        return IterableUtils.filter(delegate.getEdges(), this::containsEdge);
    }

    @Override
    public boolean containsEdge(E edge) {
        return getDelegate().containsEdge(edge)
                && containsNode(getDelegate().getTip(edge, EdgeTip.SOURCE))
                && containsNode(getDelegate().getTip(edge, EdgeTip.TARGET));
    }

    @Override
    public Iterable<? extends E> getEdges(N node,
                                          EdgeDirection direction) {
        return IterableUtils.filter(delegate.getEdges(node, direction),
                                this::containsEdge);
    }

    @Override
    public N getTip(E edge,
                    EdgeTip tip) {
        return delegate.getTip(edge, tip);
    }
}