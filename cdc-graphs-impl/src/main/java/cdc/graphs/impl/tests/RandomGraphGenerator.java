package cdc.graphs.impl.tests;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class RandomGraphGenerator {
    private final Random random = new Random();

    public RandomGraphGenerator() {
        super();
    }

    public void setSeed(long seed) {
        random.setSeed(seed);
    }

    public <N extends TestNode, E extends TestEdge<N>> void fill(TestGraph<N, E> graph,
                                                                 int nodesCount,
                                                                 int edgesCount) {

        final List<N> nodes = new ArrayList<>();
        for (int index = 0; index < nodesCount; index++) {
            nodes.add(graph.getOrCreateNode("N" + index));
        }
        for (int index = 0; index < edgesCount; index++) {
            final N source = nodes.get(random.nextInt(nodesCount));
            final N target = nodes.get(random.nextInt(nodesCount));
            graph.createEdge("E" + index, source, target);
        }
    }
}