package cdc.graphs.impl.tests;

import java.util.HashSet;
import java.util.Set;

import cdc.graphs.GraphAdapter;

public interface TestGraph<N extends TestNode, E extends TestEdge<N>> extends GraphAdapter<N, E> {
    public static String toNodeName(int number) {
        return "N" + number;
    }

    public static String toEdgeName(int sourceNumber,
                                    int targetNumber,
                                    String suffix) {
        return "E" + sourceNumber + "-" + targetNumber + suffix;
    }

    public static String toEdgeName(int sourceNumber,
                                    int targetNumber) {
        return "E" + sourceNumber + "-" + targetNumber;
    }

    public void clear();

    public default void clearLabels() {
        for (final N node : getNodes()) {
            node.setLabel(null);
        }
        for (final E edge : getEdges()) {
            edge.setLabel(null);
        }
    }

    @Override
    public Iterable<N> getNodes();

    public N getNode(String name);

    public default Set<N> getNodes(String... names) {
        final Set<N> set = new HashSet<>();

        for (final String name : names) {
            final N node = getNode(name);
            if (node == null) {
                throw new IllegalArgumentException("Invalid node name:" + name);
            } else {
                set.add(node);
            }
        }
        return set;

    }

    public default N getNode(int number) {
        return getNode(toNodeName(number));
    }

    public default boolean hasNode(String name) {
        return getNode(name) != null;
    }

    public N createNode(String name);

    public default N getOrCreateNode(String name) {
        N node = getNode(name);
        if (node == null) {
            node = createNode(name);
        }
        return node;
    }

    public default void createNodes(String... names) {
        for (final String name : names) {
            createNode(name);
        }
    }

    public default N getOrCreateNode(int number) {
        return getOrCreateNode(toNodeName(number));
    }

    public void removeNode(N node);

    public default void removeNode(String name) {
        removeNode(getNode(name));
    }

    public default void removeNode(int number) {
        removeNode(getNode(toNodeName(number)));
    }

    @Override
    public Iterable<E> getEdges();

    public E getEdge(String name);

    public default E getEdge(int sourceNumber,
                             int targetNumber) {
        return getEdge(toEdgeName(sourceNumber, targetNumber));
    }

    public default boolean hasEdge(String name) {
        return getEdge(name) != null;
    }

    public E createEdge(String name,
                        N source,
                        N target);

    public default E getOrCreateEdge(int sourceNumber,
                                     int targetNumber) {
        final String name = toEdgeName(sourceNumber, targetNumber);
        E edge = getEdge(name);
        if (edge == null) {
            final N source = getOrCreateNode(sourceNumber);
            final N target = getOrCreateNode(targetNumber);
            edge = createEdge(name, source, target);
        }
        return edge;
    }

    public default E getOrCreateEdge(int sourceNumber,
                                     int targetNumber,
                                     String suffix) {
        final String name = toEdgeName(sourceNumber, targetNumber, suffix);
        E edge = getEdge(name);
        if (edge == null) {
            final N source = getOrCreateNode(sourceNumber);
            final N target = getOrCreateNode(targetNumber);
            edge = createEdge(name, source, target);
        }
        return edge;
    }

    public void removeEdge(E edge);

    public default void removeEdge(String name) {
        removeEdge(getEdge(name));
    }

    public default void removeEdge(int sourceNumber,
                                   int targetNumber) {
        removeEdge(toEdgeName(sourceNumber, targetNumber));
    }

    public default E createEdge(String name,
                                String sourceName,
                                String targetName) {
        return createEdge(name, getNode(sourceName), getNode(targetName));
    }

    public default E createEdge(String sourceName,
                                String targetName) {
        return createEdge(sourceName + "_" + targetName, getNode(sourceName), getNode(targetName));
    }
}