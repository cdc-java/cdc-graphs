package cdc.graphs.impl.tests;

public class BijectionGenerator {
    public BijectionGenerator() {
        super();
    }

    public <N extends TestNode, E extends TestEdge<N>> void fill(TestGraph<N, E> graph,
                                                                 int size) {
        for (int index = 0; index < size; index++) {
            final N source = graph.getOrCreateNode("S" + index);
            final N target = graph.getOrCreateNode("T" + index);
            graph.createEdge("E" + index, source, target);
        }
    }
}