package cdc.graphs.impl.tests;

import java.util.Objects;

/**
 * Interface of node used for tests.
 *
 * @author Damien Carbonne
 */
public interface TestNode {
    /**
     * @return The node name (identifier-like).
     */
    public String getName();

    /**
     * @return The node label.
     */
    public String getLabel();

    /**
     * Sets the node label.
     *
     * @param label The label.
     */
    public void setLabel(String label);

    /**
     * @return A hash code based on definition of this node.
     *         Local attributes are used.
     *
     */
    public default int getDefinitionHashCode() {
        return Objects.hash(getLabel(),
                            getName());
    }

    /**
     * @param node The other node.
     * @return {@code true} if this node and {@code node} have the same definition.
     *         Local attributes are used.
     */
    public default boolean hasSameDefinition(TestNode node) {
        return Objects.equals(getLabel(), node.getLabel())
                && Objects.equals(getName(), node.getName());
    }
}