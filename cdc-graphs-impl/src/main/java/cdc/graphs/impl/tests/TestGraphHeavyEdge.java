package cdc.graphs.impl.tests;

import java.util.Objects;

import cdc.graphs.impl.BasicGraphEdge;

/**
 * Implementation of graph edge for tests.
 *
 * @author Damien Carbonne
 *
 */
public class TestGraphHeavyEdge extends BasicGraphEdge<TestGraphHeavyNode> implements TestEdge<TestGraphHeavyNode> {
    private final String name;
    private String label;

    public TestGraphHeavyEdge(String name,
                              TestGraphHeavyNode source,
                              TestGraphHeavyNode target) {
        super(source, target);
        this.name = name;
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public int hashCode() {
        return super.hashCode() * 31
                + Objects.hashCode(name);
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        if (!(object instanceof TestGraphHeavyEdge)) {
            return false;
        }
        final TestGraphHeavyEdge other = (TestGraphHeavyEdge) object;
        // Ignore label
        return Objects.equals(name, other.name);
    }

    @Override
    public String toString() {
        return name + (label == null ? "" : " (" + label + ")") + ": " + getSource() + " -> " + getTarget();
    }
}