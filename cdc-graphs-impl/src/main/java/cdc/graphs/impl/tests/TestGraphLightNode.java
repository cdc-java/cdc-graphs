package cdc.graphs.impl.tests;

/**
 * Implementation of graph node for tests.
 *
 * @author Damien Carbonne
 */
public class TestGraphLightNode implements TestNode {
    private final String name;
    private String label;

    public TestGraphLightNode(String name) {
        this.name = name;
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return label == null ? name : name + " (" + label + ")";
    }
}