package cdc.graphs.impl.tests;

import cdc.graphs.impl.BasicTree;
import cdc.graphs.impl.BasicTreeNode;

/**
 * Implementation of TreeNode for tests.
 *
 * @author Damien Carbonne
 *
 */
public class TestTreeNode extends BasicTreeNode<TestTreeNode> {
    private String name;
    private static final BasicTree<TestTreeNode> ADAPTER = new BasicTree<>();

    public TestTreeNode() {
        super();
    }

    public TestTreeNode(String name) {
        super();
        setName(name);
    }

    public TestTreeNode(String name,
                        TestTreeNode parent) {
        super();
        setName(name);
        setParent(parent);
    }

    public String getName() {
        return name;
    }

    public final void setName(String name) {
        this.name = name;
    }

    @Override
    public final void setParent(TestTreeNode parent) {
        if (ADAPTER.canSetParent(this, parent)) {
            super.setParent(parent);
        } else {
            throw new IllegalArgumentException("Can not set " + parent + " as parent of " + this);
        }
    }

    @Override
    public String toString() {
        return name;
    }
}