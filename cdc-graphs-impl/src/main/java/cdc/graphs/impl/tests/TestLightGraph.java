package cdc.graphs.impl.tests;

import java.util.HashMap;
import java.util.Map;

import cdc.graphs.impl.BasicLightGraph;
import cdc.util.lang.Checks;

public class TestLightGraph extends BasicLightGraph<TestGraphLightNode, TestGraphLightEdge>
        implements TestGraph<TestGraphLightNode, TestGraphLightEdge> {
    private final Map<String, TestGraphLightNode> nameToNode = new HashMap<>();
    private final Map<String, TestGraphLightEdge> nameToEdge = new HashMap<>();

    public TestLightGraph(boolean sorted) {
        super(sorted, CollectionKind.LIST);
    }

    @Override
    public void clear() {
        super.clear();
        nameToEdge.clear();
        nameToNode.clear();
    }

    @Override
    public TestGraphLightNode getNode(String name) {
        return nameToNode.get(name);
    }

    @Override
    public final TestGraphLightNode createNode(String name) {
        Checks.isFalse(hasNode(name), "Duplicate node name: {}", name);
        final TestGraphLightNode node = new TestGraphLightNode(name);
        addNode(node);
        nameToNode.put(name, node);
        return node;
    }

    @Override
    public void removeNode(TestGraphLightNode node) {
        Checks.isNotNull(node, "node");
        super.removeNode(node);
        nameToNode.remove(node.getName());
    }

    @Override
    public TestGraphLightEdge getEdge(String name) {
        return nameToEdge.get(name);
    }

    @Override
    public final TestGraphLightEdge createEdge(String name,
                                               TestGraphLightNode source,
                                               TestGraphLightNode target) {
        Checks.isTrue(containsNode(source), "Unknown source: {}", source);
        Checks.isTrue(containsNode(target), "Unknown target: {}", target);
        Checks.isFalse(hasEdge(name), "Duplicate edge name: {}", name);
        final TestGraphLightEdge edge = new TestGraphLightEdge(name, source, target);
        addEdge(edge);
        nameToEdge.put(name, edge);
        return edge;
    }

    @Override
    public void removeEdge(TestGraphLightEdge edge) {
        Checks.isNotNull(edge, "edge");
        super.removeEdge(edge);
        nameToEdge.remove(edge.getName());
    }
}