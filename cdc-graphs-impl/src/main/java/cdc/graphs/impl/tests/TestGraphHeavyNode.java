package cdc.graphs.impl.tests;

import cdc.graphs.impl.BasicGraphHeavyNode;

/**
 * Implementation of graph heavy node for tests.
 *
 * @author Damien Carbonne
 */
public class TestGraphHeavyNode extends BasicGraphHeavyNode<TestGraphHeavyEdge> implements TestNode {
    private final String name;
    private String label;

    public TestGraphHeavyNode(String name) {
        this.name = name;
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return label == null ? name : name + " (" + label + ")";
    }
}