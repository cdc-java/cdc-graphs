package cdc.graphs.impl.tests;

import java.util.HashMap;
import java.util.Map;

import cdc.graphs.impl.BasicHeavyGraph;
import cdc.util.lang.Checks;

/**
 * Heavy Graph implementation used for tests.
 * <p>
 * Nodes and edges have a name.
 *
 * @author Damien Carbonne
 *
 */
public final class TestHeavyGraph extends BasicHeavyGraph<TestGraphHeavyNode, TestGraphHeavyEdge>
        implements TestGraph<TestGraphHeavyNode, TestGraphHeavyEdge> {
    private final Map<String, TestGraphHeavyNode> nameToNode = new HashMap<>();
    private final Map<String, TestGraphHeavyEdge> nameToEdge = new HashMap<>();

    @Override
    public void clear() {
        super.clear();
        nameToEdge.clear();
        nameToNode.clear();
    }

    @Override
    public TestGraphHeavyNode getNode(String name) {
        return nameToNode.get(name);
    }

    @Override
    public final TestGraphHeavyNode createNode(String name) {
        Checks.isFalse(hasNode(name), "Duplicate node name: {}", name);
        final TestGraphHeavyNode node = new TestGraphHeavyNode(name);
        addNode(node);
        nameToNode.put(name, node);
        return node;
    }

    @Override
    public void removeNode(TestGraphHeavyNode node) {
        Checks.isNotNull(node, "node");
        super.removeNode(node);
        nameToNode.remove(node.getName());
    }

    @Override
    public TestGraphHeavyEdge getEdge(String name) {
        return nameToEdge.get(name);
    }

    @Override
    public final TestGraphHeavyEdge createEdge(String name,
                                               TestGraphHeavyNode source,
                                               TestGraphHeavyNode target) {
        Checks.isTrue(containsNode(source), "Unknown source: {}", source);
        Checks.isTrue(containsNode(target), "Unknown target: {}", target);
        Checks.isFalse(hasEdge(name), "Duplicate edge name: {}", name);
        final TestGraphHeavyEdge edge = new TestGraphHeavyEdge(name, source, target);
        addEdge(edge);
        nameToEdge.put(name, edge);
        return edge;
    }

    @Override
    public void removeEdge(TestGraphHeavyEdge edge) {
        Checks.isNotNull(edge, "edge");
        super.removeEdge(edge);
        nameToEdge.remove(edge.getName());
    }
}