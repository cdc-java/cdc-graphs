package cdc.graphs.impl.tests;

import java.util.Objects;

import cdc.graphs.GraphEdge;

public interface TestEdge<N extends TestNode> extends GraphEdge<N> {
    /**
     * @return The edge name (identifier-like).
     */
    public String getName();

    /**
     * @return The edge label.
     */
    public String getLabel();

    /**
     * Sets the edge label.
     *
     * @param label The label.
     */
    public void setLabel(String label);

    /**
     * @return A hash code based on definition of this edge.
     */
    public default int getDefinitionHashCode() {
        return Objects.hash(getLabel(),
                            getName(),
                            getSource().getDefinitionHashCode(),
                            getTarget().getDefinitionHashCode());
    }

    /**
     * @param edge The other edge.
     * @return {@code true} if this edge and {@code edge} have the same definition.
     */
    public default boolean hasSameDefinition(TestEdge<? extends TestNode> edge) {
        return Objects.equals(getLabel(), edge.getLabel())
                && Objects.equals(getName(), edge.getName())
                && getSource().hasSameDefinition(edge.getSource())
                && getTarget().hasSameDefinition(edge.getTarget());
    }
}