package cdc.graphs.impl;

import java.util.ArrayList;
import java.util.List;

import cdc.graphs.GraphHeavyNode;

/**
 * Default implementation of {@link GraphHeavyNode}.
 *
 * @author Damien Carbonne
 *
 * @param <E> The edge type.
 */
public class BasicGraphHeavyNode<E> implements GraphHeavyNode<E> {
    final List<E> outgoings = new ArrayList<>();
    final List<E> ingoings = new ArrayList<>();

    public BasicGraphHeavyNode() {
        super();
    }

    @Override
    public Iterable<E> getOutgoings() {
        return outgoings;
    }

    @Override
    public Iterable<E> getIngoings() {
        return ingoings;
    }
}