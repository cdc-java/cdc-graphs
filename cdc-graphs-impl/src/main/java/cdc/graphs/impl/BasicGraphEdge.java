package cdc.graphs.impl;

import java.util.Objects;

import cdc.graphs.GraphEdge;

/**
 * Base and naive implementation of graph edge.
 *
 * @author Damien Carbonne
 *
 * @param <N> Node type.
 */
public class BasicGraphEdge<N> implements GraphEdge<N> {
    private final N source;
    private final N target;

    public BasicGraphEdge(N source,
                          N target) {
        this.source = source;
        this.target = target;
    }

    @Override
    public N getSource() {
        return source;
    }

    @Override
    public N getTarget() {
        return target;
    }

    @Override
    public int hashCode() {
        return Objects.hash(source,
                            target);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof BasicGraphEdge)) {
            return false;
        }
        final BasicGraphEdge<?> other = (BasicGraphEdge<?>) object;
        return Objects.equals(source, other.source)
                && Objects.equals(target, other.target);
    }

    @Override
    public String toString() {
        return "[" + getSource() + " -> " + getTarget() + "]";
    }
}