package cdc.graphs.impl;

import java.util.function.Predicate;

import cdc.graphs.EdgeDirection;
import cdc.graphs.EdgeTip;
import cdc.graphs.GraphAdapter;
import cdc.graphs.GraphConsistencyError;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;

/**
 * Graph built from an underlying graph, by applying filters to nodes and edges.
 * <p>
 * It is the user responsibility to provide consistent filters. If the edge
 * filter gives a positive answer for an edge, then the corresponding nodes
 * should also be accepted.
 *
 * @author Damien Carbonne
 *
 * @param <N> Node class
 * @param <E> Edge class
 */
public class FilteredSubGraph<N, E> implements GraphAdapter<N, E> {
    private final GraphAdapter<N, E> delegate;
    private Predicate<N> nodePredicate = x -> true;
    private Predicate<E> edgePredicate = x -> true;

    /**
     * Wrapper used to check that edge filter is consistent with node filter.
     */
    private final Predicate<E> checkedEdgePredicate;

    public FilteredSubGraph(GraphAdapter<N, E> delegate) {
        Checks.isNotNull(delegate, "delegate");
        this.delegate = delegate;
        this.checkedEdgePredicate = input -> {
            final boolean pass = edgePredicate.test(input);
            if (pass) {
                // If the edge is accepted, both tips must be accepted
                final boolean sourceMismatch = !nodePredicate.test(delegate.getTip(input, EdgeTip.SOURCE));
                final boolean targetMismatch = !nodePredicate.test(delegate.getTip(input, EdgeTip.TARGET));
                if (sourceMismatch || targetMismatch) {
                    throw new GraphConsistencyError("Edge is accepted, but its source or target is not");
                }
            }
            return pass;
        };
    }

    public FilteredSubGraph(GraphAdapter<N, E> delegate,
                            Predicate<N> nodePredicate,
                            Predicate<E> edgePredicate) {
        this(delegate);
        setEdgePredicate(edgePredicate);
        setNodePredicate(nodePredicate);
    }

    public final void setNodePredicate(Predicate<N> predicate) {
        Checks.isNotNull(predicate, "predicate");
        this.nodePredicate = predicate;
    }

    public final void setEdgePredicate(Predicate<E> predicate) {
        Checks.isNotNull(predicate, "predicate");
        this.edgePredicate = predicate;
    }

    @Override
    public final Iterable<? extends N> getNodes() {
        return IterableUtils.filter(delegate.getNodes(), nodePredicate);
    }

    @Override
    public final boolean containsNode(N node) {
        return delegate.containsNode(node) && nodePredicate.test(node);
    }

    @Override
    public final Iterable<? extends E> getEdges() {
        return IterableUtils.filter(delegate.getEdges(), checkedEdgePredicate);
    }

    @Override
    public final boolean containsEdge(E edge) {
        return delegate.containsEdge(edge) && edgePredicate.test(edge);
    }

    @Override
    public final Iterable<? extends E> getEdges(N node,
                                                EdgeDirection direction) {
        return IterableUtils.filter(delegate.getEdges(node, direction), checkedEdgePredicate);
    }

    @Override
    public final N getTip(E edge,
                          EdgeTip tip) {
        return delegate.getTip(edge, tip);
    }
}