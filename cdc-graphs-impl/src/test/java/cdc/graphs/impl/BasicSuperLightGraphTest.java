package cdc.graphs.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.graphs.EdgeDirection;
import cdc.graphs.EdgeTip;
import cdc.util.function.IterableUtils;

class BasicSuperLightGraphTest {

    @Test
    void test1() {
        final BasicSuperLightGraph<String> g = new BasicSuperLightGraph<>();
        assertEquals(0, IterableUtils.size(g.getNodes()));
        assertEquals(0, IterableUtils.size(g.getEdges()));

        g.addNode("Hello");
        g.addNode("World");
        assertEquals(2, IterableUtils.size(g.getNodes()));
        assertEquals(0, IterableUtils.size(g.getEdges()));
        final BasicGraphEdge<String> e1 = g.addEdge("Hello", "World");
        g.addEdge("Hello", "Hello");
        g.addEdge("World", "World");
        assertEquals("Hello", g.getTip(e1, EdgeTip.SOURCE));
        assertEquals("World", g.getTip(e1, EdgeTip.TARGET));
        assertEquals(2, IterableUtils.size(g.getNodes()));
        assertEquals(3, IterableUtils.size(g.getEdges()));
        assertEquals(2, IterableUtils.size(g.getEdges("Hello")));
        assertEquals(1, IterableUtils.size(g.getEdges("Hello", EdgeDirection.INGOING)));
        assertEquals(2, IterableUtils.size(g.getEdges("Hello", EdgeDirection.OUTGOING)));
        g.removeEdge(e1);
        assertEquals(2, IterableUtils.size(g.getNodes()));
        assertEquals(2, IterableUtils.size(g.getEdges()));
        g.removeNode("Hello");
        assertEquals(1, IterableUtils.size(g.getNodes()));
        assertEquals(1, IterableUtils.size(g.getEdges()));
        assertTrue(g.containsEdge("World", "World"));
        assertFalse(g.containsEdge("Hello", "Hello"));
    }
}