package cdc.graphs.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.graphs.NodeRelativePosition;
import cdc.graphs.impl.tests.TestTree;
import cdc.graphs.impl.tests.TestTreeNode;

class TreeBasicQueriesTest {
    private final TestTree adapter = new TestTree();
    private final TestTreeNode c0 = new TestTreeNode("C0");
    private final TestTreeNode c1 = new TestTreeNode("C1", c0);
    private final TestTreeNode c11 = new TestTreeNode("C1.1", c1);
    private final TestTreeNode c12 = new TestTreeNode("C1.2", c1);
    private final TestTreeNode c13 = new TestTreeNode("C1.3", c1);
    private final TestTreeNode c131 = new TestTreeNode("C1.3.1", c13);
    private final TestTreeNode c2 = new TestTreeNode("C2", c0);
    private final TestTreeNode c21 = new TestTreeNode("C2.1", c2);
    private final TestTreeNode c22 = new TestTreeNode("C2.2", c2);
    private final TestTreeNode c23 = new TestTreeNode("C2.3", c2);

    @Test
    void testGetRoot() {
        assertEquals(c0, adapter.getRoot(c0));
        assertEquals(c0, adapter.getRoot(c1));
        assertEquals(c0, adapter.getRoot(c11));
        assertEquals(c0, adapter.getRoot(c12));
        assertEquals(c0, adapter.getRoot(c13));
        assertEquals(c0, adapter.getRoot(c131));
        assertEquals(c0, adapter.getRoot(c2));
        assertEquals(c0, adapter.getRoot(c21));
        assertEquals(c0, adapter.getRoot(c22));
        assertEquals(c0, adapter.getRoot(c23));
    }

    @Test
    void testIsRoot() {
        assertFalse(adapter.isRoot(null));
        assertTrue(adapter.isRoot(c0));
        assertFalse(adapter.isRoot(c1));
        assertFalse(adapter.isRoot(c11));
        assertFalse(adapter.isRoot(c12));
        assertFalse(adapter.isRoot(c13));
        assertFalse(adapter.isRoot(c131));
        assertFalse(adapter.isRoot(c2));
        assertFalse(adapter.isRoot(c21));
        assertFalse(adapter.isRoot(c22));
        assertFalse(adapter.isRoot(c23));
    }

    @Test
    void testIsLeaf() {
        assertFalse(adapter.isLeaf(null));
        assertFalse(adapter.isLeaf(c0));
        assertFalse(adapter.isLeaf(c1));
        assertTrue(adapter.isLeaf(c11));
        assertTrue(adapter.isLeaf(c12));
        assertFalse(adapter.isLeaf(c13));
        assertTrue(adapter.isLeaf(c131));
        assertFalse(adapter.isLeaf(c2));
        assertTrue(adapter.isLeaf(c21));
        assertTrue(adapter.isLeaf(c22));
        assertTrue(adapter.isLeaf(c23));
    }

    @Test
    void testGetDepth() {
        assertEquals(0, adapter.getDepth(c0));
        assertEquals(1, adapter.getDepth(c1));
        assertEquals(2, adapter.getDepth(c11));
        assertEquals(2, adapter.getDepth(c12));
        assertEquals(2, adapter.getDepth(c13));
        assertEquals(3, adapter.getDepth(c131));
        assertEquals(1, adapter.getDepth(c2));
        assertEquals(2, adapter.getDepth(c21));
        assertEquals(2, adapter.getDepth(c22));
        assertEquals(2, adapter.getDepth(c23));
    }

    @Test
    void testGetHeight() {
        assertEquals(3, adapter.getHeight(c0));
        assertEquals(2, adapter.getHeight(c1));
        assertEquals(0, adapter.getHeight(c11));
        assertEquals(0, adapter.getHeight(c12));
        assertEquals(1, adapter.getHeight(c13));
        assertEquals(0, adapter.getHeight(c131));
        assertEquals(1, adapter.getHeight(c2));
        assertEquals(0, adapter.getHeight(c21));
        assertEquals(0, adapter.getHeight(c22));
        assertEquals(0, adapter.getHeight(c23));
    }

    @Test
    void testGetFirstCommonAncestor() {

        assertEquals(null, adapter.getFirstCommonAncestor(null, null));
        assertEquals(null, adapter.getFirstCommonAncestor(null, c0));
        assertEquals(null, adapter.getFirstCommonAncestor(c0, null));

        assertEquals(c0, adapter.getFirstCommonAncestor(c0, c0));
        assertEquals(c0, adapter.getFirstCommonAncestor(c0, c1));
        assertEquals(c0, adapter.getFirstCommonAncestor(c0, c11));
        assertEquals(c0, adapter.getFirstCommonAncestor(c0, c12));
        assertEquals(c0, adapter.getFirstCommonAncestor(c0, c13));
        assertEquals(c0, adapter.getFirstCommonAncestor(c0, c131));
        assertEquals(c0, adapter.getFirstCommonAncestor(c0, c2));
        assertEquals(c0, adapter.getFirstCommonAncestor(c0, c21));
        assertEquals(c0, adapter.getFirstCommonAncestor(c0, c22));
        assertEquals(c0, adapter.getFirstCommonAncestor(c0, c23));

        assertEquals(c0, adapter.getFirstCommonAncestor(c1, c0));
        assertEquals(c1, adapter.getFirstCommonAncestor(c1, c1));
        assertEquals(c1, adapter.getFirstCommonAncestor(c1, c11));
        assertEquals(c1, adapter.getFirstCommonAncestor(c1, c12));
        assertEquals(c1, adapter.getFirstCommonAncestor(c1, c13));
        assertEquals(c1, adapter.getFirstCommonAncestor(c1, c131));
        assertEquals(c0, adapter.getFirstCommonAncestor(c1, c2));
        assertEquals(c0, adapter.getFirstCommonAncestor(c1, c21));
        assertEquals(c0, adapter.getFirstCommonAncestor(c1, c22));
        assertEquals(c0, adapter.getFirstCommonAncestor(c1, c23));

        assertEquals(c0, adapter.getFirstCommonAncestor(c11, c0));
        assertEquals(c1, adapter.getFirstCommonAncestor(c11, c1));
        assertEquals(c11, adapter.getFirstCommonAncestor(c11, c11));
        assertEquals(c1, adapter.getFirstCommonAncestor(c11, c12));
        assertEquals(c1, adapter.getFirstCommonAncestor(c11, c13));
        assertEquals(c1, adapter.getFirstCommonAncestor(c11, c131));
        assertEquals(c0, adapter.getFirstCommonAncestor(c11, c2));
        assertEquals(c0, adapter.getFirstCommonAncestor(c11, c21));
        assertEquals(c0, adapter.getFirstCommonAncestor(c11, c22));
        assertEquals(c0, adapter.getFirstCommonAncestor(c11, c23));

        assertEquals(c0, adapter.getFirstCommonAncestor(c12, c0));
        assertEquals(c1, adapter.getFirstCommonAncestor(c12, c1));
        assertEquals(c1, adapter.getFirstCommonAncestor(c12, c11));
        assertEquals(c12, adapter.getFirstCommonAncestor(c12, c12));
        assertEquals(c1, adapter.getFirstCommonAncestor(c12, c13));
        assertEquals(c1, adapter.getFirstCommonAncestor(c12, c131));
        assertEquals(c0, adapter.getFirstCommonAncestor(c12, c2));
        assertEquals(c0, adapter.getFirstCommonAncestor(c12, c21));
        assertEquals(c0, adapter.getFirstCommonAncestor(c12, c22));
        assertEquals(c0, adapter.getFirstCommonAncestor(c12, c23));

        assertEquals(c0, adapter.getFirstCommonAncestor(c13, c0));
        assertEquals(c1, adapter.getFirstCommonAncestor(c13, c1));
        assertEquals(c1, adapter.getFirstCommonAncestor(c13, c11));
        assertEquals(c1, adapter.getFirstCommonAncestor(c13, c12));
        assertEquals(c13, adapter.getFirstCommonAncestor(c13, c13));
        assertEquals(c13, adapter.getFirstCommonAncestor(c13, c131));
        assertEquals(c0, adapter.getFirstCommonAncestor(c13, c2));
        assertEquals(c0, adapter.getFirstCommonAncestor(c13, c21));
        assertEquals(c0, adapter.getFirstCommonAncestor(c13, c22));
        assertEquals(c0, adapter.getFirstCommonAncestor(c13, c23));
    }

    @Test
    void testIsOverOrEqual() {
        assertTrue(adapter.isOverOrEqual(null, null));
        assertFalse(adapter.isOverOrEqual(null, c0));
        assertFalse(adapter.isOverOrEqual(c0, null));

        assertTrue(adapter.isOverOrEqual(c0, c0));
        assertTrue(adapter.isOverOrEqual(c0, c1));
        assertTrue(adapter.isOverOrEqual(c0, c11));
        assertTrue(adapter.isOverOrEqual(c0, c12));
        assertTrue(adapter.isOverOrEqual(c0, c13));
        assertTrue(adapter.isOverOrEqual(c0, c131));
        assertTrue(adapter.isOverOrEqual(c0, c2));
        assertTrue(adapter.isOverOrEqual(c0, c21));
        assertTrue(adapter.isOverOrEqual(c0, c22));

        assertFalse(adapter.isOverOrEqual(c1, c0));
        assertTrue(adapter.isOverOrEqual(c1, c1));
        assertTrue(adapter.isOverOrEqual(c1, c11));
        assertTrue(adapter.isOverOrEqual(c1, c12));
        assertTrue(adapter.isOverOrEqual(c1, c13));
        assertTrue(adapter.isOverOrEqual(c1, c131));
        assertFalse(adapter.isOverOrEqual(c1, c2));
        assertFalse(adapter.isOverOrEqual(c1, c21));
        assertFalse(adapter.isOverOrEqual(c1, c22));
    }

    @Test
    void testIsStrictlyOver() {
        assertFalse(adapter.isStrictlyOver(null, null));
        assertFalse(adapter.isStrictlyOver(null, c0));
        assertFalse(adapter.isStrictlyOver(c0, null));

        assertFalse(adapter.isStrictlyOver(c0, c0));
        assertTrue(adapter.isStrictlyOver(c0, c1));
        assertTrue(adapter.isStrictlyOver(c0, c11));
        assertTrue(adapter.isStrictlyOver(c0, c12));
        assertTrue(adapter.isStrictlyOver(c0, c13));
        assertTrue(adapter.isStrictlyOver(c0, c131));
        assertTrue(adapter.isStrictlyOver(c0, c2));
        assertTrue(adapter.isStrictlyOver(c0, c21));
        assertTrue(adapter.isStrictlyOver(c0, c22));

        assertFalse(adapter.isStrictlyOver(c1, c0));
        assertFalse(adapter.isStrictlyOver(c1, c1));
        assertTrue(adapter.isStrictlyOver(c1, c11));
        assertTrue(adapter.isStrictlyOver(c1, c12));
        assertTrue(adapter.isStrictlyOver(c1, c13));
        assertTrue(adapter.isStrictlyOver(c1, c131));
        assertFalse(adapter.isStrictlyOver(c1, c2));
        assertFalse(adapter.isStrictlyOver(c1, c21));
        assertFalse(adapter.isStrictlyOver(c1, c22));
    }

    @Test
    void testIsUnderOrEqual() {
        assertTrue(adapter.isUnderOrEqual(null, null));
        assertFalse(adapter.isUnderOrEqual(null, c0));
        assertFalse(adapter.isUnderOrEqual(c0, null));

        assertTrue(adapter.isUnderOrEqual(c0, c0));
        assertFalse(adapter.isUnderOrEqual(c0, c1));
        assertFalse(adapter.isUnderOrEqual(c0, c11));
        assertFalse(adapter.isUnderOrEqual(c0, c12));
        assertFalse(adapter.isUnderOrEqual(c0, c13));
        assertFalse(adapter.isUnderOrEqual(c0, c131));
        assertFalse(adapter.isUnderOrEqual(c0, c2));
        assertFalse(adapter.isUnderOrEqual(c0, c21));
        assertFalse(adapter.isUnderOrEqual(c0, c22));

        assertTrue(adapter.isUnderOrEqual(c1, c0));
        assertTrue(adapter.isUnderOrEqual(c1, c1));
        assertFalse(adapter.isUnderOrEqual(c1, c11));
        assertFalse(adapter.isUnderOrEqual(c1, c12));
        assertFalse(adapter.isUnderOrEqual(c1, c13));
        assertFalse(adapter.isUnderOrEqual(c1, c131));
        assertFalse(adapter.isUnderOrEqual(c1, c2));
        assertFalse(adapter.isUnderOrEqual(c1, c21));
        assertFalse(adapter.isUnderOrEqual(c1, c22));
    }

    @Test
    void testGetRelativePosition() {
        assertEquals(NodeRelativePosition.SAME, adapter.getRelativePosition(c0, c0));
        assertEquals(NodeRelativePosition.STRICTLY_OVER, adapter.getRelativePosition(c0, c1));
        assertEquals(NodeRelativePosition.STRICTLY_OVER, adapter.getRelativePosition(c0, c131));
        assertEquals(NodeRelativePosition.STRICTLY_UNDER, adapter.getRelativePosition(c1, c0));
        assertEquals(NodeRelativePosition.UNRELATED, adapter.getRelativePosition(c1, c2));
    }

    @Test
    void testCanSetParent() {
        assertFalse(adapter.canSetParent(null, null));
        assertFalse(adapter.canSetParent(null, c0));
        assertTrue(adapter.canSetParent(c0, null));
        assertFalse(adapter.canSetParent(c0, c0));
        assertFalse(adapter.canSetParent(c0, c1));
        assertTrue(adapter.canSetParent(c1, c0));
        assertTrue(adapter.canSetParent(c11, c0));
    }
}