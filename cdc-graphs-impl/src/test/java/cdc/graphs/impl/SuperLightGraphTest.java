package cdc.graphs.impl;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.graphs.EdgeDirection;
import cdc.util.function.IterableUtils;

class SuperLightGraphTest {

    @Test
    void test() {
        final BasicSuperLightGraph<String> graph = new BasicSuperLightGraph<>();
        assertSame(0, graph.getEdgesCount());
        assertSame(0, graph.getNodesCount());

        graph.addNode("N1");
        graph.addNode("N2");
        assertSame(0, graph.getEdgesCount());
        assertSame(2, graph.getNodesCount());

        graph.addEdge("N1", "N2");
        assertSame(1, graph.getEdgesCount());
        assertSame(2, graph.getNodesCount());

        assertTrue(graph.containsEdge("N1", "N2"));
        assertFalse(graph.containsEdge("N2", "N1"));

        assertTrue(graph.containsNode("N1"));
        assertFalse(graph.containsNode("N3"));

        assertSame(1, IterableUtils.size(graph.getEdges("N1")));
        assertSame(1, IterableUtils.size(graph.getEdges("N2")));

        assertSame(1, IterableUtils.size(graph.getEdges("N1", EdgeDirection.OUTGOING)));
        assertSame(0, IterableUtils.size(graph.getEdges("N1", EdgeDirection.INGOING)));
        assertSame(0, IterableUtils.size(graph.getEdges("N2", EdgeDirection.OUTGOING)));
        assertSame(1, IterableUtils.size(graph.getEdges("N2", EdgeDirection.INGOING)));

        assertSame(1, graph.getConnectedNodes("N1", EdgeDirection.OUTGOING).size());
        assertSame(0, graph.getConnectedNodes("N1", EdgeDirection.INGOING).size());
        assertSame(0, graph.getConnectedNodes("N2", EdgeDirection.OUTGOING).size());
        assertSame(1, graph.getConnectedNodes("N2", EdgeDirection.INGOING).size());

        assertTrue(graph.getConnectedNodes("N1", EdgeDirection.OUTGOING).contains("N2"));
        assertTrue(graph.getConnectedNodes("N2", EdgeDirection.INGOING).contains("N1"));
    }
}