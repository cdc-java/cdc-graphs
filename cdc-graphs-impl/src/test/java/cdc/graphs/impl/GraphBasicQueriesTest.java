package cdc.graphs.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import cdc.graphs.EdgeDirection;
import cdc.graphs.EdgeTip;
import cdc.graphs.NodeConnectivity;
import cdc.graphs.impl.tests.TestGraphHeavyEdge;
import cdc.graphs.impl.tests.TestGraphHeavyNode;
import cdc.graphs.impl.tests.TestHeavyGraph;
import cdc.util.function.IterableUtils;

class GraphBasicQueriesTest {

    @SafeVarargs
    private static <T> Set<T> set(T... values) {
        final Set<T> result = new HashSet<>();
        for (final T value : values) {
            result.add(value);
        }
        return result;
    }

    @Test
    void testBasic() {
        final TestHeavyGraph g = new TestHeavyGraph();
        // final GraphBasicQueries<TestGraphHeavyNode, TestGraphHeavyEdge> gbq = new GraphBasicQueries<>(g);
        assertFalse(g.hasNodes());
        assertFalse(g.hasEdges());

        final TestGraphHeavyNode n1 = g.createNode("N1");
        assertTrue(g.hasNodes());
        assertEquals(1, g.getNodesCount());
        assertFalse(g.hasEdges());
        assertTrue(IterableUtils.isEmpty(n1.getIngoings()));
        assertTrue(IterableUtils.isEmpty(n1.getOutgoings()));
        // assertTrue(g.getNodes().contains(n1));
        assertTrue(IterableUtils.isEmpty(g.getEdges(n1)));
        assertTrue(IterableUtils.isEmpty(g.getEdges(n1, EdgeDirection.OUTGOING)));
        assertTrue(IterableUtils.isEmpty(g.getEdges(n1, EdgeDirection.INGOING)));
        assertTrue(IterableUtils.isEmpty(g.getConnectedNodes(n1)));
        assertTrue(IterableUtils.isEmpty(g.getConnectedNodes(n1, EdgeDirection.OUTGOING)));
        assertTrue(IterableUtils.isEmpty(g.getConnectedNodes(n1, EdgeDirection.INGOING)));
        assertFalse(g.hasEdges(n1, null));
        assertFalse(g.hasEdges(n1, EdgeDirection.OUTGOING));
        assertFalse(g.hasEdges(n1, EdgeDirection.INGOING));
        assertFalse(g.hasEdge(n1, n1));
        assertTrue(g.hasNode("N1"));
        assertFalse(g.hasNode("N2"));

        final TestGraphHeavyNode n2 = g.createNode("N2");
        assertTrue(g.hasNodes());
        assertEquals(2, g.getNodesCount());
        assertFalse(g.hasEdges());
        assertTrue(IterableUtils.isEmpty(n2.getIngoings()));
        assertTrue(IterableUtils.isEmpty(n2.getOutgoings()));

        final TestGraphHeavyEdge e12 = g.createEdge("E12", "N1", "N2");
        assertTrue(g.hasNodes());
        assertEquals(2, g.getNodesCount());
        assertTrue(g.hasEdges());
        assertEquals(1, g.getEdgesCount());
        assertEquals(n1, e12.getSource());
        assertEquals(n2, e12.getTarget());
        assertEquals(1, IterableUtils.size(n1.getOutgoings()));
        assertEquals(0, IterableUtils.size(n1.getIngoings()));
        assertEquals(0, IterableUtils.size(n2.getOutgoings()));
        assertEquals(1, IterableUtils.size(n2.getIngoings()));
        // assertTrue(g.getEdges().contains(e12));
        assertEquals(1, IterableUtils.size(g.getEdges(n1)));
        assertEquals(1, IterableUtils.size(g.getEdges(n1, EdgeDirection.OUTGOING)));
        assertEquals(0, IterableUtils.size(g.getEdges(n1, EdgeDirection.INGOING)));
        assertEquals(1, IterableUtils.size(g.getEdges(n2)));
        assertEquals(0, IterableUtils.size(g.getEdges(n2, EdgeDirection.OUTGOING)));
        assertEquals(1, IterableUtils.size(g.getEdges(n2, EdgeDirection.INGOING)));
        assertEquals(1, IterableUtils.size(g.getConnectedNodes(n1, null)));
        assertEquals(1, IterableUtils.size(g.getConnectedNodes(n1, EdgeDirection.OUTGOING)));
        assertEquals(0, IterableUtils.size(g.getConnectedNodes(n1, EdgeDirection.INGOING)));
        assertEquals(1, IterableUtils.size(g.getConnectedNodes(n2, null)));
        assertEquals(0, IterableUtils.size(g.getConnectedNodes(n2, EdgeDirection.OUTGOING)));
        assertEquals(1, IterableUtils.size(g.getConnectedNodes(n2, EdgeDirection.INGOING)));
        assertTrue(g.hasEdge(n1, n2));
        assertFalse(g.hasEdge(n2, n1));
        assertTrue(g.hasNode("N1"));
        assertTrue(g.hasNode("N2"));
        assertTrue(g.hasEdge("E12"));
        assertFalse(g.hasEdge("E21"));

        final TestGraphHeavyEdge e21 = g.createEdge("E21", "N2", "N1");
        assertTrue(g.hasNodes());
        assertEquals(2, g.getNodesCount());
        assertTrue(g.hasEdges());
        assertEquals(2, g.getEdgesCount());
        assertEquals(n2, e21.getSource());
        assertEquals(n1, e21.getTarget());
        assertEquals(1, IterableUtils.size(n1.getOutgoings()));
        assertEquals(1, IterableUtils.size(n1.getIngoings()));
        assertEquals(1, IterableUtils.size(n2.getOutgoings()));
        assertEquals(1, IterableUtils.size(n2.getIngoings()));
        assertEquals(2, g.getEdgesCount(n1, null));
        assertEquals(1, g.getEdgesCount(n1, EdgeDirection.OUTGOING));
        assertEquals(1, g.getEdgesCount(n1, EdgeDirection.INGOING));
        assertTrue(g.hasEdges(n1, null));
        assertTrue(g.hasEdges(n1, EdgeDirection.OUTGOING));
        assertTrue(g.hasEdges(n1, EdgeDirection.INGOING));
        assertTrue(g.hasEdge(n1, n2));
        assertTrue(g.hasEdge(n2, n1));
        assertEquals(n1, e12.getTip(EdgeTip.SOURCE));
        assertEquals(n2, e12.getTip(EdgeTip.TARGET));

        g.removeEdge("E21");
        assertEquals(2, g.getNodesCount());
        assertEquals(1, g.getEdgesCount());
        assertEquals(1, IterableUtils.size(n1.getOutgoings()));
        assertEquals(0, IterableUtils.size(n1.getIngoings()));
        assertEquals(0, IterableUtils.size(n2.getOutgoings()));
        assertEquals(1, IterableUtils.size(n2.getIngoings()));

        g.removeNode("N2");
        assertEquals(1, g.getNodesCount());
        assertEquals(0, g.getEdgesCount());
        assertEquals(0, IterableUtils.size(n1.getOutgoings()));
        assertEquals(0, IterableUtils.size(n1.getIngoings()));
        assertEquals(0, IterableUtils.size(n2.getOutgoings()));
        assertEquals(0, IterableUtils.size(n2.getIngoings()));

        g.clear();
        assertFalse(g.hasNodes());
        assertFalse(g.hasEdges());
    }

    @Test
    void testConnectivity() {
        final TestHeavyGraph g = new TestHeavyGraph();
        // final GraphBasicQueries<TestGraphHeavyNode, TestGraphHeavyEdge> gbq = new GraphBasicQueries<>(g);
        final TestGraphHeavyNode n1 = g.createNode("N1");
        final TestGraphHeavyNode n2 = g.createNode("N2");
        final TestGraphHeavyNode n3 = g.createNode("N3");
        final TestGraphHeavyNode n4 = g.createNode("N4");
        final TestGraphHeavyNode n5 = g.createNode("N5");
        g.createEdge("E12", "N1", "N2");
        g.createEdge("E23", "N2", "N3");
        g.createEdge("E44", "N4", "N4");
        assertTrue(g.isRoot(n1));
        assertFalse(g.isLeaf(n1));
        assertFalse(g.isRoot(n2));
        assertFalse(g.isLeaf(n2));
        assertFalse(g.isRoot(n3));
        assertTrue(g.isLeaf(n3));
        assertFalse(g.isRoot(n4));
        assertFalse(g.isLeaf(n4));
        assertTrue(g.isRoot(n5));
        assertTrue(g.isLeaf(n5));

        assertEquals(NodeConnectivity.OUT, g.getConnectivity(n1));
        assertEquals(NodeConnectivity.IN_OUT, g.getConnectivity(n2));
        assertEquals(NodeConnectivity.IN, g.getConnectivity(n3));
        assertEquals(NodeConnectivity.IN_OUT, g.getConnectivity(n4));
        assertEquals(NodeConnectivity.ISOLATED, g.getConnectivity(n5));

        assertEquals(set(n1, n5), g.getRoots());
        assertEquals(set(n3, n5), g.getLeaves());

        g.clear();
        assertFalse(g.hasNodes());
        assertFalse(g.hasEdges());
    }
}