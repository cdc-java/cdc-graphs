# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]
### Changed
- Updated dependencies:
    - org.junit-5.12.0


## [0.71.3] - 2025-01-03
### Changed
- Updated dependencies:
    - cdc-gv-0.100.3
    - cdc-util-0.54.0
    - org.junit-5.11.4
    - org.apache.log4j-2.24.3


## [0.71.2] - 2024-09-28
### Changed
- Updated dependencies:
    - cdc-gv-0.100.2
    - cdc-util-0.53.0
    - org.junit-5.11.1


## [0.71.1] - 2024-05-18
### Changed
- Updated dependencies:
    - cdc-gv-0.100.1
    - cdc-util-0.52.1
    - org.apache.log4j-2.23.1
    - org.junit-5.10.2
- Updated maven plugins.


## [0.71.0] - 2024-01-01
### Changed
- Updated dependencies:
    - cdc-gv-0.100.0
    - cdc-util-0.52.0
- Updated maven plugins.


## [0.70.0] - 2023-11-25
### Changed
- Moved to Java 17
- Updated dependencies:
    - cdc-gv-0.99.0
    - cdc-util-0.50.0


## [0.65.5] - 2023-11-18
### Changed
- Updated dependencies:
    - cdc-gv-0.96.9
    - cdc-util-0.33.2


## [0.65.4] - 2023-11-11
### Fixed
- Fixed a new bug in detection of connections in `SlimGraph`. #6


## [0.65.3] - 2023-11-11 [YANKED]
### Changed
- Updated dependencies:
    - org.junit-5.10.1

### Fixed
- Fixed bug in detection of connections in `SlimGraph`. #6


## [0.65.2] - 2023-10-21
### Changed
- Updated dependencies:
    - cdc-gv-0.96.8
    - cdc-util-0.33.1
    - org.junit-5.10.0
    - org.openjdk.jmh-1.37
- Updated maven plugins.
- Added maven enforcer plugin.
- Used `java.version` and `maven.version` properties in pom. 


## [0.65.1] - 2023-07-15
### Changed
- Updated dependencies:
    - cdc-gv-0.96.7
    - cdc-util-0.33.0


## [0.65.0] - 2023-05-01
### Changed
- Changed return type of `BasicLightGraph.getNodes()` and `BasicLightGraph.getEdges()`
  from `Iterable` to `Set`.
- Updated dependencies:
    - cdc-gv-0.96.6
    - cdc-util-0.32.0
    - org.junit-5.9.3

### Added
- Added `BasicSuperLightGraph.addEdgeIfMissing()`.


## [0.64.5] - 2023-04-15
### Changed
- Updated dependencies:
    - cdc-gv-0.96.5


## [0.64.4] - 2023-02-25
### Changed
- Updated dependencies:
    - cdc-gv-0.96.4
    - cdc-util-0.31.0
    - org.apache.log4j-2.20.0


## [0.64.3] - 2023-01-28
### Changed
- Updated dependencies:
    - cdc-gv-0.96.3
    - cdc-util-0.29.0
    - org.junit-5.9.1


## [0.64.2] - 2023-01-02
### Changed
- Updated dependencies:
    - cdc-gv-0.96.2
    - cdc-util-0.28.2
    - org.openjdk.jmh-1.36


## [0.64.1] - 2022-11-09
### Changed
- Updated dependencies:
    - cdc-gv-0.96.1
    - cdc-util-0.28.1


## [0.64.0] - 2022-10-02
### Added
- Created `GraphEquivalence`. It can be used to compare graphs, particularly for unit tests. #5

### Changed
- Moved `BijectionGenerator` and `RandomGraphGenerator` from test to main.
- Renamed `TestLighGraph` to `TestLightGraph`.
- Updated dependencies:
    - org.apache.log4j-2.19.0
    - org.junit-5.9.1


## [0.63.2] - 2022-08-23
### Changed
- Updated dependencies:
    - cdc-gv-0.96.0
    - cdc-util-0.28.0
    - org.junit-5.9.0


## [0.63.1] - 2022-07-07
### Changed
- Updated dependencies:
    - cdc-gv-0.95.5
    - cdc-util-0.27.0
    - org.apache.log4j-2.18.0
    - org.junit-5.9.0-RC1


## [0.63.0] - 2022-06-18
### Added
- Added `parent()` and `hasCommonItemsWith()` to `GraphPath`. #cdc-java/cdc-applic#157
- Updated dependencies:
    - cdc-gv-0.95.4
    - cdc-util-0.26.0

### Removed
- `GraphPath` constructors which are now private. #4


## [0.62.0] - 2022-05-30
### Added
- Created `GraphPath.Builder`. #4

### Changed
- `GraphPath` is now immutable. Use `GraphPath.Builder` if necessary. #4


## [0.61.1] - 2022-05-19
### Changed
- Updated maven plugins
- Updated dependencies:
    - cdc-gv-0.95.3
    - cdc-util-0.25.0
    - org.openjdk.jmh-1.35


## [0.61.0] - 2022-03-11
### Added
- Created `SlimGraph` that contains algorithms the can be used with graphs whose edges are not objects. #3

### Changed
- Updated dependencies:
    - cdc-gv-0.95.2
    - cdc-util-0.23.0
    - org.apache.log4j-2.17.2


## [0.60.1] - 2022-02-13
### Changed
- Updated dependencies:
    - cdc-gv-0.95.1
    - cdc-util-0.20.0


## [0.60.0] - 2022-02-04
### Changed
- Updated maven plugins
- Upgraded to Java 11
- Updated dependencies:
    - cdc-gv-0.95.0


## [0.52.2] - 2022-01-03
### Security
- Updated dependencies:
    - cdc-util-0.14.2
    - cdc-gv-0.92.2
    - org.apache.log4j-2.17.1. #2


## [0.52.1] - 2021-12-28
### Security
- Updated dependencies:
    - cdc-util-0.14.1
    - cdc-gv-0.92.1
    - org.apache.log4j-2.17.0. #2


## [0.52.0] - 2021-12-14
### Security
- Updated dependencies:
    - cdc-util-0.14.0
    - cdc-gv-0.92.0
    - org.apache.log4j-2.16.0. #2


## [0.51.2] - 2021-10-02
### Changed
- Updated dependencies


## [0.51.1] - 2021-07-23
### Changed
- Updated dependencies


## [0.51.0] - 2021-05-03
### Changed
- Renamed `cdc.graphs.api`to `cdc.graphs`. #1
- Updated dependencies.


## [0.50.1] - 2021-04-11
### Fixed
- Added missing tag in bom. cdc-java/cdc-util#37

## [0.50.0] - 2021-04-11 [YANKED]
### Added
- First release, extracted from [cdc-utils](https://gitlab.com/cdc-java/cdc-util). cdc-java/cdc-util#37
