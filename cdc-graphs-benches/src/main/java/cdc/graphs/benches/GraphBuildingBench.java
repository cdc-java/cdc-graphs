package cdc.graphs.benches;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.graphs.impl.BasicLightGraph;
import cdc.graphs.impl.BasicSuperLightGraph;
import cdc.util.bench.BenchUtils;

/**
 * TODO time spent to construct nodes and edges is counted. It should not
 *
 * @author Damien Carbonne
 */
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(value = 1,
        jvmArgs = { "-Xms1G", "-Xmx8G" })
@Warmup(iterations = 5,
        time = 100,
        timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 10,
        time = 250,
        timeUnit = TimeUnit.MILLISECONDS)
public class GraphBuildingBench {
    @Param({ "1000" })
    public int nodes;

    @Param({ "10" })
    public int edges;

    @Param({ "false", "true" })
    public boolean sorted;

    @Param({ "false", "true" })
    public boolean checks;

    @Param({ "SET", "LIST" })
    public BasicLightGraph.CollectionKind collectionKind;

    private static String n(int index) {
        return "N" + index;
    }

    @Benchmark
    public void benchString() {
        final BasicSuperLightGraph<String> g = new BasicSuperLightGraph<>(sorted, collectionKind);
        g.setChecksEnabled(checks);
        for (int i = 0; i < nodes; i++) {
            g.addNode(n(i));
        }
        for (int i = 0; i < nodes - edges; i++) {
            for (int j = 0; j < edges; j++) {
                g.addEdge(n(i), n(i + j));
            }
        }
    }

    @Benchmark
    public void benchInteger() {
        final BasicSuperLightGraph<Integer> g = new BasicSuperLightGraph<>(sorted, collectionKind);
        g.setChecksEnabled(checks);
        for (int i = 0; i < nodes; i++) {
            g.addNode(i);
        }
        for (int i = 0; i < nodes - edges; i++) {
            for (int j = 0; j < edges; j++) {
                g.addEdge(i, i + j);
            }
        }
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(GraphBuildingBench.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", GraphBuildingBench.class, ".csv"))
                                    .forks(1)
                                    .build();
        new Runner(opt).run();
    }
}