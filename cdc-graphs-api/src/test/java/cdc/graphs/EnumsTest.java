package cdc.graphs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.util.coverage.Coverage;
import cdc.util.function.Evaluation;

class EnumsTest {
    @Test
    void testEdgeDirection() {
        Coverage.enumStandardCoverage(EdgeDirection.class);

        assertEquals(EdgeDirection.OUTGOING, EdgeDirection.INGOING.opposite());
        assertEquals(EdgeDirection.INGOING, EdgeDirection.OUTGOING.opposite());
    }

    @Test
    void testEdgeDirectionMask() {
        Coverage.enumStandardCoverage(EdgeDirectionMask.class);
        assertEquals(EdgeDirectionMask.NONE, EdgeDirectionMask.NONE.setEnabled(EdgeDirection.INGOING, false));
        assertEquals(EdgeDirectionMask.INGOING, EdgeDirectionMask.NONE.setEnabled(EdgeDirection.INGOING, true));
        assertEquals(EdgeDirectionMask.NONE, EdgeDirectionMask.NONE.setEnabled(EdgeDirection.OUTGOING, false));
        assertEquals(EdgeDirectionMask.OUTGOING, EdgeDirectionMask.NONE.setEnabled(EdgeDirection.OUTGOING, true));

        assertEquals(EdgeDirectionMask.NONE, EdgeDirectionMask.INGOING.setEnabled(EdgeDirection.INGOING, false));
        assertEquals(EdgeDirectionMask.INGOING, EdgeDirectionMask.INGOING.setEnabled(EdgeDirection.INGOING, true));
        assertEquals(EdgeDirectionMask.INGOING, EdgeDirectionMask.INGOING.setEnabled(EdgeDirection.OUTGOING, false));
        assertEquals(EdgeDirectionMask.BOTH, EdgeDirectionMask.INGOING.setEnabled(EdgeDirection.OUTGOING, true));

        assertEquals(EdgeDirectionMask.OUTGOING, EdgeDirectionMask.OUTGOING.setEnabled(EdgeDirection.INGOING, false));
        assertEquals(EdgeDirectionMask.BOTH, EdgeDirectionMask.OUTGOING.setEnabled(EdgeDirection.INGOING, true));
        assertEquals(EdgeDirectionMask.NONE, EdgeDirectionMask.OUTGOING.setEnabled(EdgeDirection.OUTGOING, false));
        assertEquals(EdgeDirectionMask.OUTGOING, EdgeDirectionMask.OUTGOING.setEnabled(EdgeDirection.OUTGOING, true));

        assertEquals(EdgeDirectionMask.OUTGOING, EdgeDirectionMask.BOTH.setEnabled(EdgeDirection.INGOING, false));
        assertEquals(EdgeDirectionMask.BOTH, EdgeDirectionMask.BOTH.setEnabled(EdgeDirection.INGOING, true));
        assertEquals(EdgeDirectionMask.INGOING, EdgeDirectionMask.BOTH.setEnabled(EdgeDirection.OUTGOING, false));
        assertEquals(EdgeDirectionMask.BOTH, EdgeDirectionMask.BOTH.setEnabled(EdgeDirection.OUTGOING, true));

        assertFalse(EdgeDirectionMask.NONE.isEnabled(EdgeDirection.INGOING));
        assertFalse(EdgeDirectionMask.NONE.isEnabled(EdgeDirection.OUTGOING));
        assertTrue(EdgeDirectionMask.INGOING.isEnabled(EdgeDirection.INGOING));
        assertFalse(EdgeDirectionMask.INGOING.isEnabled(EdgeDirection.OUTGOING));
        assertFalse(EdgeDirectionMask.OUTGOING.isEnabled(EdgeDirection.INGOING));
        assertTrue(EdgeDirectionMask.OUTGOING.isEnabled(EdgeDirection.OUTGOING));
        assertTrue(EdgeDirectionMask.BOTH.isEnabled(EdgeDirection.INGOING));
        assertTrue(EdgeDirectionMask.BOTH.isEnabled(EdgeDirection.OUTGOING));
    }

    @Test
    void testEdgeTip() {
        Coverage.enumStandardCoverage(EdgeTip.class);

        assertEquals(EdgeTip.TARGET, EdgeTip.SOURCE.opposite());
        assertEquals(EdgeTip.SOURCE, EdgeTip.TARGET.opposite());
    }

    @Test
    void testNodeConnectivity() {
        Coverage.enumStandardCoverage(NodeConnectivity.class);

        assertTrue(NodeConnectivity.IN_OUT.hasOut());
        assertFalse(NodeConnectivity.IN.hasOut());
        assertTrue(NodeConnectivity.OUT.hasOut());
        assertFalse(NodeConnectivity.ISOLATED.hasOut());

        assertTrue(NodeConnectivity.IN_OUT.hasIn());
        assertTrue(NodeConnectivity.IN.hasIn());
        assertFalse(NodeConnectivity.OUT.hasIn());
        assertFalse(NodeConnectivity.ISOLATED.hasIn());
    }

    @Test
    void testGraphElementKind() {
        assertTrue(Coverage.enumStandardCoverage(GraphElementKind.class));
    }

    @Test
    void testTraversalOrder() {
        assertTrue(Coverage.enumStandardCoverage(TraversalOrder.class));
    }

    @Test
    void testTraversalMethod() {
        assertTrue(Coverage.enumStandardCoverage(TraversalMethod.class));
    }

    @Test
    void testNodeRelativePosition() {
        assertTrue(Coverage.enumStandardCoverage(NodeRelativePosition.class));
    }

    @Test
    void testEvaluation() {
        assertTrue(Coverage.enumStandardCoverage(Evaluation.class));
    }
}