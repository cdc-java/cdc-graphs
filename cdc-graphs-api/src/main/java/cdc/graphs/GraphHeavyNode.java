package cdc.graphs;

/**
 * Basic interface for graph heavy nodes.
 * <p>
 * A heavy node has knowledge of related edges.
 *
 * @author Damien Carbonne
 *
 * @param <E> Edge type.
 */
public interface GraphHeavyNode<E> {
    /**
     * @return The outgoing edges.
     */
    public Iterable<E> getOutgoings();

    /**
     * @return The ingoing edges.
     */
    public Iterable<E> getIngoings();
}