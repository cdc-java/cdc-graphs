package cdc.graphs;

/**
 * Enumeration of possible relationships of two elements in a partial order.
 *
 * @author Damien Carbonne
 */
public enum PartialOrderPosition {
    /**
     * The 2 elements are equal: {@code X = Y}
     */
    EQUAL,

    /**
     * The first element is strictly less than the second one: {@code X < Y}
     */
    LESS_THAN,

    /**
     * The first element is strictly greater than the second one: {@code X > Y}
     */
    GREATER_THAN,

    /**
     * The two elements are unrelated.
     */
    UNRELATED;

    public boolean related() {
        return this != UNRELATED;
    }

    public boolean unrelated() {
        return this == UNRELATED;
    }
}