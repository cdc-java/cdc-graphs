package cdc.graphs;

/**
 * Enumeration of element kinds.
 *
 * @author Damien Carbonne
 */
public enum GraphElementKind {
    /**
     * The element is a node.
     */
    NODE,

    /**
     * The element is an edge.
     */
    EDGE
}