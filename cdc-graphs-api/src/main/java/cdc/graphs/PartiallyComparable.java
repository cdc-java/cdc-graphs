package cdc.graphs;

/**
 * Interface implemented by objects that are partially comparable.
 *
 * @author Damien Carbonne
 *
 * @param <T> The object type.
 */
public interface PartiallyComparable<T> {

    /**
     * Return the partial comparison of this object with another one.
     *
     * @param o The other object.
     * @return {@link PartialOrderPosition#LESS_THAN LESS_THAN}, {@link PartialOrderPosition#EQUAL EQUAL},
     *         {@link PartialOrderPosition#GREATER_THAN GREATER_THAN} or {@link PartialOrderPosition#UNRELATED UNRELATED}
     *         if this object is less than, equal to, greater than, or unrelated to {@code o}.
     * @throws IllegalArgumentException When {@code o} is {@code null}.
     */
    public PartialOrderPosition partialCompareTo(T o);

    /**
     * Convert a total order comparison to a partial order comparison.
     *
     * @param <E> The Comparable type.
     * @param left The left value.
     * @param right The right value.
     * @return The conversion of {@code left.compareTo(right)} into {@link PartialOrderPosition}.
     */
    public static <E extends Comparable<? super E>> PartialOrderPosition partialCompare(E left,
                                                                                        E right) {
        final int cmp = left.compareTo(right);
        if (cmp < 0) {
            return PartialOrderPosition.LESS_THAN;
        } else if (cmp > 0) {
            return PartialOrderPosition.GREATER_THAN;
        } else {
            return PartialOrderPosition.EQUAL;
        }
    }
}