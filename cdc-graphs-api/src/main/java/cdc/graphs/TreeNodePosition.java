package cdc.graphs;

/**
 * Enumeration of possible node positions in a tree.
 *
 * @author Damien Carbonne
 */
public enum TreeNodePosition {
    /**
     * The node has no parent and at least one child.
     */
    ROOT,

    /**
     * The node has no parent and no children.
     */
    ROOT_LEAF,

    /**
     * The node has a parent and no children.
     */
    LEAF,

    /**
     * The node has a parent and at least one child.
     */
    BRANCH;

    public boolean isRoot() {
        return this == ROOT || this == ROOT_LEAF;
    }

    public boolean isLeaf() {
        return this == ROOT_LEAF || this == LEAF;
    }
}