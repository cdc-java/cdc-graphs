package cdc.graphs;

/**
 * Enumeration of possible relative position of 2 nodes in a directed graph.
 *
 * @author Damien Carbonne
 */
public enum NodeRelativePosition {
    /**
     * The 2 nodes are the same.
     */
    SAME,

    /**
     * The first node is over the second one.
     * <p>
     * There is a path from first node to second one.
     */
    STRICTLY_OVER,

    /**
     * The first node is under the second one.
     * <p>
     * There is a path from second bode to first one.
     */
    STRICTLY_UNDER,

    /**
     * There is a loop passing through both nodes.
     * <p>
     * This should not exist if the graph describes a partial or total order.
     */
    IN_LOOP,

    /**
     * The 2 nodes are not related.
     */
    UNRELATED
}