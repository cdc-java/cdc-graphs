package cdc.graphs;

import cdc.util.lang.Checks;

/**
 * Basic interface for graph edges.
 *
 * @author Damien Carbonne
 *
 * @param <N> Node type.
 */
public interface GraphEdge<N> {
    /**
     * @return The source node.
     */
    public N getSource();

    /**
     * @return The target node.
     */
    public N getTarget();

    /**
     * Returns the source or target node.
     *
     * @param tip Tip to return. Must not be null.
     * @return Source or target node, depending on tip.
     */
    public default N getTip(EdgeTip tip) {
        Checks.isNotNull(tip, "tip");
        if (tip == EdgeTip.SOURCE) {
            return getSource();
        } else {
            return getTarget();
        }
    }
}