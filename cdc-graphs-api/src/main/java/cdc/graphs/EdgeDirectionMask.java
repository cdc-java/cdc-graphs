package cdc.graphs;

import cdc.util.lang.IntMasks;

/**
 * Mask of EdgeDirections.
 * <p>
 * Implemented as an enum.
 * <p>
 * <b>WARNING:</b> Order of declarations matters and must be complaint with EdgeDirection.
 *
 * @author Damien Carbonne
 */
public enum EdgeDirectionMask {
    NONE,
    OUTGOING,
    INGOING,
    BOTH;

    public boolean isEnabled(EdgeDirection value) {
        return IntMasks.isEnabled(ordinal(), value);
    }

    public EdgeDirectionMask setEnabled(EdgeDirection value,
                                        boolean enabled) {
        return values()[IntMasks.setEnabled(ordinal(), value, enabled)];
    }
}