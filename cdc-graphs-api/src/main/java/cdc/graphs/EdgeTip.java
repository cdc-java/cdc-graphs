package cdc.graphs;

/**
 * Enumeration of possible edge tips.
 *
 * @author Damien Carbonne
 */
public enum EdgeTip {
    /**
     * Tip designates the source node.
     */
    SOURCE,

    /**
     * Tip designate the target node.
     */
    TARGET;

    public EdgeTip opposite() {
        return this == SOURCE ? TARGET : SOURCE;
    }
}