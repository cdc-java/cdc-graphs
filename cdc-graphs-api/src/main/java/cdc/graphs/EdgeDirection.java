package cdc.graphs;

/**
 * Enumeration of possible edge directions (OUTGOING, INGOING).
 * Used for directed graphs.
 *
 * @author Damien Carbonne
 */
public enum EdgeDirection {
    /**
     * The edge is leaving the node.
     */
    OUTGOING,

    /**
     * The edge is entering the node.
     */
    INGOING;

    public EdgeDirection opposite() {
        return this == OUTGOING ? INGOING : OUTGOING;
    }
}