package cdc.graphs;

/**
 * Enumeration of possible node connectivities.
 * This describes the existence of outgoing or ingoing edges on a node.
 *
 * @author Damien Carbonne
 */
public enum NodeConnectivity {
    /** The node has only outgoing edges. */
    OUT,

    /** The node has only ingoing edges. */
    IN,

    /** The node has both outgoing and ingoing edges. */
    IN_OUT,

    /** The node has no ingoing or outgoing eges. */
    ISOLATED;

    public boolean hasOut() {
        return this == OUT || this == IN_OUT;
    }

    public boolean hasIn() {
        return this == IN || this == IN_OUT;
    }
}