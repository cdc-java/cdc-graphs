package cdc.graphs;

import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;

/**
 * Interface giving access to a lightweight directed tree.
 * <p>
 * Edges are not represented as objects.<br>
 * It is the responsibility of implementation to ensure that the tree is consistent.
 *
 *
 * @author Damien Carbonne
 *
 * @param <N> Node class.
 */
public interface TreeAdapter<N> {
    /**
     * Returns the parent node of this node, possibly {@code null}.
     *
     * @param node The node. <em>MUST NOT</em> be {@code null}.
     * @return The parent of node, or null.
     */
    public N getParent(N node);

    /**
     * Returns an Iterable of children nodes.
     *
     * @param node The node. <em>MUST NOT</em> be {@code null}.
     * @return An Iterable of children nodes.
     */
    public Iterable<N> getChildren(N node);

    /**
     * Returns {@code true} when a node has children.
     * <p>
     * This could be deduced from getChildren(), but may provide a better implementation.
     *
     * @param node The node. <em>MUST NOT</em> be {@code null}.
     * @return {@code true} when node has children.
     */
    public boolean hasChildren(N node);

    /**
     * Returns the root of a node.
     * <p>
     * It is the highest ancestor that has no parent.<br>
     * It may be the node itself (when it is a root).
     *
     * @param node The node. <em>MUST NOT</em> be null.
     * @return The root of {@code node}.
     */
    public default N getRoot(N node) {
        Checks.isNotNull(node, "node");

        N index = node;
        while (getParent(index) != null) {
            index = getParent(index);
        }
        return index;
    }

    /**
     * Returns {@code true} when a node is a root node.
     * <p>
     * It is non null and has a null parent.
     *
     * @param node The node.
     * @return {@code true} when {@code node} is a root.
     */
    public default boolean isRoot(N node) {
        return node != null && getParent(node) == null;
    }

    /**
     * Returns {@code true} when a node is a leaf node.
     * <p>
     * It is non null and has no children.
     *
     * @param node The node.
     * @return {@code true} when {@code node} is a leaf.
     */
    public default boolean isLeaf(N node) {
        return node != null && !hasChildren(node);
    }

    /**
     * Returns the depth of a node.
     * <p>
     * It is the number of edges between the node and the root.<br>
     * Depth of root node is 0.
     *
     * @param node The node.
     * @return The depth of {@code node}.
     */
    public default int getDepth(N node) {
        Checks.isNotNull(node, "node");

        int depth = 0;
        N index = node;
        while (getParent(index) != null) {
            index = getParent(index);
            depth++;
        }
        return depth;
    }

    /**
     * Returns the height of the tree under a node.
     * <p>
     * It is the number of edges on the longest downward path between that node and a leaf.<br>
     * The height of a leaf node is 0.
     *
     * @param node The node. <em>MUST NOT</em> be null.
     * @return The height of the tree under {@code node}.
     */
    public default int getHeight(N node) {
        Checks.isNotNull(node, "node");

        if (isLeaf(node)) {
            return 0;
        } else {
            int max = 0;
            for (final N child : getChildren(node)) {
                max = Math.max(max, getHeight(child));
            }
            return max + 1;
        }
    }

    /**
     * Returns the index of a child node in its parent's children.
     *
     * @param node The node.
     * @return The index of {@code node}, or {@code -1} if {@code node} is a root.
     */
    public default int getIndex(N node) {
        Checks.isNotNull(node, "node");

        if (getParent(node) == null) {
            return -1;
        } else {
            return IterableUtils.indexOf(getChildren(getParent(node)), node);
        }
    }

    /**
     * Returns the qualified index of a node.
     * <p>
     * It is the array of indices of the node and its ancestors, excluding the root node.<br>
     * A root node has an empty qindex.
     *
     * @param node The node.
     * @return The qualified index of {@code node}
     */
    public default int[] getQIndex(N node) {
        Checks.isNotNull(node, "node");

        final int length = getDepth(node);
        final int[] result = new int[length];
        N iter = node;
        int index = length - 1;
        while (index >= 0) {
            result[index] = getIndex(iter);
            iter = getParent(iter);
            index--;
        }
        return result;
    }

    /**
     * Returns the first common ancestor of 2 nodes.
     * <p>
     * If nodes don't belong to the same tree or are null, returns null.
     *
     * @param node1 The first node.
     * @param node2 The second node.
     * @return The first common ancestor of {@code node1} and {@code node2}.
     */
    public default N getFirstCommonAncestor(N node1,
                                            N node2) {
        if (node1 == null || node2 == null) {
            return null;
        } else {
            N index1 = node1;
            N index2 = node2;
            final int depth1 = getDepth(index1);
            final int depth2 = getDepth(index2);
            if (depth2 > depth1) {
                final int delta = depth2 - depth1;
                for (int index = 0; index < delta; index++) {
                    index2 = getParent(index2);
                }
            } else if (depth2 < depth1) {
                final int delta = depth1 - depth2;
                for (int index = 0; index < delta; index++) {
                    index1 = getParent(index1);
                }
            }

            while (index1 != index2) {
                index1 = getParent(index1);
                index2 = getParent(index2);
            }

            return index1;
        }
    }

    /**
     * Returns {@code true} when a first node is over or equal to a second node.
     *
     * @param node1 The first node.
     * @param node2 The second node.
     * @return {@code true} when {@code node1} is over or equal to {@code node2}.
     */
    public default boolean isOverOrEqual(N node1,
                                         N node2) {
        N index2 = node2;
        while (index2 != null) {
            if (index2 == node1) {
                return true;
            }
            index2 = getParent(index2);
        }
        // Here, index2 == null
        // Usually false. Just do this for the case where node1 == node2 == null
        return node1 == node2;
    }

    /**
     * Returns {@code true} when a first node is strictly over a second node.
     *
     * @param node1 The first node.
     * @param node2 The second node.
     * @return {@code true} when {@code node1} is strictly over {@code node2}.
     */
    public default boolean isStrictlyOver(N node1,
                                          N node2) {
        N index2 = node2;
        while (index2 != null) {
            if (index2 == node1) {
                return index2 != node2;
            }
            index2 = getParent(index2);
        }
        // Here, index2 == null
        return false;
    }

    /**
     * Returns {@code true} when a first node is loosely or strictly over a second node.
     *
     * @param node1 The first node.
     * @param node2 The second node.
     * @param strictness The strictness.
     * @return {@code true} when a {@code node1} is loosely or strictly over {@code node2}.
     */
    public default boolean isOver(N node1,
                                  N node2,
                                  Strictness strictness) {
        Checks.isNotNull(strictness, "strictness");

        if (strictness == Strictness.STRICT) {
            return isStrictlyOver(node1, node2);
        } else {
            return isOverOrEqual(node1, node2);
        }
    }

    /**
     * Returns {@code true} when a first node is under or equal to a second node.
     *
     * @param node1 The first node.
     * @param node2 The second node.
     * @return {@code true} when {@code node1} is under or equal to {@code node2}.
     */
    public default boolean isUnderOrEqual(N node1,
                                          N node2) {
        return isOverOrEqual(node2, node1);
    }

    /**
     * Returns {@code true} when a first node is strictly under a second node.
     *
     * @param node1 The first node.
     * @param node2 The second node.
     * @return {@code true} when {@code node1} strictly under {@code node2}.
     */
    public default boolean isStrictlyUnder(N node1,
                                           N node2) {
        return isStrictlyOver(node2, node1);
    }

    /**
     * Returns {@code true} when a first node is loosely or strictly under a second node.
     *
     * @param node1 The first node.
     * @param node2 The second node.
     * @param strictness The strictness.
     * @return {@code true} when a {@code node1} is loosely or strictly under {@code node2}.
     */
    public default boolean isUnder(N node1,
                                   N node2,
                                   Strictness strictness) {
        Checks.isNotNull(strictness, "strictness");

        if (strictness == Strictness.STRICT) {
            return isStrictlyUnder(node1, node2);
        } else {
            return isUnderOrEqual(node1, node2);
        }
    }

    public default boolean areRelatedOrEqual(N node1,
                                             N node2) {
        if (node1 == node2) {
            return true;
        } else {
            return isStrictlyOver(node1, node2) || isStrictlyOver(node2, node1);
        }
    }

    public default boolean areStrictlyRelated(N node1,
                                              N node2) {
        if (node1 == node2) {
            return false;
        } else {
            return isStrictlyOver(node1, node2) || isStrictlyOver(node2, node1);
        }
    }

    public default boolean areRelated(N node1,
                                      N node2,
                                      Strictness strictness) {
        Checks.isNotNull(strictness, "strictness");

        if (strictness == Strictness.STRICT) {
            return areStrictlyRelated(node1, node2);
        } else {
            return areRelatedOrEqual(node1, node2);
        }
    }

    public default boolean areUnrelated(N node1,
                                        N node2) {
        if (node1 == node2) {
            return false;
        } else {
            return !isStrictlyOver(node1, node2) && !isStrictlyOver(node2, node1);
        }
    }

    /**
     * Returns the relative position of 2 nodes.
     *
     * @param node1 The first node
     * @param node2 The second node.
     * @return The position of {@code node1} relatively to {@code node2}.
     */
    public default NodeRelativePosition getRelativePosition(N node1,
                                                            N node2) {
        if (node1 == node2) {
            return NodeRelativePosition.SAME;
        } else if (isStrictlyOver(node1, node2)) {
            return NodeRelativePosition.STRICTLY_OVER;
        } else if (isStrictlyUnder(node1, node2)) {
            return NodeRelativePosition.STRICTLY_UNDER;
        } else {
            return NodeRelativePosition.UNRELATED;
        }
    }

    /**
     * Returns {@code true} when a node can be attached as a parent of another node.
     *
     * @param node The possible child.
     * @param candidateParent The possible parent.
     * @return {@code true} when {@code candidateParent} can be set as parent of {@code node}.
     */
    public default boolean canSetParent(N node,
                                        N candidateParent) {
        return node != null && !isOverOrEqual(node, candidateParent);
    }

    public default N[] getPath(N node) {
        Checks.isNotNull(node, "node");

        final int length = getDepth(node) + 1;
        @SuppressWarnings("unchecked")
        final N[] path = (N[]) new Object[length];
        N iter = node;
        int index = length - 1;
        while (iter != null) {
            path[index] = iter;
            iter = getParent(iter);
            index--;
        }
        return path;
    }
}