package cdc.graphs;

public class GraphConsistencyError extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public GraphConsistencyError() {
        super();
    }

    public GraphConsistencyError(String message) {
        super(message);
    }
}