package cdc.graphs;

/**
 * Enumeration of possible methods (DEPTH_FIRST or BREADTH_FIRST) used to
 * traverse a graph.
 *
 * @author Damien Carbonne
 */
public enum TraversalMethod {
    /**
     * Use a depth first traversal algorithm.
     */
    DEPTH_FIRST,

    /**
     * Use a breadth first traversal algorithm.
     */
    BREADTH_FIRST
}