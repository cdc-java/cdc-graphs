package cdc.graphs;

/**
 * Enumeration of possible matching strictness.
 *
 * @author Damien Carbonne
 */
public enum Strictness {
    /**
     * Exclude the reference item from the result set.
     */
    STRICT,

    /**
     * Include the reference item in the result set.
     */
    LOOSE
}