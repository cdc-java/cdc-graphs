package cdc.graphs;

/**
 * Enumeration possible orders (PRE_ORDER or POST_ORDER) used to
 * traverse a graph.
 *
 * @author Damien Carbonne
 */
public enum TraversalOrder {
    PRE_ORDER,
    POST_ORDER
}