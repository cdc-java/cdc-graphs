package cdc.graphs;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import cdc.util.function.IterableUtils;

/**
 * Interface giving access to a heavy graph, where nodes and edges are represented as objects.
 * <p>
 * Graph algorithms can be built using this interface without forcing a graph
 * implementation to implement this interface.
 *
 * @author Damien Carbonne
 *
 * @param <N> Node class.
 * @param <E> Edge class.
 */
public interface GraphAdapter<N, E> {
    /**
     * @return an Iterable of all nodes.
     */
    public Iterable<? extends N> getNodes();

    public default Stream<? extends N> getNodesStream() {
        return StreamSupport.stream(getNodes().spliterator(), false);
    }

    /**
     * Return an Iterable of all nodes that match a predicate.
     *
     * @param predicate The predicate.
     * @return an Iterable of all nodes that match {@code predicate}.
     */
    public default Iterable<? extends N> getNodes(Predicate<? super N> predicate) {
        return IterableUtils.filter(getNodes(), predicate);
    }

    /**
     * @return The number of nodes in the graph.
     */
    public default int getNodesCount() {
        return IterableUtils.size(getNodes());
    }

    /**
     * @return {@code true} if the graph has nodes.
     */
    public default boolean hasNodes() {
        return !IterableUtils.isEmpty(getNodes());
    }

    /**
     * Returns {@code true} if a node belongs to this graph.
     *
     * @param node The node.
     * @return {@code true} if {@code node} is member of this graph.
     */
    public boolean containsNode(N node);

    /**
     * @return an Iterable of all edges.
     */
    public Iterable<? extends E> getEdges();

    public default Stream<? extends E> getEdgesStream() {
        return StreamSupport.stream(getEdges().spliterator(), false);
    }

    /**
     * @param predicate The predicate.
     * @return an Iterable of all edges that match {@code predicate}.
     */
    public default Iterable<? extends E> getEdges(Predicate<? super E> predicate) {
        return IterableUtils.filter(getEdges(), predicate);
    }

    /**
     * @return The number of edges in the graph.
     */
    public default int getEdgesCount() {
        return IterableUtils.size(getEdges());
    }

    /**
     * @return {@code true} if the graph has edges.
     */
    public default boolean hasEdges() {
        return !IterableUtils.isEmpty(getEdges());
    }

    /**
     * Returns {@code true} if an edge belongs to this graph.
     *
     * @param edge The edge.
     * @return {@code true} if {@code edge} is member of graph.
     */
    public boolean containsEdge(E edge);

    /**
     * Returns an Iterable of edges attached to a node in a given direction.
     *
     * @param node The node. <em>MUST NOT</em> be {@code null}.
     * @param direction The direction. <em>MAY</em> be {@code null}.
     *            If {@code null}, then all edges are returned.
     * @return An Iterable of edges attached to {@code node} in {@code direction}.
     */
    public Iterable<? extends E> getEdges(N node,
                                          EdgeDirection direction);

    /**
     * Returns an Iterable of edges attached to a node in a given direction and matching a predicate.
     *
     * @param node The node. <em>MUST NOT</em> be {@code null}.
     * @param direction The direction. <em>MAY</em> be {@code null}.
     *            If {@code null}, then all edges are returned.
     * @param predicate The predicate to filter edges.
     * @return An Iterable of edges attached to {@code node} in {@code direction} and matching {@code predicate}.
     */
    public default Iterable<? extends E> getEdges(N node,
                                                  EdgeDirection direction,
                                                  Predicate<? super E> predicate) {
        return IterableUtils.filter(getEdges(node, direction), predicate);
    }

    /**
     * Returns an Iterable of edges attached to a node in a given direction.
     *
     * @param node The node. <em>MUST NOT</em> be {@code null}.
     * @param direction The direction. <em>MAY</em> be {@code null}.
     * @return A set of all nodes connected to {@code node} in {@code direction}.
     */
    public default Set<N> getConnectedNodes(N node,
                                            EdgeDirection direction) {
        final Set<N> set = new HashSet<>();
        if (direction == null || direction == EdgeDirection.OUTGOING) {
            getEdgesStream(node, EdgeDirection.OUTGOING).map(e -> getTip(e, EdgeTip.TARGET))
                                                        .forEach(set::add);
        }
        if (direction == null || direction == EdgeDirection.INGOING) {
            getEdgesStream(node, EdgeDirection.INGOING).map(e -> getTip(e, EdgeTip.SOURCE))
                                                       .forEach(set::add);
        }
        return set;
    }

    /**
     * Returns a set of all nodes that are connected to a node.
     *
     * @param node The node.
     * @return A set of all nodes that are connected to {@code node}.
     */
    public default Set<N> getConnectedNodes(N node) {
        return getConnectedNodes(node, null);
    }

    public default Stream<? extends E> getEdgesStream(N node,
                                                      EdgeDirection direction) {
        return StreamSupport.stream(getEdges(node, direction).spliterator(), false);
    }

    /**
     * @param node The node.
     * @return An Iterable of all edges attached to {@code node}.
     */
    public default Iterable<? extends E> getEdges(N node) {
        return getEdges(node, (EdgeDirection) null);
    }

    /**
     *
     * @param node The node.
     * @param predicate The predicate.
     * @return An Iterable of all edges attached to {@code node} and that match {@code predicate}.
     */
    public default Iterable<? extends E> getEdges(N node,
                                                  Predicate<? super E> predicate) {
        return getEdges(node, null, predicate);
    }

    public default Stream<? extends E> getEdgesStream(N node) {
        return StreamSupport.stream(getEdges(node).spliterator(), false);
    }

    /**
     * Returns the number of edges attached to a node, in one or any direction.
     *
     * @param node The node.
     * @param direction The optional direction. <em>MAY</em> be {@code null}
     *            to indicate that direction does not matter.
     * @return The number of edges attached to {@code node},
     *         in one (if not {@code null}) or any (if {@code null}) {@code direction}.
     */
    public default int getEdgesCount(N node,
                                     EdgeDirection direction) {
        return IterableUtils.size(getEdges(node, direction));
    }

    /**
     * Returns the number of edges attached to a node, in any direction.
     *
     * @param node The node.
     * @return The number of edges attached to {@code node}, in any direction.
     */
    public default int getEdgesCount(N node) {
        return getEdgesCount(node, null);
    }

    /**
     * Returns {@code true} when there are edges attached to a node, in one or any direction.
     *
     * @param node The node.
     * @param direction The optional direction. <em>MAY</em> be {@code null} to indicate
     *            that direction does not matter.
     * @return {@code true} when there are edges attached to {@code node},
     *         in one (if not {@code null}) or any (if {@code null}) {@code direction}.
     */
    public default boolean hasEdges(N node,
                                    EdgeDirection direction) {
        return !IterableUtils.isEmpty(getEdges(node, direction));
    }

    /**
     * Returns {@code true} when there are edges attached to a node, in any direction.
     *
     * @param node The node.
     * @return {@code true} when there are edges attached to {@code node}, in any direction.
     */
    public default boolean hasEdges(N node) {
        return hasEdges(node, null);
    }

    /**
     * Returns a tip of an edge.
     *
     * @param edge The edge. <em>MUST NOT</em> be {@code null}.
     * @param tip The tip. <em>MUST NOT</em> be {@code null}.
     * @return The node attached to edge for tip.
     */
    public N getTip(E edge,
                    EdgeTip tip);

    /**
     * Returns {@code true} when a node is a root: It has no ingoing edges.
     *
     * @param node The node.
     * @return {@code true} when node is a root.
     */
    public default boolean isRoot(N node) {
        return !hasEdges(node, EdgeDirection.INGOING);
    }

    /**
     * Return {@code true} if a node is a leaf: It has no outgoing edges.
     *
     * @param node The node.
     * @return {@code true} when node is a leaf.
     */
    public default boolean isLeaf(N node) {
        return !hasEdges(node, EdgeDirection.OUTGOING);
    }

    /**
     * @return A Set of all root nodes.
     */
    public default Set<N> getRoots() {
        return getNodesStream().filter(this::isRoot)
                               .collect(Collectors.toSet());
    }

    /**
     * @return A Set of all leaf nodes.
     */
    public default Set<N> getLeaves() {
        return getNodesStream().filter(this::isLeaf)
                               .collect(Collectors.toSet());
    }

    /**
     * Returns the connectivity of a node.
     *
     * @param node The node
     * @return The connectivity of {@code node}.
     */
    public default NodeConnectivity getConnectivity(N node) {
        final boolean in = hasEdges(node, EdgeDirection.INGOING);
        final boolean out = hasEdges(node, EdgeDirection.OUTGOING);
        if (in) {
            return out ? NodeConnectivity.IN_OUT : NodeConnectivity.IN;
        } else {
            return out ? NodeConnectivity.OUT : NodeConnectivity.ISOLATED;
        }
    }

    /**
     * Returns {@code true} when there is an edge between a source node and target node.
     * <p>
     * <b>WARNING:</b> order of passed arguments matters.
     *
     * @param source The source node.
     * @param target The target node.
     * @return {@code true} when there is an edge between {@code source} and {@code target}.
     */
    public default boolean hasEdge(N source,
                                   N target) {
        for (final E edge : getEdges(source, EdgeDirection.OUTGOING)) {
            if (getTip(edge, EdgeTip.TARGET).equals(target)) {
                return true;
            }
        }
        return false;
    }
}